<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:3285c8a9-ec6d-4747-9589-295026f6398c(org.clawiz.etl.common.language.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="6" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="lehn" ref="r:f00fd6de-b38e-411a-9521-a0b7f412e1e8(org.clawiz.core.common.language.structure)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="4628067390765956802" name="abstract" index="R5$K7" />
        <property id="5092175715804935370" name="conceptAlias" index="34LRSv" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
        <child id="1071489727084" name="propertyDeclaration" index="1TKVEl" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288299" name="jetbrains.mps.lang.structure.structure.PropertyDeclaration" flags="ig" index="1TJgyi">
        <property id="241647608299431129" name="propertyId" index="IQ2nx" />
        <reference id="1082985295845" name="dataType" index="AX2Wp" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="2zqjo15lxLa">
    <property role="EcuMT" value="2943750519754464330" />
    <property role="TrG5h" value="Job" />
    <property role="3GE5qa" value="job" />
    <property role="19KtqR" value="true" />
    <property role="34LRSv" value="Job" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="2zqjo15lxM_" role="1TKVEi">
      <property role="IQ2ns" value="2943750519754464421" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="steps" />
      <property role="20lbJX" value="1..n" />
      <ref role="20lvS9" node="2zqjo15lyhF" resolve="JobStep" />
    </node>
    <node concept="PrWs8" id="2zqjo15lxLb" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="PrWs8" id="fPT0jCsHf$" role="PzmwI">
      <ref role="PrY4T" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
    </node>
  </node>
  <node concept="1TIwiD" id="2zqjo15lxLd">
    <property role="EcuMT" value="2943750519754464333" />
    <property role="TrG5h" value="AbstractJobStepAction" />
    <property role="3GE5qa" value="job.step.action" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
  </node>
  <node concept="1TIwiD" id="2zqjo15lxLg">
    <property role="EcuMT" value="2943750519754464336" />
    <property role="3GE5qa" value="job.step" />
    <property role="TrG5h" value="JobStepRef" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="2zqjo15lxLh" role="1TKVEi">
      <property role="IQ2ns" value="2943750519754464337" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="jobStep" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="2zqjo15lyhF" resolve="JobStep" />
    </node>
  </node>
  <node concept="1TIwiD" id="2zqjo15lxLn">
    <property role="EcuMT" value="2943750519754464343" />
    <property role="TrG5h" value="Structure" />
    <property role="3GE5qa" value="structure" />
    <property role="19KtqR" value="true" />
    <property role="34LRSv" value="Structure" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="2zqjo15lxLo" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="PrWs8" id="fPT0jCslaZ" role="PzmwI">
      <ref role="PrY4T" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
    </node>
    <node concept="1TJgyj" id="2zqjo15lxLt" role="1TKVEi">
      <property role="IQ2ns" value="2943750519754464349" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="fields" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="2zqjo15lxLq" resolve="StructureField" />
    </node>
    <node concept="1TJgyj" id="fPT0jCslrT" role="1TKVEi">
      <property role="IQ2ns" value="285384861138638585" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="recordIdField" />
      <ref role="20lvS9" node="2zqjo15lxLq" resolve="StructureField" />
    </node>
  </node>
  <node concept="1TIwiD" id="2zqjo15lxLq">
    <property role="EcuMT" value="2943750519754464346" />
    <property role="3GE5qa" value="structure.field" />
    <property role="TrG5h" value="StructureField" />
    <property role="34LRSv" value="field" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="6porqNrmPQf" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="valueType" />
      <property role="20lbJX" value="1" />
      <property role="IQ2ns" value="7374764979001187727" />
      <ref role="20lvS9" node="2zqjo15lTZ2" resolve="AbstractStructureValueType" />
    </node>
    <node concept="1TJgyi" id="5teVQz_ANmH" role="1TKVEl">
      <property role="TrG5h" value="description" />
      <property role="IQ2nx" value="6291228963290953133" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="PrWs8" id="2zqjo15lxLr" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="2zqjo15lxLv">
    <property role="EcuMT" value="2943750519754464351" />
    <property role="3GE5qa" value="job.step.action.extract" />
    <property role="TrG5h" value="AbstractExtractJobStepAction" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" node="2zqjo15lxLd" resolve="AbstractJobStepAction" />
  </node>
  <node concept="1TIwiD" id="2zqjo15lxLw">
    <property role="EcuMT" value="2943750519754464352" />
    <property role="3GE5qa" value="job.step.action.load" />
    <property role="TrG5h" value="AbstractLoadJobStepAction" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" node="2zqjo15lxLd" resolve="AbstractJobStepAction" />
  </node>
  <node concept="1TIwiD" id="2zqjo15lxLx">
    <property role="EcuMT" value="2943750519754464353" />
    <property role="TrG5h" value="AbstractConnection" />
    <property role="R5$K7" value="true" />
    <property role="3GE5qa" value="connection" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="2zqjo15lxL$" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="2zqjo15lxLA">
    <property role="EcuMT" value="2943750519754464358" />
    <property role="3GE5qa" value="connection" />
    <property role="TrG5h" value="JDBCConnection" />
    <property role="34LRSv" value="JDBC connection" />
    <property role="19KtqR" value="true" />
    <ref role="1TJDcQ" node="2zqjo15lxLx" resolve="AbstractConnection" />
    <node concept="1TJgyi" id="2zqjo15lxLB" role="1TKVEl">
      <property role="IQ2nx" value="2943750519754464359" />
      <property role="TrG5h" value="url" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="2zqjo15lxLD" role="1TKVEl">
      <property role="IQ2nx" value="2943750519754464361" />
      <property role="TrG5h" value="username" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="2zqjo15lxLG" role="1TKVEl">
      <property role="IQ2nx" value="2943750519754464364" />
      <property role="TrG5h" value="password" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="2zqjo15lxLK">
    <property role="EcuMT" value="2943750519754464368" />
    <property role="3GE5qa" value="job.step.action.extract.table" />
    <property role="TrG5h" value="ExtractTableDataJobStepAction" />
    <property role="34LRSv" value="extract table data" />
    <ref role="1TJDcQ" node="2zqjo15lxLv" resolve="AbstractExtractJobStepAction" />
    <node concept="1TJgyj" id="2zqjo15lxLL" role="1TKVEi">
      <property role="IQ2ns" value="2943750519754464369" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="connection" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="2zqjo15lxLA" resolve="JDBCConnection" />
    </node>
    <node concept="1TJgyj" id="2zqjo15lxLR" role="1TKVEi">
      <property role="IQ2ns" value="2943750519754464375" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="structure" />
      <ref role="20lvS9" node="2zqjo15lxLn" resolve="Structure" />
    </node>
    <node concept="1TJgyi" id="2zqjo15lxLU" role="1TKVEl">
      <property role="IQ2nx" value="2943750519754464378" />
      <property role="TrG5h" value="sourceTableName" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="2zqjo15lxLW">
    <property role="EcuMT" value="2943750519754464380" />
    <property role="3GE5qa" value="job.step.action.load.table" />
    <property role="TrG5h" value="LoadTableDataJobStepAction" />
    <property role="34LRSv" value="load table data" />
    <ref role="1TJDcQ" node="2zqjo15lxLw" resolve="AbstractLoadJobStepAction" />
    <node concept="1TJgyj" id="2zqjo15lxLZ" role="1TKVEi">
      <property role="IQ2ns" value="2943750519754464383" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="connection" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="2zqjo15lxLA" resolve="JDBCConnection" />
    </node>
    <node concept="1TJgyj" id="2zqjo15lxM1" role="1TKVEi">
      <property role="IQ2ns" value="2943750519754464385" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="sourceStructure" />
      <ref role="20lvS9" node="2zqjo15lxLn" resolve="Structure" />
    </node>
    <node concept="1TJgyi" id="2zqjo15lxLX" role="1TKVEl">
      <property role="IQ2nx" value="2943750519754464381" />
      <property role="TrG5h" value="targetTableName" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="2zqjo15lxM8">
    <property role="EcuMT" value="2943750519754464392" />
    <property role="3GE5qa" value="job.step.action.load.type" />
    <property role="TrG5h" value="LoadTypeDataJobStepAction" />
    <property role="34LRSv" value="load type data" />
    <ref role="1TJDcQ" node="2zqjo15lxLw" resolve="AbstractLoadJobStepAction" />
    <node concept="1TJgyj" id="2zqjo15ly9F" role="1TKVEi">
      <property role="IQ2ns" value="2943750519754465899" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="connection" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="2zqjo15lxLA" resolve="JDBCConnection" />
    </node>
    <node concept="1TJgyj" id="2zqjo15ly9y" role="1TKVEi">
      <property role="IQ2ns" value="2943750519754465890" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="sourceStructure" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="2zqjo15lxLn" resolve="Structure" />
    </node>
    <node concept="1TJgyj" id="2zqjo15lxMl" role="1TKVEi">
      <property role="IQ2ns" value="2943750519754464405" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="targetType" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="lehn:6porqNrmg18" resolve="Type" />
    </node>
    <node concept="1TJgyj" id="fPT0jCsM50" role="1TKVEi">
      <property role="IQ2ns" value="285384861138755904" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="fieldMaps" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="fPT0jCsM4K" resolve="TypeDataLoadFieldMap" />
    </node>
    <node concept="PrWs8" id="fPT0jCsM6m" role="PzmwI">
      <ref role="PrY4T" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
    </node>
  </node>
  <node concept="1TIwiD" id="2zqjo15ly4H">
    <property role="EcuMT" value="2943750519754465581" />
    <property role="3GE5qa" value="job.step.action.extract.type" />
    <property role="TrG5h" value="ExtractTypeDataJobStepAction" />
    <property role="34LRSv" value="extract type data" />
    <ref role="1TJDcQ" node="2zqjo15lxLv" resolve="AbstractExtractJobStepAction" />
    <node concept="1TJgyj" id="2zqjo15ly4I" role="1TKVEi">
      <property role="IQ2ns" value="2943750519754465582" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="connection" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="2zqjo15lxLA" resolve="JDBCConnection" />
    </node>
    <node concept="1TJgyj" id="2zqjo15ly4J" role="1TKVEi">
      <property role="IQ2ns" value="2943750519754465583" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="sourceType" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="lehn:6porqNrmg18" resolve="Type" />
    </node>
    <node concept="1TJgyj" id="2zqjo15ly8J" role="1TKVEi">
      <property role="IQ2ns" value="2943750519754465839" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="structure" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="2zqjo15lxLn" resolve="Structure" />
    </node>
  </node>
  <node concept="1TIwiD" id="2zqjo15lyhF">
    <property role="EcuMT" value="2943750519754466411" />
    <property role="3GE5qa" value="job.step" />
    <property role="TrG5h" value="JobStep" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="2zqjo15lxLj" role="1TKVEi">
      <property role="IQ2ns" value="2943750519754464339" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="previousSteps" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="2zqjo15lxLg" resolve="JobStepRef" />
    </node>
    <node concept="1TJgyj" id="2zqjo15lyhJ" role="1TKVEi">
      <property role="IQ2ns" value="2943750519754466415" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="actions" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="2zqjo15lxLd" resolve="AbstractJobStepAction" />
    </node>
    <node concept="PrWs8" id="2zqjo15lyhH" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="2zqjo15lTZ2">
    <property role="EcuMT" value="2943750519754563522" />
    <property role="3GE5qa" value="structure.valuetype" />
    <property role="TrG5h" value="AbstractStructureValueType" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
  </node>
  <node concept="1TIwiD" id="3AEsidS5gfG">
    <property role="EcuMT" value="4155258003355141100" />
    <property role="3GE5qa" value="structure.valuetype" />
    <property role="TrG5h" value="StructureValueTypeString" />
    <property role="34LRSv" value="string" />
    <ref role="1TJDcQ" node="2zqjo15lTZ2" resolve="AbstractStructureValueType" />
    <node concept="1TJgyi" id="6porqNrmRYT" role="1TKVEl">
      <property role="TrG5h" value="length" />
      <property role="IQ2nx" value="7374764979001196473" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
  </node>
  <node concept="1TIwiD" id="3AEsidS5gfL">
    <property role="EcuMT" value="4155258003355141105" />
    <property role="3GE5qa" value="structure.valuetype" />
    <property role="TrG5h" value="StructureValueTypeNumber" />
    <property role="34LRSv" value="number" />
    <ref role="1TJDcQ" node="2zqjo15lTZ2" resolve="AbstractStructureValueType" />
  </node>
  <node concept="1TIwiD" id="3AEsidS5gfP">
    <property role="EcuMT" value="4155258003355141109" />
    <property role="3GE5qa" value="structure.valuetype" />
    <property role="TrG5h" value="StructureValueTypeDate" />
    <property role="34LRSv" value="date" />
    <ref role="1TJDcQ" node="2zqjo15lTZ2" resolve="AbstractStructureValueType" />
  </node>
  <node concept="1TIwiD" id="3AEsidS5gfR">
    <property role="EcuMT" value="4155258003355141111" />
    <property role="3GE5qa" value="structure.valuetype" />
    <property role="TrG5h" value="StructureValueTypeDateTime" />
    <property role="34LRSv" value="datetime" />
    <ref role="1TJDcQ" node="2zqjo15lTZ2" resolve="AbstractStructureValueType" />
  </node>
  <node concept="1TIwiD" id="3AEsidS5gfT">
    <property role="EcuMT" value="4155258003355141113" />
    <property role="3GE5qa" value="structure.valuetype" />
    <property role="TrG5h" value="StructureValueTypeScaledNumber" />
    <property role="34LRSv" value="number(,)" />
    <ref role="1TJDcQ" node="2zqjo15lTZ2" resolve="AbstractStructureValueType" />
    <node concept="1TJgyi" id="3AEsidS5gfV" role="1TKVEl">
      <property role="IQ2nx" value="4155258003355141115" />
      <property role="TrG5h" value="precision" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyi" id="3AEsidS5gfU" role="1TKVEl">
      <property role="TrG5h" value="scale" />
      <property role="IQ2nx" value="4155258003355141114" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
  </node>
  <node concept="1TIwiD" id="3AEsidS5jQY">
    <property role="EcuMT" value="4155258003355155902" />
    <property role="3GE5qa" value="structure.valuetype" />
    <property role="TrG5h" value="StructureValueTypeStructureReference" />
    <property role="34LRSv" value="-&gt;" />
    <ref role="1TJDcQ" node="2zqjo15lTZ2" resolve="AbstractStructureValueType" />
    <node concept="1TJgyj" id="3AEsidS5jR0" role="1TKVEi">
      <property role="IQ2ns" value="4155258003355155904" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="referencedStructure" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="2zqjo15lxLn" resolve="Structure" />
    </node>
  </node>
  <node concept="1TIwiD" id="fPT0jCsM4K">
    <property role="EcuMT" value="285384861138755888" />
    <property role="3GE5qa" value="job.step.action.load.type" />
    <property role="TrG5h" value="TypeDataLoadFieldMap" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="fPT0jCsM55" role="1TKVEi">
      <property role="IQ2ns" value="285384861138755909" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="sourceField" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="2zqjo15lxLq" resolve="StructureField" />
    </node>
    <node concept="1TJgyj" id="fPT0jCsM57" role="1TKVEi">
      <property role="IQ2ns" value="285384861138755911" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="targetField" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="lehn:6porqNrmPQb" resolve="TypeField" />
    </node>
  </node>
</model>

