<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:24300508-7299-4deb-9f80-a3293240997c(org.clawiz.etl.common.language.behavior)">
  <persistence version="9" />
  <languages>
    <use id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior" version="1" />
    <use id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel" version="8" />
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="5" />
    <devkit ref="2677cb18-f558-4e33-bc38-a5139cee06dc(jetbrains.mps.devkit.language-design)" />
  </languages>
  <imports>
    <import index="o8zo" ref="r:314576fc-3aee-4386-a0a5-a38348ac317d(jetbrains.mps.scope)" />
    <import index="tpcu" ref="r:00000000-0000-4000-0000-011c89590282(jetbrains.mps.lang.core.behavior)" />
    <import index="lehn" ref="r:f00fd6de-b38e-411a-9521-a0b7f412e1e8(org.clawiz.core.common.language.structure)" />
    <import index="opwy" ref="r:3285c8a9-ec6d-4747-9589-295026f6398c(org.clawiz.etl.common.language.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior">
      <concept id="1225194240794" name="jetbrains.mps.lang.behavior.structure.ConceptBehavior" flags="ng" index="13h7C7">
        <reference id="1225194240799" name="concept" index="13h7C2" />
        <child id="1225194240805" name="method" index="13h7CS" />
        <child id="1225194240801" name="constructor" index="13h7CW" />
      </concept>
      <concept id="1225194413805" name="jetbrains.mps.lang.behavior.structure.ConceptConstructorDeclaration" flags="in" index="13hLZK" />
      <concept id="1225194472830" name="jetbrains.mps.lang.behavior.structure.ConceptMethodDeclaration" flags="ng" index="13i0hz">
        <property id="5864038008284099149" name="isStatic" index="2Ki8OM" />
        <reference id="1225194472831" name="overriddenMethod" index="13i0hy" />
      </concept>
      <concept id="1225194691553" name="jetbrains.mps.lang.behavior.structure.ThisNodeExpression" flags="nn" index="13iPFW" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1180031783296" name="jetbrains.mps.lang.smodel.structure.Concept_IsSubConceptOfOperation" flags="nn" index="2Zo12i">
        <child id="1180031783297" name="conceptArgument" index="2Zo12j" />
      </concept>
      <concept id="2644386474301421077" name="jetbrains.mps.lang.smodel.structure.LinkIdRefExpression" flags="nn" index="359W_D">
        <reference id="2644386474301421078" name="conceptDeclaration" index="359W_E" />
        <reference id="2644386474301421079" name="linkDeclaration" index="359W_F" />
      </concept>
      <concept id="6677504323281689838" name="jetbrains.mps.lang.smodel.structure.SConceptType" flags="in" index="3bZ5Sz" />
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2" />
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="13h7C7" id="fPT0jCsiOV">
    <property role="3GE5qa" value="structure" />
    <ref role="13h7C2" to="opwy:2zqjo15lxLn" resolve="Structure" />
    <node concept="13hLZK" id="fPT0jCsiOW" role="13h7CW">
      <node concept="3clFbS" id="fPT0jCsiOX" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="5J_TEGDjNm6" role="13h7CS">
      <property role="TrG5h" value="getScope" />
      <property role="2Ki8OM" value="false" />
      <ref role="13i0hy" to="tpcu:52_Geb4QDV$" resolve="getScope" />
      <node concept="3clFbS" id="5J_TEGDjNm9" role="3clF47">
        <node concept="3clFbH" id="5J_TEGDjRl4" role="3cqZAp" />
        <node concept="3clFbJ" id="5J_TEGDjNoL" role="3cqZAp">
          <node concept="3clFbS" id="5J_TEGDjNoN" role="3clFbx">
            <node concept="3cpWs6" id="5J_TEGDjNQY" role="3cqZAp">
              <node concept="2YIFZM" id="5J_TEGDjNTL" role="3cqZAk">
                <ref role="1Pybhc" to="o8zo:7ipADkTevLm" resolve="SimpleRoleScope" />
                <ref role="37wK5l" to="o8zo:6t3ylNOzI9Y" resolve="forNamedElements" />
                <node concept="13iPFW" id="5J_TEGDjNVB" role="37wK5m" />
                <node concept="359W_D" id="5J_TEGDjOtG" role="37wK5m">
                  <ref role="359W_E" to="opwy:2zqjo15lxLn" resolve="Structure" />
                  <ref role="359W_F" to="opwy:2zqjo15lxLt" resolve="fields" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="5J_TEGDjNxE" role="3clFbw">
            <node concept="37vLTw" id="5J_TEGDjNpj" role="2Oq$k0">
              <ref role="3cqZAo" node="5J_TEGDjNmM" resolve="kind" />
            </node>
            <node concept="2Zo12i" id="5J_TEGDjNLo" role="2OqNvi">
              <node concept="chp4Y" id="fPT0jCslhe" role="2Zo12j">
                <ref role="cht4Q" to="opwy:2zqjo15lxLq" resolve="StructureField" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5J_TEGDjRnx" role="3cqZAp" />
        <node concept="3cpWs6" id="5J_TEGDjNnE" role="3cqZAp">
          <node concept="10Nm6u" id="5J_TEGDjNo7" role="3cqZAk" />
        </node>
      </node>
      <node concept="37vLTG" id="5J_TEGDjNmM" role="3clF46">
        <property role="TrG5h" value="kind" />
        <node concept="3bZ5Sz" id="5J_TEGDjNmN" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="5J_TEGDjNmO" role="3clF46">
        <property role="TrG5h" value="child" />
        <node concept="3Tqbb2" id="5J_TEGDjNmP" role="1tU5fm" />
      </node>
      <node concept="3uibUv" id="5J_TEGDjNmQ" role="3clF45">
        <ref role="3uigEE" to="o8zo:3fifI_xCtN$" resolve="Scope" />
      </node>
      <node concept="3Tm1VV" id="5J_TEGDjNmR" role="1B3o_S" />
    </node>
  </node>
  <node concept="13h7C7" id="fPT0jCsHfC">
    <property role="3GE5qa" value="job" />
    <ref role="13h7C2" to="opwy:2zqjo15lxLa" resolve="Job" />
    <node concept="13i0hz" id="fPT0jCsHfN" role="13h7CS">
      <property role="TrG5h" value="getScope" />
      <property role="2Ki8OM" value="false" />
      <ref role="13i0hy" to="tpcu:52_Geb4QDV$" resolve="getScope" />
      <node concept="3clFbS" id="fPT0jCsHfO" role="3clF47">
        <node concept="3clFbH" id="fPT0jCsHfP" role="3cqZAp" />
        <node concept="3clFbJ" id="fPT0jCsHfQ" role="3cqZAp">
          <node concept="3clFbS" id="fPT0jCsHfR" role="3clFbx">
            <node concept="3cpWs6" id="fPT0jCsHfS" role="3cqZAp">
              <node concept="2YIFZM" id="fPT0jCsHfT" role="3cqZAk">
                <ref role="1Pybhc" to="o8zo:7ipADkTevLm" resolve="SimpleRoleScope" />
                <ref role="37wK5l" to="o8zo:6t3ylNOzI9Y" resolve="forNamedElements" />
                <node concept="13iPFW" id="fPT0jCsHfU" role="37wK5m" />
                <node concept="359W_D" id="fPT0jCsHfV" role="37wK5m">
                  <ref role="359W_E" to="opwy:2zqjo15lxLa" resolve="Job" />
                  <ref role="359W_F" to="opwy:2zqjo15lxM_" resolve="steps" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="fPT0jCsHfW" role="3clFbw">
            <node concept="37vLTw" id="fPT0jCsHfX" role="2Oq$k0">
              <ref role="3cqZAo" node="fPT0jCsHg3" resolve="kind" />
            </node>
            <node concept="2Zo12i" id="fPT0jCsHfY" role="2OqNvi">
              <node concept="chp4Y" id="fPT0jCsHkH" role="2Zo12j">
                <ref role="cht4Q" to="opwy:2zqjo15lyhF" resolve="JobStep" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="fPT0jCsHg0" role="3cqZAp" />
        <node concept="3cpWs6" id="fPT0jCsHg1" role="3cqZAp">
          <node concept="10Nm6u" id="fPT0jCsHg2" role="3cqZAk" />
        </node>
      </node>
      <node concept="37vLTG" id="fPT0jCsHg3" role="3clF46">
        <property role="TrG5h" value="kind" />
        <node concept="3bZ5Sz" id="fPT0jCsHg4" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="fPT0jCsHg5" role="3clF46">
        <property role="TrG5h" value="child" />
        <node concept="3Tqbb2" id="fPT0jCsHg6" role="1tU5fm" />
      </node>
      <node concept="3uibUv" id="fPT0jCsHg7" role="3clF45">
        <ref role="3uigEE" to="o8zo:3fifI_xCtN$" resolve="Scope" />
      </node>
      <node concept="3Tm1VV" id="fPT0jCsHg8" role="1B3o_S" />
    </node>
    <node concept="13hLZK" id="fPT0jCsHfD" role="13h7CW">
      <node concept="3clFbS" id="fPT0jCsHfE" role="2VODD2" />
    </node>
  </node>
  <node concept="13h7C7" id="fPT0jCsM6o">
    <property role="3GE5qa" value="job.step.action.load.type" />
    <ref role="13h7C2" to="opwy:2zqjo15lxM8" resolve="LoadTypeDataJobStepAction" />
    <node concept="13i0hz" id="fPT0jCsMbp" role="13h7CS">
      <property role="TrG5h" value="getScope" />
      <property role="2Ki8OM" value="false" />
      <ref role="13i0hy" to="tpcu:52_Geb4QDV$" resolve="getScope" />
      <node concept="3clFbS" id="fPT0jCsMbq" role="3clF47">
        <node concept="3clFbH" id="fPT0jCsMbr" role="3cqZAp" />
        <node concept="3clFbJ" id="fPT0jCsMbs" role="3cqZAp">
          <node concept="3clFbS" id="fPT0jCsMbt" role="3clFbx">
            <node concept="3cpWs6" id="fPT0jCsMbu" role="3cqZAp">
              <node concept="2YIFZM" id="fPT0jCsMbv" role="3cqZAk">
                <ref role="1Pybhc" to="o8zo:7ipADkTevLm" resolve="SimpleRoleScope" />
                <ref role="37wK5l" to="o8zo:6t3ylNOzI9Y" resolve="forNamedElements" />
                <node concept="2OqwBi" id="fPT0jCsMGF" role="37wK5m">
                  <node concept="13iPFW" id="fPT0jCsMbw" role="2Oq$k0" />
                  <node concept="3TrEf2" id="fPT0jCsMX5" role="2OqNvi">
                    <ref role="3Tt5mk" to="opwy:2zqjo15ly9y" resolve="sourceStructure" />
                  </node>
                </node>
                <node concept="359W_D" id="fPT0jCsMbx" role="37wK5m">
                  <ref role="359W_E" to="opwy:2zqjo15lxLn" resolve="Structure" />
                  <ref role="359W_F" to="opwy:2zqjo15lxLt" resolve="fields" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="fPT0jCsMby" role="3clFbw">
            <node concept="37vLTw" id="fPT0jCsMbz" role="2Oq$k0">
              <ref role="3cqZAo" node="fPT0jCsMbD" resolve="kind" />
            </node>
            <node concept="2Zo12i" id="fPT0jCsMb$" role="2OqNvi">
              <node concept="chp4Y" id="fPT0jCsMlf" role="2Zo12j">
                <ref role="cht4Q" to="opwy:2zqjo15lxLq" resolve="StructureField" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="fPT0jCsNHV" role="3cqZAp" />
        <node concept="3clFbJ" id="fPT0jCsMgi" role="3cqZAp">
          <node concept="3clFbS" id="fPT0jCsMgj" role="3clFbx">
            <node concept="3cpWs6" id="fPT0jCsMgk" role="3cqZAp">
              <node concept="2YIFZM" id="fPT0jCsMgl" role="3cqZAk">
                <ref role="1Pybhc" to="o8zo:7ipADkTevLm" resolve="SimpleRoleScope" />
                <ref role="37wK5l" to="o8zo:6t3ylNOzI9Y" resolve="forNamedElements" />
                <node concept="2OqwBi" id="fPT0jCsNeo" role="37wK5m">
                  <node concept="13iPFW" id="fPT0jCsMgm" role="2Oq$k0" />
                  <node concept="3TrEf2" id="fPT0jCsNwV" role="2OqNvi">
                    <ref role="3Tt5mk" to="opwy:2zqjo15lxMl" resolve="targetType" />
                  </node>
                </node>
                <node concept="359W_D" id="fPT0jCsMgn" role="37wK5m">
                  <ref role="359W_E" to="lehn:6porqNrmg18" resolve="Type" />
                  <ref role="359W_F" to="lehn:6porqNrmQ8g" resolve="fields" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="fPT0jCsMgo" role="3clFbw">
            <node concept="37vLTw" id="fPT0jCsMgp" role="2Oq$k0">
              <ref role="3cqZAo" node="fPT0jCsMbD" resolve="kind" />
            </node>
            <node concept="2Zo12i" id="fPT0jCsMgq" role="2OqNvi">
              <node concept="chp4Y" id="fPT0jCsX9X" role="2Zo12j">
                <ref role="cht4Q" to="lehn:6porqNrmPQb" resolve="TypeField" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="fPT0jCsMbA" role="3cqZAp" />
        <node concept="3cpWs6" id="fPT0jCsMbB" role="3cqZAp">
          <node concept="10Nm6u" id="fPT0jCsMbC" role="3cqZAk" />
        </node>
      </node>
      <node concept="37vLTG" id="fPT0jCsMbD" role="3clF46">
        <property role="TrG5h" value="kind" />
        <node concept="3bZ5Sz" id="fPT0jCsMbE" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="fPT0jCsMbF" role="3clF46">
        <property role="TrG5h" value="child" />
        <node concept="3Tqbb2" id="fPT0jCsMbG" role="1tU5fm" />
      </node>
      <node concept="3uibUv" id="fPT0jCsMbH" role="3clF45">
        <ref role="3uigEE" to="o8zo:3fifI_xCtN$" resolve="Scope" />
      </node>
      <node concept="3Tm1VV" id="fPT0jCsMbI" role="1B3o_S" />
    </node>
    <node concept="13hLZK" id="fPT0jCsM6p" role="13h7CW">
      <node concept="3clFbS" id="fPT0jCsM6q" role="2VODD2" />
    </node>
  </node>
</model>

