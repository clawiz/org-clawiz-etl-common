<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:8d310396-7b1c-4a59-9c3d-63d1f38123fe(org.clawiz.etl.common.language.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="11" />
    <devkit ref="2677cb18-f558-4e33-bc38-a5139cee06dc(jetbrains.mps.devkit.language-design)" />
  </languages>
  <imports>
    <import index="opwy" ref="r:3285c8a9-ec6d-4747-9589-295026f6398c(org.clawiz.etl.common.language.structure)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="1140524381322" name="jetbrains.mps.lang.editor.structure.CellModel_ListWithRole" flags="ng" index="2czfm3">
        <child id="1140524464360" name="cellLayout" index="2czzBx" />
        <child id="928328222691832421" name="separatorTextQuery" index="2gpyvW" />
      </concept>
      <concept id="1106270549637" name="jetbrains.mps.lang.editor.structure.CellLayout_Horizontal" flags="nn" index="2iRfu4" />
      <concept id="1106270571710" name="jetbrains.mps.lang.editor.structure.CellLayout_Vertical" flags="nn" index="2iRkQZ" />
      <concept id="1237303669825" name="jetbrains.mps.lang.editor.structure.CellLayout_Indent" flags="nn" index="l2Vlx" />
      <concept id="1237307900041" name="jetbrains.mps.lang.editor.structure.IndentLayoutIndentStyleClassItem" flags="ln" index="lj46D" />
      <concept id="709996738298806197" name="jetbrains.mps.lang.editor.structure.QueryFunction_SeparatorText" flags="in" index="2o9xnK" />
      <concept id="1237385578942" name="jetbrains.mps.lang.editor.structure.IndentLayoutOnNewLineStyleClassItem" flags="ln" index="pVoyu" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="1239814640496" name="jetbrains.mps.lang.editor.structure.CellLayout_VerticalGrid" flags="nn" index="2EHx9g" />
      <concept id="1186414536763" name="jetbrains.mps.lang.editor.structure.BooleanStyleSheetItem" flags="ln" index="VOi$J">
        <property id="1186414551515" name="flag" index="VOm3f" />
      </concept>
      <concept id="1186414928363" name="jetbrains.mps.lang.editor.structure.SelectableStyleSheetItem" flags="ln" index="VPM3Z" />
      <concept id="1088013125922" name="jetbrains.mps.lang.editor.structure.CellModel_RefCell" flags="sg" stub="730538219795941030" index="1iCGBv">
        <child id="1088186146602" name="editorComponent" index="1sWHZn" />
      </concept>
      <concept id="1088185857835" name="jetbrains.mps.lang.editor.structure.InlineEditorComponent" flags="ig" index="1sVBvm" />
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <property id="1214560368769" name="emptyNoTargetText" index="39s7Ar" />
        <property id="1140017977771" name="readOnly" index="1Intyy" />
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1073389577007" name="text" index="3F0ifm" />
      </concept>
      <concept id="1073389658414" name="jetbrains.mps.lang.editor.structure.CellModel_Property" flags="sg" stub="730538219796134133" index="3F0A7n" />
      <concept id="1219418625346" name="jetbrains.mps.lang.editor.structure.IStyleContainer" flags="ng" index="3F0Thp">
        <child id="1219418656006" name="styleItem" index="3F10Kt" />
      </concept>
      <concept id="1073389882823" name="jetbrains.mps.lang.editor.structure.CellModel_RefNode" flags="sg" stub="730538219795960754" index="3F1sOY" />
      <concept id="1073390211982" name="jetbrains.mps.lang.editor.structure.CellModel_RefNodeList" flags="sg" stub="2794558372793454595" index="3F2HdR" />
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
    </language>
  </registry>
  <node concept="24kQdi" id="2zqjo15lxMB">
    <property role="3GE5qa" value="connection" />
    <ref role="1XX52x" to="opwy:2zqjo15lxLA" resolve="JDBCConnection" />
    <node concept="3EZMnI" id="2zqjo15lxMH" role="2wV5jI">
      <node concept="3F0ifn" id="2zqjo15lxMO" role="3EZMnx">
        <property role="3F0ifm" value="JDBC connection" />
      </node>
      <node concept="3F0A7n" id="2zqjo15lxMU" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="2zqjo15lxN2" role="3EZMnx">
        <node concept="lj46D" id="2zqjo15lxN7" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="2zqjo15lxNc" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="2zqjo15lxNg" role="3EZMnx">
        <property role="3F0ifm" value="url      " />
        <node concept="lj46D" id="2zqjo15lxNh" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="2zqjo15lxNi" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="2zqjo15lxP7" role="3EZMnx">
        <property role="39s7Ar" value="true" />
        <ref role="1NtTu8" to="opwy:2zqjo15lxLB" resolve="url" />
      </node>
      <node concept="3F0ifn" id="2zqjo15lxNU" role="3EZMnx">
        <property role="3F0ifm" value="username " />
        <node concept="lj46D" id="2zqjo15lxNV" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="2zqjo15lxNW" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="2zqjo15lxQV" role="3EZMnx">
        <property role="39s7Ar" value="true" />
        <ref role="1NtTu8" to="opwy:2zqjo15lxLD" resolve="username" />
      </node>
      <node concept="3F0ifn" id="2zqjo15lxNr" role="3EZMnx">
        <property role="3F0ifm" value="password " />
        <node concept="lj46D" id="2zqjo15lxNs" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="2zqjo15lxNt" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="2zqjo15lxR_" role="3EZMnx">
        <property role="39s7Ar" value="true" />
        <ref role="1NtTu8" to="opwy:2zqjo15lxLG" resolve="password" />
      </node>
      <node concept="3F0ifn" id="2zqjo15lxND" role="3EZMnx">
        <node concept="lj46D" id="2zqjo15lxNE" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="2zqjo15lxNF" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="l2Vlx" id="2zqjo15lxMK" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="2zqjo15lxRV">
    <property role="3GE5qa" value="structure" />
    <ref role="1XX52x" to="opwy:2zqjo15lxLn" resolve="Structure" />
    <node concept="3EZMnI" id="2zqjo15lxRX" role="2wV5jI">
      <node concept="3F0ifn" id="2zqjo15lxS4" role="3EZMnx">
        <property role="3F0ifm" value="Structure" />
      </node>
      <node concept="3F0A7n" id="2zqjo15lxSg" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="2zqjo15lxSA" role="3EZMnx">
        <node concept="lj46D" id="2zqjo15lxSB" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="2zqjo15lxSC" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="2zqjo15lxSL" role="3EZMnx">
        <property role="3F0ifm" value="fields    " />
        <node concept="lj46D" id="2zqjo15lxSM" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="2zqjo15lxSN" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="2zqjo15lxVL" role="3EZMnx">
        <ref role="1NtTu8" to="opwy:2zqjo15lxLt" resolve="fields" />
        <node concept="2EHx9g" id="2zqjo15lxY7" role="2czzBx" />
        <node concept="VPM3Z" id="2zqjo15lxVP" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
      </node>
      <node concept="3F0ifn" id="2zqjo15lxSZ" role="3EZMnx">
        <node concept="lj46D" id="2zqjo15lxT0" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="2zqjo15lxT1" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="2zqjo15lxTg" role="3EZMnx">
        <property role="3F0ifm" value="record id " />
        <node concept="lj46D" id="2zqjo15lxTh" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="2zqjo15lxTi" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="1iCGBv" id="fPT0jCslsr" role="3EZMnx">
        <ref role="1NtTu8" to="opwy:fPT0jCslrT" resolve="recordIdField" />
        <node concept="1sVBvm" id="fPT0jCslst" role="1sWHZn">
          <node concept="3F0A7n" id="fPT0jCslt1" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="2zqjo15lxT$" role="3EZMnx">
        <node concept="lj46D" id="2zqjo15lxT_" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="2zqjo15lxTA" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="2zqjo15lxTV" role="3EZMnx">
        <node concept="lj46D" id="2zqjo15lxTW" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="2zqjo15lxTX" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="2zqjo15lxUl" role="3EZMnx">
        <node concept="lj46D" id="2zqjo15lxUm" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="2zqjo15lxUn" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="2zqjo15lxUM" role="3EZMnx">
        <node concept="lj46D" id="2zqjo15lxUN" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="2zqjo15lxUO" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="l2Vlx" id="2zqjo15lxS0" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="2zqjo15lxWZ">
    <property role="3GE5qa" value="structure.field" />
    <ref role="1XX52x" to="opwy:2zqjo15lxLq" resolve="StructureField" />
    <node concept="3EZMnI" id="2zqjo15lxX1" role="2wV5jI">
      <node concept="3F0A7n" id="2zqjo15lxX8" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F1sOY" id="2zqjo15lxXe" role="3EZMnx">
        <ref role="1NtTu8" to="opwy:6porqNrmPQf" resolve="valueType" />
      </node>
      <node concept="3F0ifn" id="2zqjo15lxXw" role="3EZMnx">
        <property role="3F0ifm" value="&quot;" />
      </node>
      <node concept="3F0A7n" id="2zqjo15lxXm" role="3EZMnx">
        <property role="39s7Ar" value="true" />
        <ref role="1NtTu8" to="opwy:5teVQz_ANmH" resolve="description" />
      </node>
      <node concept="3F0ifn" id="2zqjo15lxY0" role="3EZMnx">
        <property role="3F0ifm" value="&quot;" />
      </node>
      <node concept="2iRfu4" id="2zqjo15lxX4" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="2zqjo15lxYa">
    <property role="3GE5qa" value="job" />
    <ref role="1XX52x" to="opwy:2zqjo15lxLa" resolve="Job" />
    <node concept="3EZMnI" id="2zqjo15lxYc" role="2wV5jI">
      <node concept="3F0ifn" id="2zqjo15lxYj" role="3EZMnx">
        <property role="3F0ifm" value="Job" />
      </node>
      <node concept="3F0A7n" id="2zqjo15lxYp" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="2zqjo15lxYA" role="3EZMnx">
        <node concept="lj46D" id="2zqjo15lxYF" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="2zqjo15lxYK" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="2zqjo15lxZc" role="3EZMnx">
        <property role="3F0ifm" value="steps" />
        <node concept="pVoyu" id="2zqjo15lxZe" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="3SDF_NZCPGu" role="3EZMnx">
        <node concept="lj46D" id="3SDF_NZCPGQ" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="3SDF_NZCPGV" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="3AEsidS4E32" role="3EZMnx">
        <ref role="1NtTu8" to="opwy:2zqjo15lxM_" resolve="steps" />
        <node concept="2EHx9g" id="3AEsidS4E4l" role="2czzBx" />
        <node concept="VPM3Z" id="3AEsidS4E36" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
        <node concept="lj46D" id="3SDF_NZCU5I" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="3SDF_NZCU5Q" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="2o9xnK" id="3SDF_NZCJyd" role="2gpyvW">
          <node concept="3clFbS" id="3SDF_NZCJye" role="2VODD2">
            <node concept="3cpWs6" id="3SDF_NZCJEA" role="3cqZAp">
              <node concept="Xl_RD" id="3SDF_NZCJVq" role="3cqZAk">
                <property role="Xl_RC" value="" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="2zqjo15lxZn" role="3EZMnx">
        <node concept="lj46D" id="2zqjo15lxZo" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="2zqjo15lxZp" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="2zqjo15lxZ_" role="3EZMnx">
        <node concept="lj46D" id="2zqjo15lxZA" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="2zqjo15lxZB" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="l2Vlx" id="2zqjo15lxYf" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="2zqjo15ly4T">
    <property role="3GE5qa" value="job.step.action.extract.table" />
    <ref role="1XX52x" to="opwy:2zqjo15lxLK" resolve="ExtractTableDataJobStepAction" />
    <node concept="3EZMnI" id="2zqjo15ly4V" role="2wV5jI">
      <node concept="3F0ifn" id="2zqjo15ly52" role="3EZMnx">
        <property role="3F0ifm" value="extract table data" />
      </node>
      <node concept="3EZMnI" id="2zqjo15ly5t" role="3EZMnx">
        <node concept="VPM3Z" id="2zqjo15ly5v" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
        <node concept="3EZMnI" id="2zqjo15ly5C" role="3EZMnx">
          <node concept="VPM3Z" id="2zqjo15ly5E" role="3F10Kt">
            <property role="VOm3f" value="false" />
          </node>
          <node concept="3F0ifn" id="2zqjo15ly6o" role="3EZMnx">
            <property role="3F0ifm" value="connection" />
          </node>
          <node concept="1iCGBv" id="2zqjo15ly6E" role="3EZMnx">
            <ref role="1NtTu8" to="opwy:2zqjo15lxLL" resolve="connection" />
            <node concept="1sVBvm" id="2zqjo15ly6G" role="1sWHZn">
              <node concept="3F0A7n" id="2zqjo15ly6O" role="2wV5jI">
                <property role="1Intyy" value="true" />
                <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
              </node>
            </node>
          </node>
          <node concept="2iRfu4" id="2zqjo15ly5H" role="2iSdaV" />
        </node>
        <node concept="3EZMnI" id="2zqjo15ly5W" role="3EZMnx">
          <node concept="VPM3Z" id="2zqjo15ly5X" role="3F10Kt">
            <property role="VOm3f" value="false" />
          </node>
          <node concept="3F0ifn" id="2zqjo15ly6u" role="3EZMnx">
            <property role="3F0ifm" value="table     " />
          </node>
          <node concept="3F0A7n" id="2zqjo15ly6U" role="3EZMnx">
            <ref role="1NtTu8" to="opwy:2zqjo15lxLU" resolve="sourceTableName" />
          </node>
          <node concept="2iRfu4" id="2zqjo15ly5Z" role="2iSdaV" />
        </node>
        <node concept="3EZMnI" id="2zqjo15ly68" role="3EZMnx">
          <node concept="VPM3Z" id="2zqjo15ly69" role="3F10Kt">
            <property role="VOm3f" value="false" />
          </node>
          <node concept="3F0ifn" id="2zqjo15ly6x" role="3EZMnx">
            <property role="3F0ifm" value="structure " />
          </node>
          <node concept="1iCGBv" id="2zqjo15ly71" role="3EZMnx">
            <ref role="1NtTu8" to="opwy:2zqjo15lxLR" resolve="structure" />
            <node concept="1sVBvm" id="2zqjo15ly73" role="1sWHZn">
              <node concept="3F0A7n" id="2zqjo15ly7b" role="2wV5jI">
                <property role="1Intyy" value="true" />
                <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
              </node>
            </node>
          </node>
          <node concept="2iRfu4" id="2zqjo15ly6b" role="2iSdaV" />
        </node>
        <node concept="2iRkQZ" id="2zqjo15ly5y" role="2iSdaV" />
      </node>
      <node concept="2iRfu4" id="2zqjo15ly4Y" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="2zqjo15ly7m">
    <property role="3GE5qa" value="job.step.action.extract.type" />
    <ref role="1XX52x" to="opwy:2zqjo15ly4H" resolve="ExtractTypeDataJobStepAction" />
    <node concept="3EZMnI" id="2zqjo15ly7o" role="2wV5jI">
      <node concept="3F0ifn" id="2zqjo15ly7p" role="3EZMnx">
        <property role="3F0ifm" value="extract type data" />
      </node>
      <node concept="3EZMnI" id="2zqjo15ly7q" role="3EZMnx">
        <node concept="VPM3Z" id="2zqjo15ly7r" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
        <node concept="3EZMnI" id="2zqjo15ly7s" role="3EZMnx">
          <node concept="VPM3Z" id="2zqjo15ly7t" role="3F10Kt">
            <property role="VOm3f" value="false" />
          </node>
          <node concept="3F0ifn" id="2zqjo15ly7u" role="3EZMnx">
            <property role="3F0ifm" value="connection" />
          </node>
          <node concept="1iCGBv" id="2zqjo15ly7v" role="3EZMnx">
            <ref role="1NtTu8" to="opwy:2zqjo15ly4I" resolve="connection" />
            <node concept="1sVBvm" id="2zqjo15ly7w" role="1sWHZn">
              <node concept="3F0A7n" id="2zqjo15ly7x" role="2wV5jI">
                <property role="1Intyy" value="true" />
                <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
              </node>
            </node>
          </node>
          <node concept="2iRfu4" id="2zqjo15ly7y" role="2iSdaV" />
        </node>
        <node concept="3EZMnI" id="2zqjo15ly7z" role="3EZMnx">
          <node concept="VPM3Z" id="2zqjo15ly7$" role="3F10Kt">
            <property role="VOm3f" value="false" />
          </node>
          <node concept="3F0ifn" id="2zqjo15ly7_" role="3EZMnx">
            <property role="3F0ifm" value="type      " />
          </node>
          <node concept="1iCGBv" id="2zqjo15ly8e" role="3EZMnx">
            <ref role="1NtTu8" to="opwy:2zqjo15ly4J" resolve="sourceType" />
            <node concept="1sVBvm" id="2zqjo15ly8g" role="1sWHZn">
              <node concept="3F0A7n" id="2zqjo15ly8o" role="2wV5jI">
                <property role="1Intyy" value="true" />
                <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
              </node>
            </node>
          </node>
          <node concept="2iRfu4" id="2zqjo15ly7B" role="2iSdaV" />
        </node>
        <node concept="3EZMnI" id="2zqjo15ly7C" role="3EZMnx">
          <node concept="VPM3Z" id="2zqjo15ly7D" role="3F10Kt">
            <property role="VOm3f" value="false" />
          </node>
          <node concept="3F0ifn" id="2zqjo15ly7E" role="3EZMnx">
            <property role="3F0ifm" value="structure " />
          </node>
          <node concept="1iCGBv" id="2zqjo15ly8N" role="3EZMnx">
            <ref role="1NtTu8" to="opwy:2zqjo15ly8J" resolve="structure" />
            <node concept="1sVBvm" id="2zqjo15ly8P" role="1sWHZn">
              <node concept="3F0A7n" id="2zqjo15ly8X" role="2wV5jI">
                <property role="1Intyy" value="true" />
                <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
              </node>
            </node>
          </node>
          <node concept="2iRfu4" id="2zqjo15ly7I" role="2iSdaV" />
        </node>
        <node concept="2iRkQZ" id="2zqjo15ly7J" role="2iSdaV" />
      </node>
      <node concept="2iRfu4" id="2zqjo15ly7K" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="2zqjo15ly90">
    <property role="3GE5qa" value="job.step.action.load.type" />
    <ref role="1XX52x" to="opwy:2zqjo15lxM8" resolve="LoadTypeDataJobStepAction" />
    <node concept="3EZMnI" id="2zqjo15ly9N" role="2wV5jI">
      <node concept="3F0ifn" id="2zqjo15ly9U" role="3EZMnx">
        <property role="3F0ifm" value="load type data" />
      </node>
      <node concept="3EZMnI" id="2zqjo15lya0" role="3EZMnx">
        <node concept="VPM3Z" id="2zqjo15lya2" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
        <node concept="3EZMnI" id="2zqjo15lyaY" role="3EZMnx">
          <node concept="VPM3Z" id="2zqjo15lyb0" role="3F10Kt">
            <property role="VOm3f" value="false" />
          </node>
          <node concept="3F0ifn" id="2zqjo15lyb2" role="3EZMnx">
            <property role="3F0ifm" value="connection    " />
          </node>
          <node concept="1iCGBv" id="2zqjo15lyc6" role="3EZMnx">
            <ref role="1NtTu8" to="opwy:2zqjo15ly9F" resolve="connection" />
            <node concept="1sVBvm" id="2zqjo15lyc8" role="1sWHZn">
              <node concept="3F0A7n" id="2zqjo15lycg" role="2wV5jI">
                <property role="1Intyy" value="true" />
                <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
              </node>
            </node>
          </node>
          <node concept="2iRfu4" id="2zqjo15lyb3" role="2iSdaV" />
        </node>
        <node concept="3EZMnI" id="2zqjo15lybl" role="3EZMnx">
          <node concept="VPM3Z" id="2zqjo15lybm" role="3F10Kt">
            <property role="VOm3f" value="false" />
          </node>
          <node concept="3F0ifn" id="2zqjo15lybn" role="3EZMnx">
            <property role="3F0ifm" value="from structure" />
          </node>
          <node concept="2iRfu4" id="2zqjo15lybo" role="2iSdaV" />
          <node concept="1iCGBv" id="2zqjo15lycm" role="3EZMnx">
            <ref role="1NtTu8" to="opwy:2zqjo15ly9y" resolve="sourceStructure" />
            <node concept="1sVBvm" id="2zqjo15lyco" role="1sWHZn">
              <node concept="3F0A7n" id="2zqjo15lycw" role="2wV5jI">
                <property role="1Intyy" value="true" />
                <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3EZMnI" id="2zqjo15lyb$" role="3EZMnx">
          <node concept="VPM3Z" id="2zqjo15lyb_" role="3F10Kt">
            <property role="VOm3f" value="false" />
          </node>
          <node concept="3F0ifn" id="2zqjo15lybA" role="3EZMnx">
            <property role="3F0ifm" value="to type       " />
          </node>
          <node concept="1iCGBv" id="2zqjo15lydc" role="3EZMnx">
            <ref role="1NtTu8" to="opwy:2zqjo15lxMl" resolve="targetType" />
            <node concept="1sVBvm" id="2zqjo15lyde" role="1sWHZn">
              <node concept="3F0A7n" id="2zqjo15lydm" role="2wV5jI">
                <property role="1Intyy" value="true" />
                <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
              </node>
            </node>
          </node>
          <node concept="2iRfu4" id="2zqjo15lybB" role="2iSdaV" />
        </node>
        <node concept="2iRkQZ" id="2zqjo15lya5" role="2iSdaV" />
      </node>
      <node concept="2iRfu4" id="2zqjo15ly9Q" role="2iSdaV" />
      <node concept="3F0ifn" id="fPT0jCsIrz" role="3EZMnx">
        <property role="3F0ifm" value="     " />
      </node>
      <node concept="3EZMnI" id="fPT0jCsM41" role="3EZMnx">
        <node concept="VPM3Z" id="fPT0jCsM43" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
        <node concept="3EZMnI" id="fPT0jCsT6S" role="3EZMnx">
          <node concept="VPM3Z" id="fPT0jCsT6U" role="3F10Kt">
            <property role="VOm3f" value="false" />
          </node>
          <node concept="3F0ifn" id="fPT0jCsT6W" role="3EZMnx">
            <property role="3F0ifm" value="maps" />
          </node>
          <node concept="3F2HdR" id="fPT0jCsT77" role="3EZMnx">
            <ref role="1NtTu8" to="opwy:fPT0jCsM50" resolve="fieldMaps" />
            <node concept="2EHx9g" id="fPT0jCsT7n" role="2czzBx" />
            <node concept="VPM3Z" id="fPT0jCsT7b" role="3F10Kt">
              <property role="VOm3f" value="false" />
            </node>
          </node>
          <node concept="2iRfu4" id="fPT0jCsT6X" role="2iSdaV" />
        </node>
        <node concept="2iRkQZ" id="fPT0jCsM46" role="2iSdaV" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="2zqjo15lyhM">
    <property role="3GE5qa" value="job.step" />
    <ref role="1XX52x" to="opwy:2zqjo15lyhF" resolve="JobStep" />
    <node concept="3EZMnI" id="2zqjo15lyhO" role="2wV5jI">
      <node concept="3F0A7n" id="2zqjo15lyhV" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3EZMnI" id="2zqjo15lyi9" role="3EZMnx">
        <node concept="3F2HdR" id="2zqjo15lyii" role="3EZMnx">
          <ref role="1NtTu8" to="opwy:2zqjo15lxLj" resolve="previousSteps" />
          <node concept="2EHx9g" id="2zqjo15lyin" role="2czzBx" />
        </node>
        <node concept="2iRkQZ" id="2zqjo15lyic" role="2iSdaV" />
      </node>
      <node concept="2iRfu4" id="2zqjo15lyhR" role="2iSdaV" />
      <node concept="3F0ifn" id="3AEsidS50Hw" role="3EZMnx">
        <property role="3F0ifm" value=" -&gt; " />
      </node>
      <node concept="3EZMnI" id="2zqjo15lyiC" role="3EZMnx">
        <node concept="VPM3Z" id="2zqjo15lyiE" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
        <node concept="3EZMnI" id="2zqjo15lyiS" role="3EZMnx">
          <node concept="VPM3Z" id="2zqjo15lyiU" role="3F10Kt">
            <property role="VOm3f" value="false" />
          </node>
          <node concept="3F0ifn" id="2zqjo15lyj2" role="3EZMnx" />
          <node concept="3F2HdR" id="2zqjo15lyjg" role="3EZMnx">
            <ref role="1NtTu8" to="opwy:2zqjo15lyhJ" resolve="actions" />
            <node concept="2EHx9g" id="2zqjo15lyjp" role="2czzBx" />
            <node concept="VPM3Z" id="2zqjo15lyjk" role="3F10Kt">
              <property role="VOm3f" value="false" />
            </node>
          </node>
          <node concept="2iRfu4" id="2zqjo15lyiX" role="2iSdaV" />
        </node>
        <node concept="2iRkQZ" id="2zqjo15lyiH" role="2iSdaV" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="3AEsidS5gfY">
    <property role="3GE5qa" value="structure.valuetype" />
    <ref role="1XX52x" to="opwy:3AEsidS5gfP" resolve="StructureValueTypeDate" />
    <node concept="3EZMnI" id="3AEsidS5gg0" role="2wV5jI">
      <node concept="3F0ifn" id="3AEsidS5gg7" role="3EZMnx">
        <property role="3F0ifm" value="date" />
      </node>
      <node concept="2iRfu4" id="3AEsidS5gg3" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="3AEsidS5gga">
    <property role="3GE5qa" value="structure.valuetype" />
    <ref role="1XX52x" to="opwy:3AEsidS5gfR" resolve="StructureValueTypeDateTime" />
    <node concept="3EZMnI" id="3AEsidS5ggc" role="2wV5jI">
      <node concept="3F0ifn" id="3AEsidS5ggj" role="3EZMnx">
        <property role="3F0ifm" value="datetime" />
      </node>
      <node concept="2iRfu4" id="3AEsidS5ggf" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="3AEsidS5ggm">
    <property role="3GE5qa" value="structure.valuetype" />
    <ref role="1XX52x" to="opwy:3AEsidS5gfL" resolve="StructureValueTypeNumber" />
    <node concept="3EZMnI" id="3AEsidS5ggo" role="2wV5jI">
      <node concept="3F0ifn" id="3AEsidS5ggv" role="3EZMnx">
        <property role="3F0ifm" value="number" />
      </node>
      <node concept="2iRfu4" id="3AEsidS5ggr" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="3AEsidS5ggy">
    <property role="3GE5qa" value="structure.valuetype" />
    <ref role="1XX52x" to="opwy:3AEsidS5gfT" resolve="StructureValueTypeScaledNumber" />
    <node concept="3EZMnI" id="3AEsidS5ggB" role="2wV5jI">
      <node concept="3F0ifn" id="3AEsidS5ggD" role="3EZMnx">
        <property role="3F0ifm" value="number" />
      </node>
      <node concept="3F0ifn" id="3AEsidS5ggL" role="3EZMnx">
        <property role="3F0ifm" value="(" />
      </node>
      <node concept="3F0A7n" id="3AEsidS5ggT" role="3EZMnx">
        <ref role="1NtTu8" to="opwy:3AEsidS5gfV" resolve="precision" />
      </node>
      <node concept="3F0ifn" id="3AEsidS5gh3" role="3EZMnx">
        <property role="3F0ifm" value="," />
      </node>
      <node concept="3F0A7n" id="3AEsidS5ghm" role="3EZMnx">
        <ref role="1NtTu8" to="opwy:3AEsidS5gfU" resolve="scale" />
      </node>
      <node concept="3F0ifn" id="3AEsidS5gh$" role="3EZMnx">
        <property role="3F0ifm" value=")" />
      </node>
      <node concept="2iRfu4" id="3AEsidS5ggE" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="3AEsidS5jRa">
    <property role="3GE5qa" value="structure.valuetype" />
    <ref role="1XX52x" to="opwy:3AEsidS5jQY" resolve="StructureValueTypeStructureReference" />
    <node concept="3EZMnI" id="3AEsidS5jRc" role="2wV5jI">
      <node concept="3F0ifn" id="3AEsidS5jRj" role="3EZMnx">
        <property role="3F0ifm" value="-&gt;" />
      </node>
      <node concept="1iCGBv" id="3AEsidS5jRp" role="3EZMnx">
        <ref role="1NtTu8" to="opwy:3AEsidS5jR0" resolve="referencedStructure" />
        <node concept="1sVBvm" id="3AEsidS5jRr" role="1sWHZn">
          <node concept="3F0A7n" id="3AEsidS5jRz" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="2iRfu4" id="3AEsidS5jRf" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="3AEsidS5nFY">
    <property role="3GE5qa" value="job.step" />
    <ref role="1XX52x" to="opwy:2zqjo15lxLg" resolve="JobStepRef" />
    <node concept="3EZMnI" id="3AEsidS5nG0" role="2wV5jI">
      <node concept="1iCGBv" id="3AEsidS5nGh" role="3EZMnx">
        <ref role="1NtTu8" to="opwy:2zqjo15lxLh" resolve="jobStep" />
        <node concept="1sVBvm" id="3AEsidS5nGj" role="1sWHZn">
          <node concept="3F0A7n" id="3AEsidS5nGq" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="2iRfu4" id="3AEsidS5nG3" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="fPT0jCsM5i">
    <property role="3GE5qa" value="job.step.action.load.type" />
    <ref role="1XX52x" to="opwy:fPT0jCsM4K" resolve="TypeDataLoadFieldMap" />
    <node concept="3EZMnI" id="fPT0jCsM5k" role="2wV5jI">
      <node concept="1iCGBv" id="fPT0jCsM5r" role="3EZMnx">
        <ref role="1NtTu8" to="opwy:fPT0jCsM55" resolve="sourceField" />
        <node concept="1sVBvm" id="fPT0jCsM5t" role="1sWHZn">
          <node concept="3F0A7n" id="fPT0jCsM5B" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="fPT0jCsM5J" role="3EZMnx">
        <property role="3F0ifm" value="-&gt;" />
      </node>
      <node concept="1iCGBv" id="fPT0jCsM5V" role="3EZMnx">
        <ref role="1NtTu8" to="opwy:fPT0jCsM57" resolve="targetField" />
        <node concept="1sVBvm" id="fPT0jCsM5X" role="1sWHZn">
          <node concept="3F0A7n" id="fPT0jCsM68" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="2iRfu4" id="fPT0jCsM5n" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="fPT0jCtrof">
    <property role="3GE5qa" value="structure.valuetype" />
    <ref role="1XX52x" to="opwy:3AEsidS5gfG" resolve="StructureValueTypeString" />
    <node concept="3EZMnI" id="fPT0jCtroh" role="2wV5jI">
      <node concept="3F0ifn" id="fPT0jCtroo" role="3EZMnx">
        <property role="3F0ifm" value="string(" />
      </node>
      <node concept="3F0A7n" id="fPT0jCtrou" role="3EZMnx">
        <ref role="1NtTu8" to="opwy:6porqNrmRYT" resolve="length" />
      </node>
      <node concept="3F0ifn" id="fPT0jCtroA" role="3EZMnx">
        <property role="3F0ifm" value=")" />
      </node>
      <node concept="2iRfu4" id="fPT0jCtrok" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="4M5nQGohBa0">
    <property role="3GE5qa" value="job.step.action.load.table" />
    <ref role="1XX52x" to="opwy:2zqjo15lxLW" resolve="LoadTableDataJobStepAction" />
    <node concept="3EZMnI" id="4M5nQGohBa2" role="2wV5jI">
      <node concept="3F0ifn" id="4M5nQGohBa9" role="3EZMnx">
        <property role="3F0ifm" value="load table data" />
      </node>
      <node concept="2iRfu4" id="4M5nQGohBa5" role="2iSdaV" />
    </node>
  </node>
</model>

