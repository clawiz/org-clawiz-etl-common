/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.etl.common.generator.structure.component.row;

import org.clawiz.etl.common.metadata.data.structure.field.StructureField;
import org.clawiz.etl.common.generator.structure.component.AbstractStructureGeneratorJavaClassComponent;
import org.clawiz.etl.common.manager.dataset.AbstractDataSetRow;

public class StructureRowPrototypeComponent extends AbstractStructureGeneratorJavaClassComponent {


    public void addImports() {

    }

    public void addVariables() {

        for (StructureField field : getStructure().getFields() ) {

            addImport(field.getValueType().getJavaClass());
            addVariable(field.getValueType().getJavaClass().getName(), field.getJavaVariableName(), null, true, true);

        }

    }


    @Override
    public void process() {
        super.process();

        setName(getGenerator().getComponentByClass(StructureRowComponent.class).getName() + "Prototype");

        setExtends(AbstractDataSetRow.class.getName());

        addImports();

        addVariables();

    }

}
