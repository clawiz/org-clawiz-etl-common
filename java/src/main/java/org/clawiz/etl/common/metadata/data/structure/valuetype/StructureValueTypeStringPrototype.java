package org.clawiz.etl.common.metadata.data.structure.valuetype;

import java.math.BigDecimal;
import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class StructureValueTypeStringPrototype extends org.clawiz.etl.common.metadata.data.structure.valuetype.AbstractStructureValueType {
    
    @ExchangeAttribute
    private BigDecimal length;
    
    public StructureValueTypeString withName(String value) {
        setName(value);
        return (StructureValueTypeString) this;
    }
    
    public BigDecimal getLength() {
        return this.length;
    }
    
    public void setLength(BigDecimal value) {
        this.length = value;
    }
    
    public StructureValueTypeString withLength(BigDecimal value) {
        setLength(value);
        return (StructureValueTypeString) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
