/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.etl.common.metadata.data.job;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.metadata.install.AbstractMetadataNodeInstaller;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.etl.common.metadata.data.job.step.JobStep;
import org.clawiz.etl.common.metadata.data.job.step.JobStepList;
import org.clawiz.etl.common.metadata.data.job.step.action.AbstractJobStepAction;
import org.clawiz.etl.common.metadata.data.structure.Structure;
import org.clawiz.etl.common.install.job.JobInstaller;
import org.clawiz.etl.common.manager.dataset.AbstractDataSet;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

public class Job extends JobPrototype {

    @Override
    public <T extends AbstractMetadataNodeInstaller> Class<T> getInstallerClass() {
        return (Class<T>) JobInstaller.class;
    }

    public JobStep getStep(String stepName) {
        return getStep(stepName, true);
    }

    public JobStep getStep(String stepName, boolean throwNotFoundError) {
        JobStep step = getSteps().get(stepName);
        if ( step != null || ! throwNotFoundError ) {
            return step;
        }
        throw new CoreException("Step '?' not found in job '?'", stepName, getFullName());
    }

    ConcurrentHashMap<Class, ArrayList<AbstractJobStepAction>> loadDataSetClassActionsCache = new ConcurrentHashMap<>();

    public <T extends AbstractDataSet> ArrayList<AbstractJobStepAction> getLoadDataSetClassActions(Class<T> dataSetClass) {

        ArrayList<AbstractJobStepAction> result = loadDataSetClassActionsCache.get(dataSetClass);
        if ( result != null ) {
            return result;
        }
        result = new ArrayList<>();

        for (JobStep step : getSteps() ) {
            for ( AbstractJobStepAction action : step.getActions() ) {
                for ( Structure structure : action.getProcessStructures() ) {
                    if ( dataSetClass.getName().equals(structure.getDataSetClassName())) {
                        result.add(action);
                    }
                }
            }
        }


        loadDataSetClassActionsCache.put(dataSetClass, result);

        return result;
    }

    public <T extends AbstractDataSet> ArrayList<AbstractJobStepAction> getLoadDataSetActions(T dataSet) {
        return getLoadDataSetClassActions(dataSet.getClass());
    }

}
