package org.clawiz.etl.common.metadata.data.job.step.action.extract;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class AbstractExtractJobStepActionPrototype extends org.clawiz.etl.common.metadata.data.job.step.action.AbstractJobStepAction {
    
    public AbstractExtractJobStepAction withName(String value) {
        setName(value);
        return (AbstractExtractJobStepAction) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
