/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.etl.common.generator.service.component.step.service;

import org.clawiz.core.common.system.generator.java.component.element.AbstractJavaClassElement;
import org.clawiz.core.common.system.generator.java.component.element.JavaMemberElement;
import org.clawiz.core.common.system.generator.java.component.element.JavaVariableElement;
import org.clawiz.etl.common.metadata.data.connection.AbstractConnection;
import org.clawiz.etl.common.metadata.data.job.step.action.AbstractJobStepAction;
import org.clawiz.etl.common.generator.service.component.step.AbstractJobGeneratorStepJavaClassComponent;
import org.clawiz.etl.common.generator.service.component.step.service.element.JobStepServiceProcessActionDispatcherMethodElement;
import org.clawiz.etl.common.generator.service.component.step.service.element.action.AbstractJobStepServicePrototypeActionMethodElement;
import org.clawiz.etl.common.manager.instance.step.AbstractJobStepInstance;
import org.clawiz.etl.common.manager.instance.step.action.JobStepActionInstance;
import org.clawiz.etl.common.manager.service.step.AbstractJobStepService;

import java.util.HashMap;

public class JobStepServicePrototypeComponent extends AbstractJobGeneratorStepJavaClassComponent {

    protected void addImports() {
        addImport(JobStepActionInstance.class);
    }

    AbstractJavaClassElement connectionVariablesBlock;

    private void addVariables() {

        connectionVariablesBlock = addElement(AbstractJavaClassElement.class);
        connectionVariablesBlock.setText("");
        connectionVariablesBlock.setLpad("");

    }


    HashMap<String, String> connectionToVariablesCache   = new HashMap<>();
    HashMap<String, String>  connectionVariableNamesCache = new HashMap<>();
    public String getConnectionVariableName(AbstractConnection connection) {
        String key          = connection.getFullName();
        String variableName = connectionToVariablesCache.get(key);
        if ( variableName != null ) {
            return variableName;
        }
        variableName = connection.getJavaVariableName();
        int index    = 2;
        while ( connectionVariableNamesCache.containsKey(variableName) ) {
            variableName = connection.getJavaVariableName() + (index++);
        }

        JavaVariableElement variableElement = connectionVariablesBlock.addElement(JavaVariableElement.class);
        variableElement.setType(connection.getPackageName() + "." + connection.getJavaVariableName().toLowerCase() + "." + connection.getJavaClassName());
        variableElement.setName(variableName);
        variableElement.setAccessLevel(JavaMemberElement.AccessLevel.PROTECTED);


        connectionToVariablesCache.put(key, variableName);
        connectionVariableNamesCache.put(variableName, variableName);
        return variableName;
    }


    @Override
    public void process() {
        super.process();

        setName(getServiceClassName() + "Prototype");

        setExtends(AbstractJobStepService.class.getName() + "<" + getInstanceClassName() + ">" );

        addImports();

        addVariables();

        for ( AbstractJobStepAction action : getStep().getActions() ) {
            Class clazz = getGenerator().getActionMap(action.getClass());
            if ( clazz == null ) {
                throwException("Job ? step ? action map not defined for ?", getStep().getJob().getFullName(), getStep().getName(), action.getClass().getName());
            }
            AbstractJobStepServicePrototypeActionMethodElement actionMethodElement = (AbstractJobStepServicePrototypeActionMethodElement) addElement(clazz);
            actionMethodElement.setAction(action);

        }

        addElement(JobStepServiceProcessActionDispatcherMethodElement.class);

    }

}
