package org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.field;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsStructureFieldPrototype extends AbstractMpsNode {
    
    public String description;
    
    public org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.valuetype.MpsAbstractStructureValueType valueType;
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.description = null;
        }
        this.description = value;
    }
    
    public org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.valuetype.MpsAbstractStructureValueType getValueType() {
        return this.valueType;
    }
    
    public void setValueType(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.valuetype.MpsAbstractStructureValueType value) {
        this.valueType = value;
    }
    
    public String getLanguageId() {
        return "6d27b471-0525-4d4c-bf85-b4bd447d6edd";
    }
    
    public String getLanguageName() {
        return "org.clawiz.etl.common.language";
    }
    
    public String getLanguageConceptId() {
        return "2943750519754464346";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.etl.common.language.structure.StructureField";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("6d27b471-0525-4d4c-bf85-b4bd447d6edd", "org.clawiz.etl.common.language", "2943750519754464346", "org.clawiz.etl.common.language.structure.StructureField", ConceptPropertyType.PROPERTY, "6291228963290953133", "description"));
        result.add(new ConceptProperty("6d27b471-0525-4d4c-bf85-b4bd447d6edd", "org.clawiz.etl.common.language", "2943750519754464346", "org.clawiz.etl.common.language.structure.StructureField", ConceptPropertyType.CHILD, "7374764979001187727", "valueType"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        addConceptNodeProperty("2943750519754464346", "description", getDescription());
        addConceptNodeChild("2943750519754464346", "valueType", getValueType());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.etl.common.metadata.data.structure.field.StructureField.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.etl.common.metadata.data.structure.field.StructureField structure = (org.clawiz.etl.common.metadata.data.structure.field.StructureField) node;
        
        structure.setDescription(getDescription());
        
        if ( getValueType() != null ) {
            structure.setValueType((org.clawiz.etl.common.metadata.data.structure.valuetype.AbstractStructureValueType) getValueType().toMetadataNode(structure, "valueType"));
        } else {
            structure.setValueType(null);
        }
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.etl.common.metadata.data.structure.field.StructureField structure = (org.clawiz.etl.common.metadata.data.structure.field.StructureField) node;
        
        setDescription(structure.getDescription());
        
        if ( structure.getValueType() != null ) {
            setValueType(loadChildMetadataNode(structure.getValueType()));
        } else {
            setValueType(null);
        }
        
    }
}
