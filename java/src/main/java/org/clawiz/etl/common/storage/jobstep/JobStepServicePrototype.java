package org.clawiz.etl.common.storage.jobstep;

import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.storage.type.TypeService;
import java.math.BigDecimal;
import org.clawiz.core.common.utils.date.DateUtils;
import java.util.Date;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.session.transaction.TransactionAction;
import org.clawiz.core.common.system.session.transaction.TransactionActionType;

public class JobStepServicePrototype extends AbstractTypeService<JobStepObject> {
    
    private static BigDecimal _typeId;
    
    public void init() {
        
        super.init();
        
        if ( _typeId == null ) {
            _typeId = getService(TypeService.class).packageNameToId("org.clawiz.etl.common.storage", "JobStep");
            if ( _typeId == null ) { 
                throwException("Type '?' not registered in database", new Object[]{"org.clawiz.etl.common.storage.JobStep"});
            }
        }
        
        setTypeId(_typeId);
    }
    
    /**
    * Fill object default values and check data consistency
    * 
    * @param      jobStep Checked object
    */
    public void check(JobStepObject jobStep) {
        
        if ( jobStep == null ) {
            throwException("Cannot check null ?", new Object[]{"JobStepObject"});
        }
        
        jobStep.fillDefaults();
        
        
        if ( jobNameToId(jobStep.getJobId(), jobStep.getName(), jobStep.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "JobStep", jobStep.toJobName() });
        }
        
    }
    
    /**
    * Create new JobStepObject instance and fill default values
    * 
    * @return     Created object
    */
    public JobStepObject create() {
        
        JobStepObject jobStep = new JobStepObject();
        jobStep.setService((JobStepService) this);
        
        jobStep.fillDefaults();
        
        return jobStep;
    }
    
    /**
    * Load object from database by record id
    * 
    * @param      id Id of loaded record
    * @return     Loaded object
    */
    public JobStepObject load(BigDecimal id) {
        
        Statement statement = executeQuery("select job_id, name, description, active from cw_etl_job_steps where id = ?", id);
        if ( ! statement.next() ) { 
            statement.close();
            throwException("? with id ? not found in database", new Object[]{"JobStep", id});
        }
        
        JobStepObject result = new JobStepObject();
        
        result.setService((JobStepService) this);
        result.setId(id);
        result.setJobId(statement.getBigDecimal(1));
        result.setName(statement.getString(2));
        result.setDescription(statement.getString(3));
        result.setActive(("T".equals(statement.getString(4))));
        
        statement.close();
        
        return result;
    }
    
    /**
    * Load list of JobStepObject from database
    * 
    * @param      whereClause Where part of SQL statement
    * @param      parameters  Where parameter values
    * @return     List of found objects
    */
    public JobStepList loadList(String whereClause, Object... parameters) {
        return loadOrderedList(whereClause, null, parameters);
    }
    
    /**
    * Load list of JobStepObject from database
    * 
    * @param      whereClause   Where part of SQL statement
    * @param      orderByClause Order by part of SQL statement
    * @param      parameters    Where parameter values
    * @return     List of found objects
    */
    public JobStepList loadOrderedList(String whereClause, String orderByClause, Object... parameters) {
        
        
        JobStepList result = new JobStepList();
        
        
        Statement statement = executeQuery("select id, job_id, name, description, active from cw_etl_job_steps"
                                                   + (whereClause   != null ? " where " + whereClause : "")
                                                   + (orderByClause != null ? " order by " + orderByClause : "")
                                                   , parameters);
        while ( statement.next() ) {
            JobStepObject object = new JobStepObject();
        
            object.setService((JobStepService) this);
            object.setId(statement.getBigDecimal(1));
            object.setJobId(statement.getBigDecimal(2));
            object.setName(statement.getString(3));
            object.setDescription(statement.getString(4));
            object.setActive(("T".equals(statement.getString(5))));
        
            result.add(object);
        }
        statement.close();
        
        return result;
    }
    
    /**
    * Find id of record by key 'JobName' fields
    * 
    * @param      jobId Job
    * @param      name  Name
    * @return     Id of found record or null
    */
    public BigDecimal jobNameToId(java.math.BigDecimal jobId, java.lang.String name) {
        return jobNameToId(jobId, name, null);
    }
    
    /**
    * Find id of record by key 'JobName' fields with id not equal skipId
    * 
    * @param      jobId  Job
    * @param      name   Name
    * @param      skipId Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal jobNameToId(java.math.BigDecimal jobId, java.lang.String name, BigDecimal skipId) {
        
        if ( jobId == null || name == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_etl_job_steps where job_id = ? and upper_name = ?", jobId, name.toUpperCase());
        } else {
            return executeQueryBigDecimal("select id from cw_etl_job_steps where job_id = ? and upper_name = ? and id != ?", jobId, name.toUpperCase(), skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'JobName' fields or create new record with values set to given parameters
    * 
    * @param      jobId               Job
    * @param      name                Name
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal jobNameToId(java.math.BigDecimal jobId, java.lang.String name, boolean createNewIfNotFound) {
        
        BigDecimal id = jobNameToId(jobId, name, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        JobStepObject object = create();
        object.setJobId(jobId);
        object.setName(name);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'JobName' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToJobName(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select job_id, name from cw_etl_job_steps where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        JobStepObject object = new JobStepObject();
        object.setService((JobStepService)this);
        object.setId(id);
        object.setJobId(statement.getBigDecimal(1));
        object.setName(statement.getString(2));
        
        statement.close();
        
        return object.toJobName();
    }
    
    /**
    * Load record by id and prepare toString() value
    * 
    * @param      id Id of record
    * @return     toString() value for record id
    */
    public String idToString(BigDecimal id) {
        return idToJobName(id);
    }
    
    /**
    * Return toString() value for object
    * 
    * @param      jobStepObject Object for toString transformation
    * @return     toString() value for object
    */
    public String objectToString(JobStepObject jobStepObject) {
        return jobStepObject.toJobName();
    }
    
    protected void saveAudit(TransactionAction transactionAction, JobStepObject oldJobStepObject, JobStepObject newJobStepObject) {
        
        JobStepObject o = oldJobStepObject != null ? oldJobStepObject : new JobStepObject();
        JobStepObject n = newJobStepObject != null ? newJobStepObject : new JobStepObject();
        
        
        executeUpdate("insert into a_cw_etl_job_steps (scn, action_type, id , o_job_id, o_name, o_upper_name, o_description, o_active, n_job_id, n_name, n_upper_name, n_description, n_active) values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", transactionAction.getScn(), transactionAction.getType().toString(), o.getId() != null ? o.getId() : n.getId(), o.getJobId(), o.getName(), o.getName() != null ? o.getName().toUpperCase() : null, o.getDescription(), o.isActive() ? "T" : "F", n.getJobId(), n.getName(), n.getName() != null ? n.getName().toUpperCase() : null, n.getDescription(), n.isActive() ? "T" : "F");
    }
    
    /**
    * Save object to database:
    *  - check data consistency with check() call
    *  - INSERT into database for newly created objects or
    *    UPDATE if object is loaded from existing database record
    * 
    * @param      jobStep Saved object
    */
    public void save(JobStepObject jobStep) {
        
        if ( jobStep == null ) {
            throwException("Cannot save NULL ?", new Object[]{"JobStepObject"});
        }
        
        TransactionAction transactionAction;
        JobStepObject oldJobStep;
        if ( jobStep.getService() == null ) {
            jobStep.setService((JobStepService)this);
        }
        
        check(jobStep);
        
        if ( jobStep.getId() == null ) {
        
            jobStep.setId(getObjectService().createObject(getTypeId()));
        
            oldJobStep = null;
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.NEW, jobStep.getId());
        
            executeUpdate("insert into cw_etl_job_steps" 
                          + "( id, job_id, name, upper_name, description, active ) "
                          + "values"
                          + "(?, ?, ?, ?, ?, ?)",
                          jobStep.getId(), jobStep.getJobId(), jobStep.getName(), ( jobStep.getName() != null ? jobStep.getName().toUpperCase() : null ), jobStep.getDescription(), jobStep.isActive() ? "T" : "F" );
        
            if ( jobStep.getJobId() != null ) { getObjectService().setLink(jobStep.getJobId(), jobStep.getId()); }
        
        } else {
        
            oldJobStep = load(jobStep.getId());
            if ( oldJobStep.equals(jobStep) ) { return; }
        
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.CHANGE, jobStep.getId());
        
            executeUpdate("update cw_etl_job_steps set "
                          + "job_id = ?, name = ?, upper_name = ?, description = ?, active = ? "
                          + "where id = ?",
                          jobStep.getJobId(), jobStep.getName(), ( jobStep.getName() != null ? jobStep.getName().toUpperCase() : null ), jobStep.getDescription(), jobStep.isActive() ? "T" : "F", jobStep.getId() );
        
            getObjectService().changeLinkParent(oldJobStep.getJobId(), jobStep.getJobId(), jobStep.getId());
        
        }
        
        saveAudit(transactionAction, oldJobStep, jobStep);
        
    }
    
    /**
    * Delete object from database
    * 
    * @param      id ID of deleted object
    */
    public void delete(BigDecimal id) {
        
        JobStepObject oldJobStepObject = load(id);
        
        TransactionAction transactionAction = getSession().newObjectTransactionAction(TransactionActionType.DELETE, id);
        saveAudit(transactionAction, oldJobStepObject, null);
        
        getObjectService().deleteObject(id);
        
        if (oldJobStepObject.getJobId() != null) { getObjectService().deleteLink(oldJobStepObject.getJobId(), id); }
        
        executeUpdate("delete from cw_etl_job_steps where id = ?", id);
        
    }
}
