package org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step.action.load.table;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsLoadTableDataJobStepActionPrototype extends org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step.action.load.MpsAbstractLoadJobStepAction {
    
    public String targetTableName;
    
    public org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.connection.MpsJDBCConnection connection;
    
    public org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.MpsStructure sourceStructure;
    
    public String getTargetTableName() {
        return this.targetTableName;
    }
    
    public void setTargetTableName(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.targetTableName = null;
        }
        this.targetTableName = value;
    }
    
    public org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.connection.MpsJDBCConnection getConnection() {
        return this.connection;
    }
    
    public void setConnection(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.connection.MpsJDBCConnection value) {
        this.connection = value;
    }
    
    public org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.MpsStructure getSourceStructure() {
        return this.sourceStructure;
    }
    
    public void setSourceStructure(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.MpsStructure value) {
        this.sourceStructure = value;
    }
    
    public String getLanguageId() {
        return "6d27b471-0525-4d4c-bf85-b4bd447d6edd";
    }
    
    public String getLanguageName() {
        return "org.clawiz.etl.common.language";
    }
    
    public String getLanguageConceptId() {
        return "2943750519754464380";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.etl.common.language.structure.LoadTableDataJobStepAction";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("6d27b471-0525-4d4c-bf85-b4bd447d6edd", "org.clawiz.etl.common.language", "2943750519754464380", "org.clawiz.etl.common.language.structure.LoadTableDataJobStepAction", ConceptPropertyType.PROPERTY, "2943750519754464381", "targetTableName"));
        result.add(new ConceptProperty("6d27b471-0525-4d4c-bf85-b4bd447d6edd", "org.clawiz.etl.common.language", "2943750519754464380", "org.clawiz.etl.common.language.structure.LoadTableDataJobStepAction", ConceptPropertyType.REFERENCE, "2943750519754464383", "connection"));
        result.add(new ConceptProperty("6d27b471-0525-4d4c-bf85-b4bd447d6edd", "org.clawiz.etl.common.language", "2943750519754464380", "org.clawiz.etl.common.language.structure.LoadTableDataJobStepAction", ConceptPropertyType.REFERENCE, "2943750519754464385", "sourceStructure"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeProperty("2943750519754464380", "targetTableName", getTargetTableName());
        addConceptNodeRef("2943750519754464380", "connection", getConnection());
        addConceptNodeRef("2943750519754464380", "sourceStructure", getSourceStructure());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.etl.common.metadata.data.job.step.action.load.table.LoadTableDataJobStepAction.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.etl.common.metadata.data.job.step.action.load.table.LoadTableDataJobStepAction structure = (org.clawiz.etl.common.metadata.data.job.step.action.load.table.LoadTableDataJobStepAction) node;
        
        structure.setTargetTableName(getTargetTableName());
        
        if ( getConnection() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "connection", false, getConnection());
        } else {
            structure.setConnection(null);
        }
        
        if ( getSourceStructure() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "sourceStructure", false, getSourceStructure());
        } else {
            structure.setSourceStructure(null);
        }
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
        addForeignKey(getConnection());
        addForeignKey(getSourceStructure());
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.etl.common.metadata.data.job.step.action.load.table.LoadTableDataJobStepAction structure = (org.clawiz.etl.common.metadata.data.job.step.action.load.table.LoadTableDataJobStepAction) node;
        
        setTargetTableName(structure.getTargetTableName());
        
        if ( structure.getConnection() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "connection", false, structure.getConnection());
        } else {
            setConnection(null);
        }
        
        if ( structure.getSourceStructure() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "sourceStructure", false, structure.getSourceStructure());
        } else {
            setSourceStructure(null);
        }
        
    }
}
