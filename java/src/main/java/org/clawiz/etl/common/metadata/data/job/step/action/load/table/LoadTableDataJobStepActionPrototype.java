package org.clawiz.etl.common.metadata.data.job.step.action.load.table;

import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class LoadTableDataJobStepActionPrototype extends org.clawiz.etl.common.metadata.data.job.step.action.load.AbstractLoadJobStepAction {
    
    @ExchangeAttribute
    private String targetTableName;
    
    @ExchangeReference
    private org.clawiz.etl.common.metadata.data.connection.JDBCConnection connection;
    
    @ExchangeReference
    private org.clawiz.etl.common.metadata.data.structure.Structure sourceStructure;
    
    public LoadTableDataJobStepAction withName(String value) {
        setName(value);
        return (LoadTableDataJobStepAction) this;
    }
    
    public String getTargetTableName() {
        return this.targetTableName;
    }
    
    public void setTargetTableName(String value) {
        this.targetTableName = value;
    }
    
    public LoadTableDataJobStepAction withTargetTableName(String value) {
        setTargetTableName(value);
        return (LoadTableDataJobStepAction) this;
    }
    
    public org.clawiz.etl.common.metadata.data.connection.JDBCConnection getConnection() {
        return this.connection;
    }
    
    public void setConnection(org.clawiz.etl.common.metadata.data.connection.JDBCConnection value) {
        this.connection = value;
    }
    
    public LoadTableDataJobStepAction withConnection(org.clawiz.etl.common.metadata.data.connection.JDBCConnection value) {
        setConnection(value);
        return (LoadTableDataJobStepAction) this;
    }
    
    public org.clawiz.etl.common.metadata.data.structure.Structure getSourceStructure() {
        return this.sourceStructure;
    }
    
    public void setSourceStructure(org.clawiz.etl.common.metadata.data.structure.Structure value) {
        this.sourceStructure = value;
    }
    
    public LoadTableDataJobStepAction withSourceStructure(org.clawiz.etl.common.metadata.data.structure.Structure value) {
        setSourceStructure(value);
        return (LoadTableDataJobStepAction) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getConnection() != null ) { 
            getConnection().prepare(session);
        }
        if ( getSourceStructure() != null ) { 
            getSourceStructure().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getConnection());
        
        references.add(getSourceStructure());
        
    }
}
