/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.etl.common.generator.service.component.step.service.element.action.extract;

import org.clawiz.core.common.system.database.Connection;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.generator.java.component.element.JavaMethodElement;
import org.clawiz.etl.common.metadata.data.job.step.action.AbstractJobStepAction;
import org.clawiz.etl.common.metadata.data.job.step.action.extract.table.ExtractTableDataJobStepAction;
import org.clawiz.etl.common.metadata.data.structure.Structure;
import org.clawiz.etl.common.metadata.data.structure.field.StructureField;

import java.sql.Struct;

public class JobStepServicePrototypeExtractTableDataActionMethodElement extends AbstractJobStepServicePrototypeExtractActionMethodElement {

    ExtractTableDataJobStepAction action;
    Structure                     structure;

    JavaMethodElement getQuerySqlMethod;
    JavaMethodElement executeQueryMethod;

    JavaMethodElement getRowsCountSqlMethod;
    JavaMethodElement getRowsCountMethod;

    protected void addGetQuerySqlMethod() {

        getQuerySqlMethod = getComponent().addMethod("String", "get" + action.getJavaClassName() + "QuerySql");
        getQuerySqlMethod.addParameter("JobStepActionInstance", "instance");

        getQuerySqlMethod.addText("return \"select\"");
        String prefix = "   ";


        for (StructureField field : structure.getFields() ) {
            getQuerySqlMethod.addText("       + \"" + prefix + field.getName() + "\"");
            prefix = "  ,";
        }

        getQuerySqlMethod.addText("       + \" from " + action.getSourceTableName() + "\""
                + (structure.getRecordIdField() != null ? "" : ";")
        );

        if ( action.getStructure().getRecordIdField() != null ) {
            getQuerySqlMethod.addText("       + \" order by " + structure.getRecordIdField().getName() + "\";");
        }


    }

    @Override
    public void setAction(AbstractJobStepAction action) {
        super.setAction(action);
        this.action    = (ExtractTableDataJobStepAction) action;
        this.structure = this.action.getStructure();

        getComponent().addImport(Statement.class);


        getRowsCountSqlMethod = getComponent().addMethod("String", "get" + action.getJavaClassName() + "RowsCountSql");
        getRowsCountSqlMethod.addParameter("JobStepActionInstance", "instance");
        getRowsCountSqlMethod.addText("return \"select count(*) from " + ((ExtractTableDataJobStepAction) action).getSourceTableName() + "\";");

        getRowsCountMethod    = getComponent().addMethod("long", "get" + action.getJavaClassName() + "RowsCount");
        getRowsCountMethod.addParameter("JobStepActionInstance", "instance");
        getRowsCountMethod.addText("return "
                + getConnectionVariableName(this.action.getConnection()) + ".executeQueryBigDecimal(" + getRowsCountSqlMethod.getName() + "(instance)).longValue();");

        addGetQuerySqlMethod();

        executeQueryMethod = getComponent().addMethod("Statement", "execute" + action.getJavaClassName() + "Query");
        executeQueryMethod.addParameter("JobStepActionInstance", "instance");
        executeQueryMethod.addText("return "
                + getConnectionVariableName(this.action.getConnection()) + ".executeQuery(" + getQuerySqlMethod.getName() + "(instance));");

    }

    @Override
    public void process() {
        super.process();

        addText("");
        addText("long      totalRowsCount     = " + getRowsCountMethod.getName() + "(instance);");
        addText("long      extractedRowsCount = 0;");
        addText("Statement statement          = " + executeQueryMethod.getName() + "(instance);");
        addText("");

        addText(jobGeneratorHelper.getStructureDataSetFullClassName(structure) + " dataSet = new " + jobGeneratorHelper.getStructureDataSetFullClassName(structure) + "();");

        addText("while ( statement.next() ) {");
        addText("    " + jobGeneratorHelper.getStructureRowFullClassName(structure) + " row = new " + jobGeneratorHelper.getStructureRowFullClassName(structure) + "();");
        addText("    dataSet.add(row);");
        addText("    ");
        int index = 1;
        for (StructureField field : structure.getFields() ) {
            addText("    row.set" + field.getJavaClassName() + "(statement.get" + field.getValueType().getStatementValueTypeName() + "(" + index++  + "));");
        }
        addText("    ");
        addText("    if ( dataSet.size() >= " + getDataSetMaxSizeMethod.getName() + "(instance) ) {");
        addText("        extractedRowsCount += dataSet.size();");
        addText("        dataSet.setTotalRowsCount(totalRowsCount);");
        addText("        dataSet.setExtractedRowsCount(extractedRowsCount);");
        addText("        putDataSet(instance, dataSet);");
        addText("        dataSet = new " + jobGeneratorHelper.getStructureDataSetFullClassName(structure) + "();");
        addText("    }");
        addText("}");
        addText("");
        addText("statement.close();");
        addText("");
        addText("extractedRowsCount += dataSet.size();");
        addText("dataSet.setTotalRowsCount(totalRowsCount);");
        addText("dataSet.setExtractedRowsCount(extractedRowsCount);");
        addText("putDataSet(instance, dataSet);");
        addText("");

    }
}
