package org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.valuetype;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsStructureValueTypeNumberPrototype extends org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.valuetype.MpsAbstractStructureValueType {
    
    public String getLanguageId() {
        return "6d27b471-0525-4d4c-bf85-b4bd447d6edd";
    }
    
    public String getLanguageName() {
        return "org.clawiz.etl.common.language";
    }
    
    public String getLanguageConceptId() {
        return "4155258003355141105";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.etl.common.language.structure.StructureValueTypeNumber";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.etl.common.metadata.data.structure.valuetype.StructureValueTypeNumber.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.etl.common.metadata.data.structure.valuetype.StructureValueTypeNumber structure = (org.clawiz.etl.common.metadata.data.structure.valuetype.StructureValueTypeNumber) node;
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.etl.common.metadata.data.structure.valuetype.StructureValueTypeNumber structure = (org.clawiz.etl.common.metadata.data.structure.valuetype.StructureValueTypeNumber) node;
        
    }
}
