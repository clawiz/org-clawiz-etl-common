/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.etl.common.generator.service.component.step.service.element.action.load;

import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.etl.common.metadata.data.job.step.action.AbstractJobStepAction;
import org.clawiz.etl.common.metadata.data.job.step.action.load.type.LoadTypeDataJobStepAction;
import org.clawiz.etl.common.metadata.data.job.step.action.load.type.TypeDataLoadFieldMap;
import org.clawiz.etl.common.metadata.data.structure.Structure;
import org.clawiz.etl.common.metadata.data.structure.field.StructureField;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

public class JobStepServicePrototypeLoadTypeDataActionMethodElement extends AbstractJobStepServicePrototypeLoadActionElement {

    LoadTypeDataJobStepAction action;
    Structure                 structure;
    Type                      type;

    @Override
    public LoadTypeDataJobStepAction getAction() {
        return action;
    }

    @Override
    public void setAction(AbstractJobStepAction action) {
        super.setAction(action);
        this.action = (LoadTypeDataJobStepAction) action;
        structure   = this.action.getSourceStructure();
        type        = this.action.getTargetType();
    }

    ArrayList<TypeDataLoadFieldMap> allMaps;
    HashMap<String, TypeField>      structureToTypedMap = new HashMap<>();
    HashMap<String, StructureField> typeToStructureMap  = new HashMap<>();

    protected void addMap(StructureField structureField, TypeField typeField) {
        TypeDataLoadFieldMap map = new TypeDataLoadFieldMap();
        map.setSourceField(structureField);
        
    }

    protected void prepareMap() {

        for (TypeDataLoadFieldMap map : action.getFieldMaps() ) {

        }

    }

    @Override
    public void process() {
        super.process();
        addText("// " + getAction().getClass().getName());

        getComponent().addImport(BigDecimal.class);

        addText(structure.getDataSetClassName() + " dataSet = (" + structure.getDataSetClassName() + ") instance.getDataSet();");
        String typeServiceClassName =  type.getServicePackageName() + "." + type.getJavaClassName() + "Service";
        addText(typeServiceClassName + " typeService = getService(" + typeServiceClassName + ".class);");
        addText("");
        addText("for ( " + structure.getRowClassName() + " row : dataSet ) {");
        addText("");
        addText("    BigDecimal id = null;");
        addText("");
        addText("");
        addText("");
        addText("}");
        addText("");
        addText("afterLoadDataSet(instance, dataSet);");
        addText("");

    }

}
