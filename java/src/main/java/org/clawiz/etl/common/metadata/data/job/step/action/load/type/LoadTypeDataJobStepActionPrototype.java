package org.clawiz.etl.common.metadata.data.job.step.action.load.type;

import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class LoadTypeDataJobStepActionPrototype extends org.clawiz.etl.common.metadata.data.job.step.action.load.AbstractLoadJobStepAction {
    
    @ExchangeReference
    private org.clawiz.etl.common.metadata.data.connection.JDBCConnection connection;
    
    @ExchangeReference
    private org.clawiz.etl.common.metadata.data.structure.Structure sourceStructure;
    
    @ExchangeReference
    private org.clawiz.core.common.metadata.data.type.Type targetType;
    
    @ExchangeElement
    private org.clawiz.etl.common.metadata.data.job.step.action.load.type.TypeDataLoadFieldMapList fieldMaps = new org.clawiz.etl.common.metadata.data.job.step.action.load.type.TypeDataLoadFieldMapList();
    
    public LoadTypeDataJobStepAction withName(String value) {
        setName(value);
        return (LoadTypeDataJobStepAction) this;
    }
    
    public org.clawiz.etl.common.metadata.data.connection.JDBCConnection getConnection() {
        return this.connection;
    }
    
    public void setConnection(org.clawiz.etl.common.metadata.data.connection.JDBCConnection value) {
        this.connection = value;
    }
    
    public LoadTypeDataJobStepAction withConnection(org.clawiz.etl.common.metadata.data.connection.JDBCConnection value) {
        setConnection(value);
        return (LoadTypeDataJobStepAction) this;
    }
    
    public org.clawiz.etl.common.metadata.data.structure.Structure getSourceStructure() {
        return this.sourceStructure;
    }
    
    public void setSourceStructure(org.clawiz.etl.common.metadata.data.structure.Structure value) {
        this.sourceStructure = value;
    }
    
    public LoadTypeDataJobStepAction withSourceStructure(org.clawiz.etl.common.metadata.data.structure.Structure value) {
        setSourceStructure(value);
        return (LoadTypeDataJobStepAction) this;
    }
    
    public org.clawiz.core.common.metadata.data.type.Type getTargetType() {
        return this.targetType;
    }
    
    public void setTargetType(org.clawiz.core.common.metadata.data.type.Type value) {
        this.targetType = value;
    }
    
    public LoadTypeDataJobStepAction withTargetType(org.clawiz.core.common.metadata.data.type.Type value) {
        setTargetType(value);
        return (LoadTypeDataJobStepAction) this;
    }
    
    public org.clawiz.etl.common.metadata.data.job.step.action.load.type.TypeDataLoadFieldMapList getFieldMaps() {
        return this.fieldMaps;
    }
    
    public LoadTypeDataJobStepAction withFieldMap(org.clawiz.etl.common.metadata.data.job.step.action.load.type.TypeDataLoadFieldMap value) {
        getFieldMaps().add(value);
        return (LoadTypeDataJobStepAction) this;
    }
    
    public <T extends org.clawiz.etl.common.metadata.data.job.step.action.load.type.TypeDataLoadFieldMap> T createFieldMap(Class<T> nodeClass) {
        org.clawiz.etl.common.metadata.data.job.step.action.load.type.TypeDataLoadFieldMap value = createChildNode(nodeClass, "fieldMaps");
        getFieldMaps().add(value);
        return (T) value;
    }
    
    public org.clawiz.etl.common.metadata.data.job.step.action.load.type.TypeDataLoadFieldMap createFieldMap() {
        return createFieldMap(org.clawiz.etl.common.metadata.data.job.step.action.load.type.TypeDataLoadFieldMap.class);
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getConnection() != null ) { 
            getConnection().prepare(session);
        }
        if ( getSourceStructure() != null ) { 
            getSourceStructure().prepare(session);
        }
        if ( getTargetType() != null ) { 
            getTargetType().prepare(session);
        }
        for (MetadataNode node : getFieldMaps()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getConnection());
        
        references.add(getSourceStructure());
        
        references.add(getTargetType());
        
        for (MetadataNode node : getFieldMaps()) {
            references.add(node);
        }
        
    }
}
