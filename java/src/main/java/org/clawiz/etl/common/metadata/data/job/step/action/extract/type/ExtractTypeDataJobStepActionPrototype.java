package org.clawiz.etl.common.metadata.data.job.step.action.extract.type;

import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class ExtractTypeDataJobStepActionPrototype extends org.clawiz.etl.common.metadata.data.job.step.action.extract.AbstractExtractJobStepAction {
    
    @ExchangeReference
    private org.clawiz.etl.common.metadata.data.connection.JDBCConnection connection;
    
    @ExchangeReference
    private org.clawiz.core.common.metadata.data.type.Type sourceType;
    
    @ExchangeReference
    private org.clawiz.etl.common.metadata.data.structure.Structure structure;
    
    public ExtractTypeDataJobStepAction withName(String value) {
        setName(value);
        return (ExtractTypeDataJobStepAction) this;
    }
    
    public org.clawiz.etl.common.metadata.data.connection.JDBCConnection getConnection() {
        return this.connection;
    }
    
    public void setConnection(org.clawiz.etl.common.metadata.data.connection.JDBCConnection value) {
        this.connection = value;
    }
    
    public ExtractTypeDataJobStepAction withConnection(org.clawiz.etl.common.metadata.data.connection.JDBCConnection value) {
        setConnection(value);
        return (ExtractTypeDataJobStepAction) this;
    }
    
    public org.clawiz.core.common.metadata.data.type.Type getSourceType() {
        return this.sourceType;
    }
    
    public void setSourceType(org.clawiz.core.common.metadata.data.type.Type value) {
        this.sourceType = value;
    }
    
    public ExtractTypeDataJobStepAction withSourceType(org.clawiz.core.common.metadata.data.type.Type value) {
        setSourceType(value);
        return (ExtractTypeDataJobStepAction) this;
    }
    
    public org.clawiz.etl.common.metadata.data.structure.Structure getStructure() {
        return this.structure;
    }
    
    public void setStructure(org.clawiz.etl.common.metadata.data.structure.Structure value) {
        this.structure = value;
    }
    
    public ExtractTypeDataJobStepAction withStructure(org.clawiz.etl.common.metadata.data.structure.Structure value) {
        setStructure(value);
        return (ExtractTypeDataJobStepAction) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getConnection() != null ) { 
            getConnection().prepare(session);
        }
        if ( getSourceType() != null ) { 
            getSourceType().prepare(session);
        }
        if ( getStructure() != null ) { 
            getStructure().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getConnection());
        
        references.add(getSourceType());
        
        references.add(getStructure());
        
    }
}
