/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.etl.common.manager.instance;

import org.clawiz.etl.common.metadata.data.job.Job;
import org.clawiz.etl.common.metadata.data.job.step.JobStep;
import org.clawiz.etl.common.manager.dataset.AbstractDataSet;
import org.clawiz.etl.common.manager.dataset.DataSetList;
import org.clawiz.etl.common.manager.instance.step.AbstractJobStepInstance;
import org.clawiz.etl.common.manager.instance.step.JobStepInstanceList;
import org.clawiz.etl.common.manager.instance.step.JobStepInstanceState;
import org.clawiz.etl.common.manager.service.AbstractJobService;
import org.clawiz.etl.common.manager.service.step.AbstractJobStepService;

import java.util.ArrayList;
import java.util.HashMap;

abstract public class AbstractJobInstance<T extends AbstractJobService> {


    Job                 job;
    T                   service;

    JobInstanceState    state          = JobInstanceState.READY;
    Exception           cause;

    JobStepInstanceList steps          = new JobStepInstanceList();
    DataSetList         dataSets       = new DataSetList();

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public JobInstanceState getState() {
        return state;
    }

    public void setState(JobInstanceState state) {
        this.state = state;
    }

    public Exception getCause() {
        return cause;
    }

    public void setCause(Exception cause) {
        this.cause = cause;
    }

    public T getService() {
        return service;
    }

    public void setService(T service) {
        this.service = service;
    }

    public JobStepInstanceList getSteps() {
        return steps;
    }

/*
    public DataSetList getDataSets() {
        return dataSets;
    }
*/

    protected <T extends AbstractJobStepInstance, S extends AbstractJobStepService> T addStepInstance(JobStep jobStep, Class<T> instanceClass, Class<S> serviceClass) {

        T stepInstance = null;
        try {
            stepInstance = instanceClass.newInstance();
        } catch (Exception e) {
            service.throwException("Exception on create step ? instance : ?", jobStep.getFullName(), e.getMessage(), e);
        }
        stepInstance.setJobInstance(this);
        stepInstance.setStep(jobStep);
        stepInstance.setJobInstance(this);
        stepInstance.setService(getService().getService(serviceClass));
        steps.add(stepInstance);
        return stepInstance;
    }

    abstract public void prepare();

    abstract public AbstractJobStepInstance getStepInstance(String stepName);

    private ArrayList<AbstractJobStepInstance>                        _readySteps;
    private HashMap<AbstractJobStepInstance, AbstractJobStepInstance> _readyStepsMap;
    private HashMap<AbstractJobStepInstance, AbstractJobStepInstance> _runStepsMap;
    public synchronized ArrayList<AbstractJobStepInstance> getAllowedReadySteps() {

        if ( _readySteps != null ) {
            return _readySteps;
        }

        ArrayList<AbstractJobStepInstance>                        readySteps    = new ArrayList<>();
        HashMap<AbstractJobStepInstance, AbstractJobStepInstance> readyStepsMap = new HashMap<>();
        HashMap<AbstractJobStepInstance, AbstractJobStepInstance> runStepsMap   = new HashMap<>();

        HashMap<String, String> doneSteps = new HashMap<>();
        for (AbstractJobStepInstance stepInstance : getSteps() ) {
            if ( stepInstance.getState() == JobStepInstanceState.DONE ) {
                doneSteps.put(stepInstance.getStep().getName(), null);
            }
            if ( stepInstance.getState() == JobStepInstanceState.RUN ) {
                runStepsMap.put(stepInstance, stepInstance);
            }
        }

        for (AbstractJobStepInstance stepInstance : getSteps() ) {

            if ( stepInstance.getState() != JobStepInstanceState.READY) {
                continue;
            }

            boolean passed = true;
            for(JobStep startStep : stepInstance.getStep().getPreviousSteps() ) {
                if ( ! doneSteps.containsKey(startStep.getName() )) {
                    passed = false;
                    break;
                }
            }

            if ( passed ) {
                readySteps.add(stepInstance);
                readyStepsMap.put(stepInstance, stepInstance);
            }

        }

        _readySteps    = readySteps;
        _readyStepsMap = readyStepsMap;
        _runStepsMap   = runStepsMap;

        return _readySteps;

    }

    public HashMap<AbstractJobStepInstance, AbstractJobStepInstance> getAllowedReadyStepsMap() {
        if ( _readyStepsMap == null ) {
            getAllowedReadySteps();
        }
        return _readyStepsMap;
    }

    public HashMap<AbstractJobStepInstance, AbstractJobStepInstance> getRunStepsMap() {
        if ( _runStepsMap == null ) {
            getAllowedReadyStepsMap();
        }
        return _runStepsMap;
    }

    public void clearCaches() {
        _readySteps = null;
        _readyStepsMap = null;
        _runStepsMap = null;
    }


}
