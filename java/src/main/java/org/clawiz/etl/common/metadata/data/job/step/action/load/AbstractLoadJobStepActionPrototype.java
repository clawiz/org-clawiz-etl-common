package org.clawiz.etl.common.metadata.data.job.step.action.load;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class AbstractLoadJobStepActionPrototype extends org.clawiz.etl.common.metadata.data.job.step.action.AbstractJobStepAction {
    
    public AbstractLoadJobStepAction withName(String value) {
        setName(value);
        return (AbstractLoadJobStepAction) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
