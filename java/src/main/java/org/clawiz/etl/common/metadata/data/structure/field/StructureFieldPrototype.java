package org.clawiz.etl.common.metadata.data.structure.field;

import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class StructureFieldPrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    @ExchangeAttribute
    private String description;
    
    @ExchangeElement
    private org.clawiz.etl.common.metadata.data.structure.valuetype.AbstractStructureValueType valueType;
    
    public StructureField withName(String value) {
        setName(value);
        return (StructureField) this;
    }
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String value) {
        this.description = value;
    }
    
    public StructureField withDescription(String value) {
        setDescription(value);
        return (StructureField) this;
    }
    
    public org.clawiz.etl.common.metadata.data.structure.valuetype.AbstractStructureValueType getValueType() {
        return this.valueType;
    }
    
    public void setValueType(org.clawiz.etl.common.metadata.data.structure.valuetype.AbstractStructureValueType value) {
        this.valueType = value;
    }
    
    public StructureField withValueType(org.clawiz.etl.common.metadata.data.structure.valuetype.AbstractStructureValueType value) {
        setValueType(value);
        return (StructureField) this;
    }
    
    public <T extends org.clawiz.etl.common.metadata.data.structure.valuetype.AbstractStructureValueType> T createValueType(Class<T> nodeClass) {
        if ( getValueType() != null ) {
            throwException("Cannot create already defined value of '?' for '?'", "valueType", this.getFullName());
        }
        org.clawiz.etl.common.metadata.data.structure.valuetype.AbstractStructureValueType value = createChildNode(nodeClass, "valueType");
        setValueType(value);
        return (T) value;
    }
    
    public org.clawiz.etl.common.metadata.data.structure.valuetype.AbstractStructureValueType createValueType() {
        return createValueType(org.clawiz.etl.common.metadata.data.structure.valuetype.AbstractStructureValueType.class);
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getValueType() != null ) { 
            getValueType().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getValueType());
        
    }
}
