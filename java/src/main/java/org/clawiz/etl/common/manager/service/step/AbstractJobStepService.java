/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.etl.common.manager.service.step;

import org.clawiz.core.common.system.service.Service;
import org.clawiz.etl.common.manager.JobManager;
import org.clawiz.etl.common.manager.dataset.AbstractDataSet;
import org.clawiz.etl.common.manager.instance.step.AbstractJobStepInstance;
import org.clawiz.etl.common.manager.instance.step.action.JobStepActionInstance;
import org.clawiz.etl.common.manager.service.step.action.JobStepActionThread;

public class AbstractJobStepService<T extends AbstractJobStepInstance> extends Service {

    JobManager jobManager;

    public void putDataSet(JobStepActionInstance instance, AbstractDataSet dataSet) {
        if ( dataSet.size() == 0 ) {
            return;
        }
        logDebug("Extracted " + instance.getAction().getName() + " " + dataSet.size()
                + " rows : " + dataSet.getExtractedRowsCount() + "/" + dataSet.getTotalRowsCount() + " = "
                + ( dataSet.getTotalRowsCount() != 0 ? ( dataSet.getExtractedRowsCount() * 100 / dataSet.getTotalRowsCount() ) : 100 )
                +"%");
        jobManager.putDataSet(instance, dataSet);
    }

    public void afterLoadDataSet(JobStepActionInstance instance, AbstractDataSet dataSet) {
        logDebug("Loaded    " + instance.getAction().getName() + " " + dataSet.size()
                + " rows : " + dataSet.getExtractedRowsCount() + "/" + dataSet.getTotalRowsCount() + " = "
                + ( dataSet.getTotalRowsCount() != 0 ? ( dataSet.getExtractedRowsCount() * 100 / dataSet.getTotalRowsCount() ) : 100 )
                +"%");
    }

    public void process(JobStepActionInstance instance) {

    }

    public void threadDone(JobStepActionThread thread) {

        thread.getActionInstance().getStepInstance().getJobInstance().getService().stepThreadDone(thread);

    }

    public void threadException(JobStepActionThread thread, Exception cause) {

        thread.getActionInstance().getStepInstance().getJobInstance().getService().stepThreadException(thread, cause);

    }

}
