/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.etl.common.generator.service.component.job.instance;

import org.clawiz.core.common.system.generator.java.component.element.JavaMemberElement;
import org.clawiz.core.common.system.generator.java.component.element.JavaVariableElement;
import org.clawiz.etl.common.metadata.data.job.step.JobStep;
import org.clawiz.etl.common.generator.service.component.job.AbstractJobGeneratorJobJavaClassComponent;
import org.clawiz.etl.common.generator.service.component.job.instance.element.JobInstancePrototypeGetStepInstanceMethodElement;
import org.clawiz.etl.common.generator.service.component.job.instance.element.JobInstancePrototypePrepareMethodElement;
import org.clawiz.etl.common.manager.instance.AbstractJobInstance;
import org.clawiz.etl.common.manager.instance.step.AbstractJobStepInstance;

import java.util.ArrayList;

public class JobInstancePrototypeComponent extends AbstractJobGeneratorJobJavaClassComponent {

    ArrayList<JavaVariableElement> variableElements = new ArrayList<>();

    protected void addImports() {

        addImport(AbstractJobStepInstance.class);
    }

    protected void addVariables() {

        for (JobStep step : getJob().getSteps() ) {

            String stepInstanceClassName = step.getJavaClassName() + "Instance";
            String stepServiceClassName  = step.getJavaClassName() + "Service";

            addImport(getJob().getPackageName() + "." + getJob().getJavaClassName().toLowerCase() + "." + step.getJavaClassName().toLowerCase() + "." + stepInstanceClassName);
            addImport(getJob().getPackageName() + "." + getJob().getJavaClassName().toLowerCase() + "." + step.getJavaClassName().toLowerCase() + "." + stepServiceClassName);

            JavaVariableElement ve = addVariable(stepInstanceClassName, jobGeneratorHelper.getStepInstanceJavaVariableName(step));
            ve.setAccessLevel(JavaMemberElement.AccessLevel.PRIVATE);
            variableElements.add(ve);

        }

    }

    protected void addGetSetMethods() {

        for ( JavaVariableElement ve : variableElements ) {
            ve.addGetter();
        }

    }



    @Override
    public void process() {
        super.process();

        setName(getGenerator().getComponentByClass(JobInstanceComponent.class).getName() + "Prototype");

        setExtends(AbstractJobInstance.class.getName() + "<" + getServiceClassName() + ">");

        addImports();

        addVariables();

        addGetSetMethods();

        addElement(JobInstancePrototypePrepareMethodElement.class);

        addElement(JobInstancePrototypeGetStepInstanceMethodElement.class);

    }

}
