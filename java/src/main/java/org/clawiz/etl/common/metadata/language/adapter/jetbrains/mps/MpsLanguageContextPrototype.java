package org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps;

import java.lang.String;

public class MpsLanguageContextPrototype extends org.clawiz.metadata.jetbrains.mps.parser.context.AbstractMpsLanguageContext {
    
    public String getLanguageName() {
        return "org.clawiz.etl.common.language";
    }
    
    public void prepare() {
        
        addClassMap(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.MpsJob.class);
        addClassMap(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step.action.MpsAbstractJobStepAction.class);
        addClassMap(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step.MpsJobStepRef.class);
        addClassMap(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.MpsStructure.class);
        addClassMap(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.field.MpsStructureField.class);
        addClassMap(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step.action.extract.MpsAbstractExtractJobStepAction.class);
        addClassMap(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step.action.load.MpsAbstractLoadJobStepAction.class);
        addClassMap(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.connection.MpsAbstractConnection.class);
        addClassMap(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.connection.MpsJDBCConnection.class);
        addClassMap(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step.action.extract.table.MpsExtractTableDataJobStepAction.class);
        addClassMap(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step.action.load.table.MpsLoadTableDataJobStepAction.class);
        addClassMap(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step.action.load.type.MpsLoadTypeDataJobStepAction.class);
        addClassMap(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step.action.extract.type.MpsExtractTypeDataJobStepAction.class);
        addClassMap(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step.MpsJobStep.class);
        addClassMap(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.valuetype.MpsAbstractStructureValueType.class);
        addClassMap(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.valuetype.MpsStructureValueTypeString.class);
        addClassMap(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.valuetype.MpsStructureValueTypeNumber.class);
        addClassMap(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.valuetype.MpsStructureValueTypeDate.class);
        addClassMap(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.valuetype.MpsStructureValueTypeDateTime.class);
        addClassMap(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.valuetype.MpsStructureValueTypeScaledNumber.class);
        addClassMap(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.valuetype.MpsStructureValueTypeStructureReference.class);
        addClassMap(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step.action.load.type.MpsTypeDataLoadFieldMap.class);
        
    }
}
