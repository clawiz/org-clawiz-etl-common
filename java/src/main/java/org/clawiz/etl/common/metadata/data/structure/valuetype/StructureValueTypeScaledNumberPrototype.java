package org.clawiz.etl.common.metadata.data.structure.valuetype;

import java.math.BigDecimal;
import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class StructureValueTypeScaledNumberPrototype extends org.clawiz.etl.common.metadata.data.structure.valuetype.AbstractStructureValueType {
    
    @ExchangeAttribute
    private BigDecimal precision;
    
    @ExchangeAttribute
    private BigDecimal scale;
    
    public StructureValueTypeScaledNumber withName(String value) {
        setName(value);
        return (StructureValueTypeScaledNumber) this;
    }
    
    public BigDecimal getPrecision() {
        return this.precision;
    }
    
    public void setPrecision(BigDecimal value) {
        this.precision = value;
    }
    
    public StructureValueTypeScaledNumber withPrecision(BigDecimal value) {
        setPrecision(value);
        return (StructureValueTypeScaledNumber) this;
    }
    
    public BigDecimal getScale() {
        return this.scale;
    }
    
    public void setScale(BigDecimal value) {
        this.scale = value;
    }
    
    public StructureValueTypeScaledNumber withScale(BigDecimal value) {
        setScale(value);
        return (StructureValueTypeScaledNumber) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
