/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.etl.common.manager;

import org.clawiz.core.common.system.service.Service;
import org.clawiz.etl.common.metadata.data.job.step.JobStep;
import org.clawiz.etl.common.metadata.data.job.step.action.AbstractJobStepAction;
import org.clawiz.etl.common.metadata.data.job.step.action.JobStepActionType;
import org.clawiz.etl.common.helper.JobHelper;
import org.clawiz.etl.common.manager.dataset.AbstractDataSet;
import org.clawiz.etl.common.manager.instance.AbstractJobInstance;
import org.clawiz.etl.common.manager.instance.JobInstanceState;
import org.clawiz.etl.common.manager.instance.step.AbstractJobStepInstance;
import org.clawiz.etl.common.manager.instance.step.JobStepInstanceState;
import org.clawiz.etl.common.manager.instance.step.action.JobStepActionInstance;
import org.clawiz.etl.common.manager.instance.step.action.JobStepActionState;
import org.clawiz.etl.common.manager.service.step.action.JobStepActionThread;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;

public class JobManager extends Service {

    JobHelper jobHelper;

    public static ConcurrentHashMap<AbstractJobInstance, AbstractJobInstance> activeInstances = new ConcurrentHashMap<>();


    protected void registerException(AbstractJobInstance jobInstance, Exception e) {
        jobInstance.setState(JobInstanceState.ERROR);
        jobInstance.setCause(e);
    }

    private static ConcurrentLinkedDeque<JobStepActionThread>                                                threadsQueue     = new ConcurrentLinkedDeque<>();
    private static ConcurrentHashMap<AbstractJobInstance, ArrayList<JobStepActionThread>>                    jobThreadsQueue  = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<JobStepActionThread, JobStepActionThread>                               activeThreads    = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<AbstractJobInstance, HashMap<JobStepActionThread, JobStepActionThread>> jobActiveThreads = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<AbstractJobInstance, AbstractJobInstance>                               lockedJobs       = new ConcurrentHashMap<>();

    private class JobDataSetQueueRecord {
        AbstractDataSet         dataSet;
        AbstractJobStepInstance stepInstance;
        AbstractJobStepAction   action;

        public JobDataSetQueueRecord(AbstractDataSet dataSet, AbstractJobStepInstance stepInstance, AbstractJobStepAction action) {
            this.dataSet = dataSet;
            this.stepInstance = stepInstance;
            this.action = action;
        }
    }

    private static ConcurrentHashMap<AbstractJobInstance, LinkedList<JobDataSetQueueRecord>> jobsDataSetQueue = new ConcurrentHashMap<>();

    private int maxThreadsCount = 1;

    private synchronized void processThreadQueue() {

        boolean found = true;
        while (activeThreads.size() < maxThreadsCount && threadsQueue.size() > 0 && found) {

            found = false;

            JobStepActionThread thread = threadsQueue.getFirst();
            AbstractJobInstance jobInstance = thread.getActionInstance().getStepInstance().getJobInstance();

            if (lockedJobs.containsKey(jobInstance)) {
                continue;
            }

            threadsQueue.removeFirst();
            ArrayList<JobStepActionThread> jobQueue = jobThreadsQueue.get(jobInstance);
            if (jobQueue != null) {
                jobQueue.remove(thread);
            }


            jobActiveThreads.get(jobInstance).put(thread, thread);
            activeThreads.put(thread, thread);
            thread.start();

            found = true;
        }

    }

    private void addActionThread(JobStepActionThread thread) {

        if ( jobThreadsQueue.get(thread.getActionInstance().getStepInstance().getJobInstance()) != null ) {
            threadsQueue.add(thread);
            jobThreadsQueue.get(thread.getActionInstance().getStepInstance().getJobInstance()).add(thread);
        }

    }

    public void createActionInstance(AbstractJobStepInstance stepInstance, AbstractJobStepAction action) {
        createActionInstance(stepInstance, action, null);
    }

    public void createActionInstance(AbstractJobStepInstance stepInstance, AbstractJobStepAction action, AbstractDataSet dataSet) {

        JobStepActionThread   thread        = new JobStepActionThread();
        JobStepActionInstance actionInstance = new JobStepActionInstance();

        thread.setActionInstance(actionInstance);
        actionInstance.setThread(thread);

        actionInstance.setStepInstance(stepInstance);
        actionInstance.setAction(action);
        actionInstance.setDataSet(dataSet);

        thread.setActionInstance(actionInstance);

        addActionThread(thread);

        processThreadQueue();
    }

    public void startAllowedSteps(AbstractJobInstance jobInstance) {

        synchronized (jobInstance) {

            ArrayList<AbstractJobStepInstance> allowedSteps = jobInstance.getAllowedReadySteps();

            for (int i = 0; i < allowedSteps.size(); i++) {

                AbstractJobStepInstance stepInstance = allowedSteps.get(i);

//                logDebug("START step " + stepInstance.getStep().getName());

                stepInstance.setState(JobStepInstanceState.RUN);
                for (AbstractJobStepAction action : stepInstance.getStep().getActions()) {
                    if (action.getType() == JobStepActionType.EXTRACT) {
                        createActionInstance(stepInstance, action);
                    }
                }
            }

            processJobDataSetQueue(jobInstance);

        }


    }

    public void start(AbstractJobInstance instance) {

        try {
            instance.prepare();
        } catch (Exception e) {
            registerException(instance, e);
            throw e;
        }

        instance.setState(JobInstanceState.RUN);
        activeInstances.put(instance, instance);

        jobThreadsQueue.put(instance, new ArrayList<>());
        jobActiveThreads.put(instance, new HashMap<>());
        jobsDataSetQueue.put(instance, new LinkedList<>());

        startAllowedSteps(instance);

    }


    public void done(AbstractJobInstance instance) {
        activeInstances.remove(instance);
    }

    private void jobInstanceDone(AbstractJobInstance jobInstance) {
        jobInstance.setState(JobInstanceState.DONE);
    }

    private void jobInstanceException(AbstractJobInstance jobInstance, Exception cause) {

        synchronized (jobInstance) {

            lockedJobs.put(jobInstance, jobInstance);

            jobsDataSetQueue.remove(jobInstance);

            jobInstance.setState(JobInstanceState.ERROR);
            jobInstance.setCause(cause);


            ArrayList<JobStepActionThread> jobQueue = jobThreadsQueue.get(jobInstance);
            jobThreadsQueue.remove(jobInstance);
            if (jobQueue != null) {
                for (JobStepActionThread thread : jobQueue) {
                    threadsQueue.remove(thread);
                }
            }

            HashMap<JobStepActionThread, JobStepActionThread> jobThreads = jobActiveThreads.get(jobInstance);
            jobActiveThreads.remove(jobInstance);
            if ( jobThreads != null ) {
                for (JobStepActionThread thread : jobThreads.keySet()) {
                    thread.interrupt();
                    jobActiveThreads.remove(thread);
                }
            }

            lockedJobs.remove(jobInstance);

        }

    }

    private void processJobDataSetQueue(AbstractJobInstance jobInstance) {
        ArrayList<JobDataSetQueueRecord>  readyQr = new ArrayList<>();
        LinkedList<JobDataSetQueueRecord> qrList  = jobsDataSetQueue.get(jobInstance);
        if ( qrList == null ) {
            return;
        }
        for(JobDataSetQueueRecord qr : qrList ) {
            if ( jobInstance.getAllowedReadyStepsMap().containsKey(qr.stepInstance)
                    || jobInstance.getRunStepsMap().containsKey(qr.stepInstance)) {
                readyQr.add(qr);
            }
        }

        for ( JobDataSetQueueRecord qr : readyQr ) {
            createActionInstance(qr.stepInstance, qr.action, qr.dataSet);
            qrList.remove(qr);
        }

    }

    private void checkStepInstanceDone(AbstractJobStepInstance stepInstance) {

        AbstractJobInstance jobInstance = stepInstance.getJobInstance();

        synchronized (jobInstance) {

            for (JobStep previousStep : stepInstance.getStep().getPreviousSteps()) {
                if (jobInstance.getStepInstance(previousStep.getName()).getState() != JobStepInstanceState.DONE) {
                    return;
                }
            }

            ArrayList<JobStepActionThread> jobQueue = jobThreadsQueue.get(jobInstance);
            if ( jobQueue != null ) {
                for (JobStepActionThread thread : jobQueue) {
                    if (thread.getActionInstance().getStepInstance() == stepInstance) {
                        return;
                    }
                }
            }

            HashMap<JobStepActionThread, JobStepActionThread> jobThreads = jobActiveThreads.get(jobInstance);
            if ( jobThreads != null ) {
                for (JobStepActionThread thread : jobThreads.keySet()) {
                    if (thread.getActionInstance().getStepInstance() == stepInstance) {
                        return;
                    }
                }
            }

            stepInstance.setState(JobStepInstanceState.DONE);

            jobInstance.clearCaches();

            processJobDataSetQueue(jobInstance);

            startAllowedSteps(jobInstance);

            for (AbstractJobStepInstance checkStep : jobInstance.getSteps()) {
                if (checkStep.getState() != JobStepInstanceState.DONE) {
                    return;
                }
            }

            if ( jobsDataSetQueue.get(jobInstance).size() > 0 ) {
                return;
            }

            jobInstanceDone(jobInstance);

        }

    }

    private void stepInstanceException(AbstractJobStepInstance stepInstance, Exception cause) {


        stepInstance.setState(JobStepInstanceState.ERROR);
        stepInstance.setCause(cause);
        stepInstance.getJobInstance().clearCaches();

        jobInstanceException(stepInstance.getJobInstance(), cause);

    }


    public void putDataSet(JobStepActionInstance currentActionInstance, AbstractDataSet dataSet) {


//        logDebug("PUT dataSet " + currentActionInstance.getAction().getName() + " -> " + dataSet.getClass().getName());

        AbstractJobInstance     jobInstance         = currentActionInstance.getStepInstance().getJobInstance();

        ArrayList<AbstractJobStepAction> actions    = jobInstance.getJob().getLoadDataSetActions(dataSet);

        for ( int i=0; i < actions.size(); i++) {

            AbstractJobStepAction nextAction = actions.get(i);
            if ( dataSet.isActionProcessed(nextAction) ) {
                continue;
            }

            AbstractJobStepInstance nextStepInstance = jobInstance.getStepInstance(nextAction.getJobStep().getName());
            if ( nextStepInstance.getState() == JobStepInstanceState.DONE ) {
                nextStepInstance.setState(JobStepInstanceState.RUN);
                jobInstance.clearCaches();
            }

            dataSet.setActionProcessed(nextAction);

            if ( jobInstance.getAllowedReadyStepsMap().containsKey(nextStepInstance) ) {
                createActionInstance(nextStepInstance, nextAction, dataSet);
            } else {
                jobsDataSetQueue.get(jobInstance).add(new JobDataSetQueueRecord(dataSet, nextStepInstance, nextAction));
            }

        }


    }

    public void stepActionThreadDone(JobStepActionThread thread) {

        if ( ! thread.isProtectedMode() ) {
            throwException("Wrong call of threadDone method for job action ?", thread.getActionInstance().getAction().getFullName());
        }

        JobStepActionInstance actionInstance = thread.getActionInstance();
        actionInstance.setState(JobStepActionState.DONE);

        jobActiveThreads.get(actionInstance.getStepInstance().getJobInstance()).remove(thread);
        activeThreads.remove(thread);

        processThreadQueue();

        AbstractDataSet dataSet = thread.getActionInstance().getDataSet();
        if ( dataSet != null ) {
            putDataSet(actionInstance, dataSet);
        }

        checkStepInstanceDone(thread.getActionInstance().getStepInstance());


    }

    public void stepActionThreadException(JobStepActionThread thread, Exception cause) {

        if (!thread.isProtectedMode()) {
            throwException("Wrong call of threadException method for job action ?", thread.getActionInstance().getAction().getFullName());
        }

        thread.getActionInstance().setState(JobStepActionState.ERROR);
        thread.getActionInstance().setCause(cause);

        HashMap<JobStepActionThread, JobStepActionThread> jobThreads = jobActiveThreads.get(thread.getActionInstance().getStepInstance().getJobInstance());
        if ( jobThreads != null ) {
            jobThreads.remove(thread);
        }
        activeThreads.remove(thread);
        processThreadQueue();
        stepInstanceException(thread.getActionInstance().getStepInstance(), cause);

    }




}
