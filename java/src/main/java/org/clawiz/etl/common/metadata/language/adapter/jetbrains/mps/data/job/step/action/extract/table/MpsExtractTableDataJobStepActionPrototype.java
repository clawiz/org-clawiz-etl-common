package org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step.action.extract.table;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsExtractTableDataJobStepActionPrototype extends org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step.action.extract.MpsAbstractExtractJobStepAction {
    
    public String sourceTableName;
    
    public org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.connection.MpsJDBCConnection connection;
    
    public org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.MpsStructure structure;
    
    public String getSourceTableName() {
        return this.sourceTableName;
    }
    
    public void setSourceTableName(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.sourceTableName = null;
        }
        this.sourceTableName = value;
    }
    
    public org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.connection.MpsJDBCConnection getConnection() {
        return this.connection;
    }
    
    public void setConnection(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.connection.MpsJDBCConnection value) {
        this.connection = value;
    }
    
    public org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.MpsStructure getStructure() {
        return this.structure;
    }
    
    public void setStructure(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.MpsStructure value) {
        this.structure = value;
    }
    
    public String getLanguageId() {
        return "6d27b471-0525-4d4c-bf85-b4bd447d6edd";
    }
    
    public String getLanguageName() {
        return "org.clawiz.etl.common.language";
    }
    
    public String getLanguageConceptId() {
        return "2943750519754464368";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.etl.common.language.structure.ExtractTableDataJobStepAction";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("6d27b471-0525-4d4c-bf85-b4bd447d6edd", "org.clawiz.etl.common.language", "2943750519754464368", "org.clawiz.etl.common.language.structure.ExtractTableDataJobStepAction", ConceptPropertyType.PROPERTY, "2943750519754464378", "sourceTableName"));
        result.add(new ConceptProperty("6d27b471-0525-4d4c-bf85-b4bd447d6edd", "org.clawiz.etl.common.language", "2943750519754464368", "org.clawiz.etl.common.language.structure.ExtractTableDataJobStepAction", ConceptPropertyType.REFERENCE, "2943750519754464369", "connection"));
        result.add(new ConceptProperty("6d27b471-0525-4d4c-bf85-b4bd447d6edd", "org.clawiz.etl.common.language", "2943750519754464368", "org.clawiz.etl.common.language.structure.ExtractTableDataJobStepAction", ConceptPropertyType.REFERENCE, "2943750519754464375", "structure"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeProperty("2943750519754464368", "sourceTableName", getSourceTableName());
        addConceptNodeRef("2943750519754464368", "connection", getConnection());
        addConceptNodeRef("2943750519754464368", "structure", getStructure());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.etl.common.metadata.data.job.step.action.extract.table.ExtractTableDataJobStepAction.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.etl.common.metadata.data.job.step.action.extract.table.ExtractTableDataJobStepAction structure = (org.clawiz.etl.common.metadata.data.job.step.action.extract.table.ExtractTableDataJobStepAction) node;
        
        structure.setSourceTableName(getSourceTableName());
        
        if ( getConnection() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "connection", false, getConnection());
        } else {
            structure.setConnection(null);
        }
        
        if ( getStructure() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "structure", false, getStructure());
        } else {
            structure.setStructure(null);
        }
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
        addForeignKey(getConnection());
        addForeignKey(getStructure());
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.etl.common.metadata.data.job.step.action.extract.table.ExtractTableDataJobStepAction structure = (org.clawiz.etl.common.metadata.data.job.step.action.extract.table.ExtractTableDataJobStepAction) node;
        
        setSourceTableName(structure.getSourceTableName());
        
        if ( structure.getConnection() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "connection", false, structure.getConnection());
        } else {
            setConnection(null);
        }
        
        if ( structure.getStructure() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "structure", false, structure.getStructure());
        } else {
            setStructure(null);
        }
        
    }
}
