package org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.connection;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsAbstractConnectionPrototype extends AbstractMpsNode {
    
    public String getLanguageId() {
        return "6d27b471-0525-4d4c-bf85-b4bd447d6edd";
    }
    
    public String getLanguageName() {
        return "org.clawiz.etl.common.language";
    }
    
    public String getLanguageConceptId() {
        return "2943750519754464353";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.etl.common.language.structure.AbstractConnection";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.etl.common.metadata.data.connection.AbstractConnection.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.etl.common.metadata.data.connection.AbstractConnection structure = (org.clawiz.etl.common.metadata.data.connection.AbstractConnection) node;
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.etl.common.metadata.data.connection.AbstractConnection structure = (org.clawiz.etl.common.metadata.data.connection.AbstractConnection) node;
        
    }
}
