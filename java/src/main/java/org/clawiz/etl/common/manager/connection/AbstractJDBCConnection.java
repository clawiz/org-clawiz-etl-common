/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.etl.common.manager.connection;

import org.clawiz.core.common.Core;
import org.clawiz.core.common.system.config.Config;
import org.clawiz.core.common.system.database.Connection;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.database.datasource.DataSource;
import org.clawiz.core.common.system.database.datasource.DataSourceConfig;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.utils.RandomGUID;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;

public class AbstractJDBCConnection extends AbstractConnection {

    public static final String URL_CONNECTION_CONFIG_PATH                      = "url";
    public static final String USER_CONNECTION_CONFIG_PATH                     = "user";
    public static final String PASSWORD_CONNECTION_CONFIG_PATH                 = "password";
    public static final String CONNECTION_POOL_MAX_SIZE_CONNECTION_CONFIG_PATH = "connection-pool.maximum-size";

    public static final int DEFAULT_CONNECTION_POOL_MAX_SIZE = 3;

    private DataSourceConfig dataSourceConfig;

    public Config getConfig() {
        return null;
    }

    public DataSourceConfig  getDataSourceConfig() {
        if ( dataSourceConfig == null ) {
            dataSourceConfig = new DataSourceConfig();
            dataSourceConfig.loadConfig(getConfig());
        }
        return dataSourceConfig;
    }

    protected String getUrl() {
        return getDataSourceConfig().getUrl();
    }

    protected String getUser() {
        return getDataSourceConfig().getUser();
    }

    protected String getPassword() {
        return getDataSourceConfig().getPassword();
    }

    protected int getConnectionPoolSize() {
        return getDataSourceConfig().getConnectionPoolSize();
    }

    Connection connection;


    private static HashMap<Class, DataSource> dataSourcesCache = new HashMap<>();
    private DataSource _dataSource;

    protected synchronized DataSource getDataSource() {
        if ( _dataSource != null ) {
            return _dataSource;
        }
        _dataSource = dataSourcesCache.get(this.getClass());
        if ( _dataSource != null ) {
            return _dataSource;
        }
        _dataSource = Core.createDataSource(getDataSourceConfig());

        dataSourcesCache.put(this.getClass(), _dataSource);

        return _dataSource;
    }

    private Session connectionSession;

    protected Connection newJdbcConnection() {
        connectionSession = Core.getSessions().newSession(getDataSource(), new RandomGUID().toString());
        getSession().addChildSession(connectionSession);
        return connectionSession.getConnection();
    }


    public synchronized Connection getConnection() {
        if ( connection == null ) {
            connection = newJdbcConnection();
        }
        return connection;
    }

    public Statement executeQuery(String sql, Object... parameters) {
        Statement statement = getConnection().executeQuery(sql, parameters);
        return statement;
    }

    public BigDecimal executeQueryBigDecimal(String sql, Object... parameters) {
        return getConnection().executeQueryBigDecimal(sql, parameters);
    }

    public Date executeQueryDate(String sql, Object... parameters) {
        return getConnection().executeQueryDate(sql, parameters);
    }

    public String executeQueryString(String sql, Object... parameters) {
        return getConnection().executeQueryString(sql, parameters);
    }

    public void done() {
        connectionSession.destroy();
    }

}
