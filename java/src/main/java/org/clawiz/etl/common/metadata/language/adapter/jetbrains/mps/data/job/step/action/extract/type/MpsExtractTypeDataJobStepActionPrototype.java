package org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step.action.extract.type;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsExtractTypeDataJobStepActionPrototype extends org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step.action.extract.MpsAbstractExtractJobStepAction {
    
    public org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.connection.MpsJDBCConnection connection;
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.MpsType sourceType;
    
    public org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.MpsStructure structure;
    
    public org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.connection.MpsJDBCConnection getConnection() {
        return this.connection;
    }
    
    public void setConnection(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.connection.MpsJDBCConnection value) {
        this.connection = value;
    }
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.MpsType getSourceType() {
        return this.sourceType;
    }
    
    public void setSourceType(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.MpsType value) {
        this.sourceType = value;
    }
    
    public org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.MpsStructure getStructure() {
        return this.structure;
    }
    
    public void setStructure(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.MpsStructure value) {
        this.structure = value;
    }
    
    public String getLanguageId() {
        return "6d27b471-0525-4d4c-bf85-b4bd447d6edd";
    }
    
    public String getLanguageName() {
        return "org.clawiz.etl.common.language";
    }
    
    public String getLanguageConceptId() {
        return "2943750519754465581";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.etl.common.language.structure.ExtractTypeDataJobStepAction";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("6d27b471-0525-4d4c-bf85-b4bd447d6edd", "org.clawiz.etl.common.language", "2943750519754465581", "org.clawiz.etl.common.language.structure.ExtractTypeDataJobStepAction", ConceptPropertyType.REFERENCE, "2943750519754465582", "connection"));
        result.add(new ConceptProperty("6d27b471-0525-4d4c-bf85-b4bd447d6edd", "org.clawiz.etl.common.language", "2943750519754465581", "org.clawiz.etl.common.language.structure.ExtractTypeDataJobStepAction", ConceptPropertyType.REFERENCE, "2943750519754465583", "sourceType"));
        result.add(new ConceptProperty("6d27b471-0525-4d4c-bf85-b4bd447d6edd", "org.clawiz.etl.common.language", "2943750519754465581", "org.clawiz.etl.common.language.structure.ExtractTypeDataJobStepAction", ConceptPropertyType.REFERENCE, "2943750519754465839", "structure"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeRef("2943750519754465581", "connection", getConnection());
        addConceptNodeRef("2943750519754465581", "sourceType", getSourceType());
        addConceptNodeRef("2943750519754465581", "structure", getStructure());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.etl.common.metadata.data.job.step.action.extract.type.ExtractTypeDataJobStepAction.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.etl.common.metadata.data.job.step.action.extract.type.ExtractTypeDataJobStepAction structure = (org.clawiz.etl.common.metadata.data.job.step.action.extract.type.ExtractTypeDataJobStepAction) node;
        
        if ( getConnection() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "connection", false, getConnection());
        } else {
            structure.setConnection(null);
        }
        
        if ( getSourceType() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "sourceType", false, getSourceType());
        } else {
            structure.setSourceType(null);
        }
        
        if ( getStructure() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "structure", false, getStructure());
        } else {
            structure.setStructure(null);
        }
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
        addForeignKey(getConnection());
        addForeignKey(getSourceType());
        addForeignKey(getStructure());
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.etl.common.metadata.data.job.step.action.extract.type.ExtractTypeDataJobStepAction structure = (org.clawiz.etl.common.metadata.data.job.step.action.extract.type.ExtractTypeDataJobStepAction) node;
        
        if ( structure.getConnection() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "connection", false, structure.getConnection());
        } else {
            setConnection(null);
        }
        
        if ( structure.getSourceType() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "sourceType", false, structure.getSourceType());
        } else {
            setSourceType(null);
        }
        
        if ( structure.getStructure() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "structure", false, structure.getStructure());
        } else {
            setStructure(null);
        }
        
    }
}
