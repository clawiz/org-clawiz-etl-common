package org.clawiz.etl.common.storage.jobstep;


import org.clawiz.etl.common.storage.job.JobService;

import java.math.BigDecimal;

public class JobStepService extends JobStepServicePrototype {


    @Override
    public void save(JobStepObject jobStep) {
        super.save(jobStep);
        JobService.clearStaticCaches();
    }

    @Override
    public void delete(BigDecimal id) {
        super.delete(id);
        JobService.clearStaticCaches();
    }
}
