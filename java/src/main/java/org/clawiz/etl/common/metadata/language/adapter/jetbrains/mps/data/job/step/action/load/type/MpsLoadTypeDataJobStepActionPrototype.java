package org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step.action.load.type;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsLoadTypeDataJobStepActionPrototype extends org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step.action.load.MpsAbstractLoadJobStepAction {
    
    public org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.connection.MpsJDBCConnection connection;
    
    public org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.MpsStructure sourceStructure;
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.MpsType targetType;
    
    public ArrayList<org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step.action.load.type.MpsTypeDataLoadFieldMap> fieldMaps = new ArrayList<>();
    
    public org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.connection.MpsJDBCConnection getConnection() {
        return this.connection;
    }
    
    public void setConnection(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.connection.MpsJDBCConnection value) {
        this.connection = value;
    }
    
    public org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.MpsStructure getSourceStructure() {
        return this.sourceStructure;
    }
    
    public void setSourceStructure(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.MpsStructure value) {
        this.sourceStructure = value;
    }
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.MpsType getTargetType() {
        return this.targetType;
    }
    
    public void setTargetType(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.MpsType value) {
        this.targetType = value;
    }
    
    public ArrayList<org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step.action.load.type.MpsTypeDataLoadFieldMap> getFieldMaps() {
        return this.fieldMaps;
    }
    
    public String getLanguageId() {
        return "6d27b471-0525-4d4c-bf85-b4bd447d6edd";
    }
    
    public String getLanguageName() {
        return "org.clawiz.etl.common.language";
    }
    
    public String getLanguageConceptId() {
        return "2943750519754464392";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.etl.common.language.structure.LoadTypeDataJobStepAction";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("6d27b471-0525-4d4c-bf85-b4bd447d6edd", "org.clawiz.etl.common.language", "2943750519754464392", "org.clawiz.etl.common.language.structure.LoadTypeDataJobStepAction", ConceptPropertyType.REFERENCE, "2943750519754465899", "connection"));
        result.add(new ConceptProperty("6d27b471-0525-4d4c-bf85-b4bd447d6edd", "org.clawiz.etl.common.language", "2943750519754464392", "org.clawiz.etl.common.language.structure.LoadTypeDataJobStepAction", ConceptPropertyType.REFERENCE, "2943750519754465890", "sourceStructure"));
        result.add(new ConceptProperty("6d27b471-0525-4d4c-bf85-b4bd447d6edd", "org.clawiz.etl.common.language", "2943750519754464392", "org.clawiz.etl.common.language.structure.LoadTypeDataJobStepAction", ConceptPropertyType.REFERENCE, "2943750519754464405", "targetType"));
        result.add(new ConceptProperty("6d27b471-0525-4d4c-bf85-b4bd447d6edd", "org.clawiz.etl.common.language", "2943750519754464392", "org.clawiz.etl.common.language.structure.LoadTypeDataJobStepAction", ConceptPropertyType.CHILD, "285384861138755904", "fieldMaps"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        for (AbstractMpsNode value : getFieldMaps() ) {
            addConceptNodeChild("2943750519754464392", "fieldMaps", value);
        }
        addConceptNodeRef("2943750519754464392", "connection", getConnection());
        addConceptNodeRef("2943750519754464392", "sourceStructure", getSourceStructure());
        addConceptNodeRef("2943750519754464392", "targetType", getTargetType());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.etl.common.metadata.data.job.step.action.load.type.LoadTypeDataJobStepAction.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.etl.common.metadata.data.job.step.action.load.type.LoadTypeDataJobStepAction structure = (org.clawiz.etl.common.metadata.data.job.step.action.load.type.LoadTypeDataJobStepAction) node;
        
        if ( getConnection() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "connection", false, getConnection());
        } else {
            structure.setConnection(null);
        }
        
        if ( getSourceStructure() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "sourceStructure", false, getSourceStructure());
        } else {
            structure.setSourceStructure(null);
        }
        
        if ( getTargetType() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "targetType", false, getTargetType());
        } else {
            structure.setTargetType(null);
        }
        
        structure.getFieldMaps().clear();
        for (org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step.action.load.type.MpsTypeDataLoadFieldMap mpsNode : getFieldMaps() ) {
            if ( mpsNode != null ) {
                structure.getFieldMaps().add((org.clawiz.etl.common.metadata.data.job.step.action.load.type.TypeDataLoadFieldMap) mpsNode.toMetadataNode(structure, "fieldMaps"));
            } else {
                structure.getFieldMaps().add(null);
            }
        }
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
        addForeignKey(getConnection());
        addForeignKey(getSourceStructure());
        addForeignKey(getTargetType());
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.etl.common.metadata.data.job.step.action.load.type.LoadTypeDataJobStepAction structure = (org.clawiz.etl.common.metadata.data.job.step.action.load.type.LoadTypeDataJobStepAction) node;
        
        if ( structure.getConnection() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "connection", false, structure.getConnection());
        } else {
            setConnection(null);
        }
        
        if ( structure.getSourceStructure() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "sourceStructure", false, structure.getSourceStructure());
        } else {
            setSourceStructure(null);
        }
        
        if ( structure.getTargetType() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "targetType", false, structure.getTargetType());
        } else {
            setTargetType(null);
        }
        
        getFieldMaps().clear();
        for (org.clawiz.etl.common.metadata.data.job.step.action.load.type.TypeDataLoadFieldMap metadataNode : structure.getFieldMaps() ) {
            if ( metadataNode != null ) {
                getFieldMaps().add(loadChildMetadataNode(metadataNode));
            } else {
                getFieldMaps().add(null);
            }
        }
        
    }
}
