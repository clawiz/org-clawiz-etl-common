package org.clawiz.etl.common.metadata.data.structure.valuetype;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class StructureValueTypeNumberPrototype extends org.clawiz.etl.common.metadata.data.structure.valuetype.AbstractStructureValueType {
    
    public StructureValueTypeNumber withName(String value) {
        setName(value);
        return (StructureValueTypeNumber) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
