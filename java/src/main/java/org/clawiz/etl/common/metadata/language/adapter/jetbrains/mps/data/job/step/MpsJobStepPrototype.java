package org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsJobStepPrototype extends AbstractMpsNode {
    
    public ArrayList<org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step.MpsJobStepRef> previousSteps = new ArrayList<>();
    
    public ArrayList<org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step.action.MpsAbstractJobStepAction> actions = new ArrayList<>();
    
    public ArrayList<org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step.MpsJobStepRef> getPreviousSteps() {
        return this.previousSteps;
    }
    
    public ArrayList<org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step.action.MpsAbstractJobStepAction> getActions() {
        return this.actions;
    }
    
    public String getLanguageId() {
        return "6d27b471-0525-4d4c-bf85-b4bd447d6edd";
    }
    
    public String getLanguageName() {
        return "org.clawiz.etl.common.language";
    }
    
    public String getLanguageConceptId() {
        return "2943750519754466411";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.etl.common.language.structure.JobStep";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("6d27b471-0525-4d4c-bf85-b4bd447d6edd", "org.clawiz.etl.common.language", "2943750519754466411", "org.clawiz.etl.common.language.structure.JobStep", ConceptPropertyType.CHILD, "2943750519754464339", "previousSteps"));
        result.add(new ConceptProperty("6d27b471-0525-4d4c-bf85-b4bd447d6edd", "org.clawiz.etl.common.language", "2943750519754466411", "org.clawiz.etl.common.language.structure.JobStep", ConceptPropertyType.CHILD, "2943750519754466415", "actions"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        for (AbstractMpsNode value : getPreviousSteps() ) {
            addConceptNodeChild("2943750519754466411", "previousSteps", value);
        }
        for (AbstractMpsNode value : getActions() ) {
            addConceptNodeChild("2943750519754466411", "actions", value);
        }
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.etl.common.metadata.data.job.step.JobStep.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.etl.common.metadata.data.job.step.JobStep structure = (org.clawiz.etl.common.metadata.data.job.step.JobStep) node;
        
        structure.getPreviousSteps().clear();
        for (org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step.MpsJobStepRef mpsNode : getPreviousSteps() ) {
            if ( mpsNode.getJobStep() != null ) {
                getParserContext().addDeferredParseNodeResolve(structure, "previousSteps", true, mpsNode.getJobStep());
            } else {
                structure.getPreviousSteps().add(null);
            }
        }
        
        structure.getActions().clear();
        for (org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step.action.MpsAbstractJobStepAction mpsNode : getActions() ) {
            if ( mpsNode != null ) {
                structure.getActions().add((org.clawiz.etl.common.metadata.data.job.step.action.AbstractJobStepAction) mpsNode.toMetadataNode(structure, "actions"));
            } else {
                structure.getActions().add(null);
            }
        }
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.etl.common.metadata.data.job.step.JobStep structure = (org.clawiz.etl.common.metadata.data.job.step.JobStep) node;
        
        getPreviousSteps().clear();
        for (org.clawiz.etl.common.metadata.data.job.step.JobStep metadataNode : structure.getPreviousSteps() ) {
            if ( metadataNode != null ) {
                getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "previousSteps", true, metadataNode);
            } else {
                getPreviousSteps().add(null);
            }
        }
        
        getActions().clear();
        for (org.clawiz.etl.common.metadata.data.job.step.action.AbstractJobStepAction metadataNode : structure.getActions() ) {
            if ( metadataNode != null ) {
                getActions().add(loadChildMetadataNode(metadataNode));
            } else {
                getActions().add(null);
            }
        }
        
    }
}
