package org.clawiz.etl.common.metadata.data.structure.valuetype;

import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class StructureValueTypeStructureReferencePrototype extends org.clawiz.etl.common.metadata.data.structure.valuetype.AbstractStructureValueType {
    
    @ExchangeReference
    private org.clawiz.etl.common.metadata.data.structure.Structure referencedStructure;
    
    public StructureValueTypeStructureReference withName(String value) {
        setName(value);
        return (StructureValueTypeStructureReference) this;
    }
    
    public org.clawiz.etl.common.metadata.data.structure.Structure getReferencedStructure() {
        return this.referencedStructure;
    }
    
    public void setReferencedStructure(org.clawiz.etl.common.metadata.data.structure.Structure value) {
        this.referencedStructure = value;
    }
    
    public StructureValueTypeStructureReference withReferencedStructure(org.clawiz.etl.common.metadata.data.structure.Structure value) {
        setReferencedStructure(value);
        return (StructureValueTypeStructureReference) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getReferencedStructure() != null ) { 
            getReferencedStructure().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getReferencedStructure());
        
    }
}
