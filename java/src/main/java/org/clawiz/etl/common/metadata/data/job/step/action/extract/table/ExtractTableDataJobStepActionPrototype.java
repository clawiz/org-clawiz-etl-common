package org.clawiz.etl.common.metadata.data.job.step.action.extract.table;

import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class ExtractTableDataJobStepActionPrototype extends org.clawiz.etl.common.metadata.data.job.step.action.extract.AbstractExtractJobStepAction {
    
    @ExchangeAttribute
    private String sourceTableName;
    
    @ExchangeReference
    private org.clawiz.etl.common.metadata.data.connection.JDBCConnection connection;
    
    @ExchangeReference
    private org.clawiz.etl.common.metadata.data.structure.Structure structure;
    
    public ExtractTableDataJobStepAction withName(String value) {
        setName(value);
        return (ExtractTableDataJobStepAction) this;
    }
    
    public String getSourceTableName() {
        return this.sourceTableName;
    }
    
    public void setSourceTableName(String value) {
        this.sourceTableName = value;
    }
    
    public ExtractTableDataJobStepAction withSourceTableName(String value) {
        setSourceTableName(value);
        return (ExtractTableDataJobStepAction) this;
    }
    
    public org.clawiz.etl.common.metadata.data.connection.JDBCConnection getConnection() {
        return this.connection;
    }
    
    public void setConnection(org.clawiz.etl.common.metadata.data.connection.JDBCConnection value) {
        this.connection = value;
    }
    
    public ExtractTableDataJobStepAction withConnection(org.clawiz.etl.common.metadata.data.connection.JDBCConnection value) {
        setConnection(value);
        return (ExtractTableDataJobStepAction) this;
    }
    
    public org.clawiz.etl.common.metadata.data.structure.Structure getStructure() {
        return this.structure;
    }
    
    public void setStructure(org.clawiz.etl.common.metadata.data.structure.Structure value) {
        this.structure = value;
    }
    
    public ExtractTableDataJobStepAction withStructure(org.clawiz.etl.common.metadata.data.structure.Structure value) {
        setStructure(value);
        return (ExtractTableDataJobStepAction) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getConnection() != null ) { 
            getConnection().prepare(session);
        }
        if ( getStructure() != null ) { 
            getStructure().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getConnection());
        
        references.add(getStructure());
        
    }
}
