package org.clawiz.etl.common.metadata.data.job;

import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class JobPrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    @ExchangeElement
    private org.clawiz.etl.common.metadata.data.job.step.JobStepList steps = new org.clawiz.etl.common.metadata.data.job.step.JobStepList();
    
    public Job withName(String value) {
        setName(value);
        return (Job) this;
    }
    
    public org.clawiz.etl.common.metadata.data.job.step.JobStepList getSteps() {
        return this.steps;
    }
    
    public Job withStep(org.clawiz.etl.common.metadata.data.job.step.JobStep value) {
        getSteps().add(value);
        return (Job) this;
    }
    
    public <T extends org.clawiz.etl.common.metadata.data.job.step.JobStep> T createStep(Class<T> nodeClass) {
        org.clawiz.etl.common.metadata.data.job.step.JobStep value = createChildNode(nodeClass, "steps");
        getSteps().add(value);
        return (T) value;
    }
    
    public org.clawiz.etl.common.metadata.data.job.step.JobStep createStep() {
        return createStep(org.clawiz.etl.common.metadata.data.job.step.JobStep.class);
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        for (MetadataNode node : getSteps()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        for (MetadataNode node : getSteps()) {
            references.add(node);
        }
        
    }
}
