package org.clawiz.etl.common.metadata.data.job.step;

import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class JobStepPrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    @ExchangeReference
    private org.clawiz.etl.common.metadata.data.job.step.JobStepList previousSteps = new org.clawiz.etl.common.metadata.data.job.step.JobStepList();
    
    @ExchangeElement
    private org.clawiz.etl.common.metadata.data.job.step.action.JobStepActionList actions = new org.clawiz.etl.common.metadata.data.job.step.action.JobStepActionList();
    
    public JobStep withName(String value) {
        setName(value);
        return (JobStep) this;
    }
    
    public org.clawiz.etl.common.metadata.data.job.step.JobStepList getPreviousSteps() {
        return this.previousSteps;
    }
    
    public JobStep withPreviousStep(org.clawiz.etl.common.metadata.data.job.step.JobStep value) {
        getPreviousSteps().add(value);
        return (JobStep) this;
    }
    
    public org.clawiz.etl.common.metadata.data.job.step.action.JobStepActionList getActions() {
        return this.actions;
    }
    
    public JobStep withAction(org.clawiz.etl.common.metadata.data.job.step.action.AbstractJobStepAction value) {
        getActions().add(value);
        return (JobStep) this;
    }
    
    public <T extends org.clawiz.etl.common.metadata.data.job.step.action.AbstractJobStepAction> T createAction(Class<T> nodeClass) {
        org.clawiz.etl.common.metadata.data.job.step.action.AbstractJobStepAction value = createChildNode(nodeClass, "actions");
        getActions().add(value);
        return (T) value;
    }
    
    public org.clawiz.etl.common.metadata.data.job.step.action.AbstractJobStepAction createAction() {
        return createAction(org.clawiz.etl.common.metadata.data.job.step.action.AbstractJobStepAction.class);
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        for (MetadataNode node : getPreviousSteps()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        for (MetadataNode node : getActions()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        for (MetadataNode node : getPreviousSteps()) {
            references.add(node);
        }
        
        for (MetadataNode node : getActions()) {
            references.add(node);
        }
        
    }
}
