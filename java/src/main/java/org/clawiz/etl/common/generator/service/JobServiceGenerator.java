/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.etl.common.generator.service;

import org.clawiz.core.common.system.generator.java.AbstractJavaClassGenerator;
import org.clawiz.etl.common.metadata.data.job.Job;
import org.clawiz.etl.common.metadata.data.job.step.JobStep;
import org.clawiz.etl.common.metadata.data.job.step.action.AbstractJobStepAction;
import org.clawiz.etl.common.metadata.data.job.step.action.extract.table.ExtractTableDataJobStepAction;
import org.clawiz.etl.common.metadata.data.job.step.action.load.type.LoadTypeDataJobStepAction;
import org.clawiz.etl.common.generator.helper.JobGeneratorHelper;
import org.clawiz.etl.common.generator.service.component.job.instance.JobInstanceComponent;
import org.clawiz.etl.common.generator.service.component.job.instance.JobInstancePrototypeComponent;
import org.clawiz.etl.common.generator.service.component.job.service.JobServiceComponent;
import org.clawiz.etl.common.generator.service.component.job.service.JobServicePrototypeComponent;
import org.clawiz.etl.common.generator.service.component.step.service.element.action.AbstractJobStepServicePrototypeActionMethodElement;
import org.clawiz.etl.common.generator.service.component.step.service.element.action.extract.JobStepServicePrototypeExtractTableDataActionMethodElement;
import org.clawiz.etl.common.generator.service.component.step.service.element.action.load.JobStepServicePrototypeLoadTypeDataActionMethodElement;
import org.clawiz.etl.common.generator.service.component.step.instance.JobStepInstanceComponent;
import org.clawiz.etl.common.generator.service.component.step.instance.JobStepInstancePrototypeComponent;
import org.clawiz.etl.common.generator.service.component.step.service.JobStepServiceComponent;
import org.clawiz.etl.common.generator.service.component.step.service.JobStepServicePrototypeComponent;

import java.util.HashMap;

public class JobServiceGenerator extends AbstractJavaClassGenerator {

    public static final String AFTER_PREPARE_EXTENSION_NAME = "afterPrepare";

    JobGeneratorHelper jobGeneratorHelper;

    Job job;

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
        setPackageName(job.getPackageName() + "." + job.getName().toLowerCase());
    }

    public JobGeneratorHelper getJobGeneratorHelper() {
        return jobGeneratorHelper;
    }

    HashMap<Class, Class> actionsMap = new HashMap<>();
    public <A extends AbstractJobStepAction, E extends AbstractJobStepServicePrototypeActionMethodElement> void addActionMap(Class<A> actionClass, Class<E> elemetClass) {
        actionsMap.put(actionClass, elemetClass);
    }

    public <A extends AbstractJobStepAction, E extends AbstractJobStepServicePrototypeActionMethodElement> Class<E> getActionMap(Class<A> actionClass) {
        return actionsMap.get(actionClass);
    }

    @Override
    protected void prepare() {

        super.prepare();

        addActionMap(ExtractTableDataJobStepAction.class, JobStepServicePrototypeExtractTableDataActionMethodElement.class);
        addActionMap(LoadTypeDataJobStepAction.class, JobStepServicePrototypeLoadTypeDataActionMethodElement.class);

        processExtensions(AFTER_PREPARE_EXTENSION_NAME);

    }

    @Override
    protected void process() {
        logInfo("Start generating job service " + job.getFullName());
        super.process();

        addComponent(JobInstanceComponent.class);
        addComponent(JobInstancePrototypeComponent.class);

        addComponent(JobServiceComponent.class);
        addComponent(JobServicePrototypeComponent.class);

        for(JobStep step : job.getSteps()) {

            addComponent(JobStepInstanceComponent.class).setStep(step);
            addComponent(JobStepInstancePrototypeComponent.class).setStep(step);

            addComponent(JobStepServiceComponent.class).setStep(step);
            addComponent(JobStepServicePrototypeComponent.class).setStep(step);

        }

    }

    @Override
    protected void done() {
        super.done();
        logInfo("Done  generating job service " + job.getFullName());
    }




}
