/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.etl.common.metadata.data.job.step.action;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.system.generator.GeneratorUtils;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.etl.common.metadata.data.job.step.JobStep;
import org.clawiz.etl.common.metadata.data.structure.Structure;

public class AbstractJobStepAction extends AbstractJobStepActionPrototype {


    public JobStep getJobStep() {
        return (JobStep) getParentNode();
    }

    public JobStepActionType getType() {
        throw new CoreException("Method ?.getType not implemented", this.getClass().getName());
    }


    protected String      preparedName;
    protected Structure[] extractStructures   = new Structure[]{};
    protected Structure[] processStructures   = new Structure[]{};

    protected String getNamePrefix() {
        throw new CoreException("Method ?.getNamePrefix not implemented", this.getClass().getName());
    }

    private String getPreparedName() {
        return preparedName;
    }

    @Override
    public String getName() {
        return preparedName;
    }

    @Override
    public void prepare(Session session) {
        super.prepare(session);

        String _preparedPrefix = GeneratorUtils.toJavaClassName(getNamePrefix(), true);
        String _preparedName   = _preparedPrefix;
        boolean found = true;
        int     index = 2;
        while (found) {
            found = false;
            for ( AbstractJobStepAction action : getJobStep().getActions() ) {
                if ( _preparedName .equals(action.getPreparedName()) ) {
                    found = true;
                    _preparedName  = _preparedPrefix + index++;
                    break;
                }
            }
        }
        preparedName = _preparedName;

    }

    public Structure[] getExtractStructures() {
        return extractStructures;
    }

    public Structure[] getProcessStructures() {
        return processStructures;
    }

    protected void setExtractStructures(Structure[] extractStructures) {
        this.extractStructures = extractStructures;
    }

    protected void setProcessStructures(Structure[] processStructures) {
        this.processStructures = processStructures;
    }
}
