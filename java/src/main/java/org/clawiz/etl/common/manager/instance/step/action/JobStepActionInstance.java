/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.etl.common.manager.instance.step.action;

import org.clawiz.etl.common.metadata.data.job.step.action.AbstractJobStepAction;
import org.clawiz.etl.common.manager.dataset.AbstractDataSet;
import org.clawiz.etl.common.manager.instance.step.AbstractJobStepInstance;
import org.clawiz.etl.common.manager.service.step.action.JobStepActionThread;

public class JobStepActionInstance {

    AbstractJobStepInstance stepInstance;

    AbstractJobStepAction   action;
    JobStepActionThread     thread;

    JobStepActionState      state = JobStepActionState.READY;
    Exception               cause;

    AbstractDataSet         dataSet;


    public AbstractJobStepInstance getStepInstance() {
        return stepInstance;
    }

    public void setStepInstance(AbstractJobStepInstance stepInstance) {
        this.stepInstance = stepInstance;
    }

    public AbstractJobStepAction getAction() {
        return action;
    }

    public void setAction(AbstractJobStepAction action) {
        this.action = action;
    }

    public JobStepActionThread getThread() {
        return thread;
    }

    public void setThread(JobStepActionThread thread) {
        this.thread = thread;
    }

    public JobStepActionState getState() {
        return state;
    }

    public void setState(JobStepActionState state) {
        this.state = state;
    }

    public Exception getCause() {
        return cause;
    }

    public void setCause(Exception cause) {
        this.cause = cause;
    }

    public AbstractDataSet getDataSet() {
        return dataSet;
    }

    public void setDataSet(AbstractDataSet dataSet) {
        this.dataSet = dataSet;
    }
}


