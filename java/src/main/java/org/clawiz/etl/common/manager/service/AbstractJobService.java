/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.etl.common.manager.service;

import org.clawiz.core.common.system.service.Service;
import org.clawiz.etl.common.manager.JobManager;
import org.clawiz.etl.common.manager.instance.AbstractJobInstance;
import org.clawiz.etl.common.manager.instance.step.AbstractJobStepInstance;
import org.clawiz.etl.common.manager.instance.step.JobStepInstanceState;
import org.clawiz.etl.common.manager.service.step.action.JobStepActionThread;

abstract public class AbstractJobService<T extends AbstractJobInstance> extends Service {

    protected JobManager jobManager;

    abstract public T createInstance();

    public T start() {
        T instance = createInstance();
        jobManager.start(instance);
        return instance;
    }


    public void done(T instance) {
        jobManager.done(instance);
    }

    public void stepThreadDone(JobStepActionThread thread) {

        jobManager.stepActionThreadDone(thread);

    }

    public void stepThreadException(JobStepActionThread thread, Exception cause) {

        jobManager.stepActionThreadException(thread, cause);

    }


}
