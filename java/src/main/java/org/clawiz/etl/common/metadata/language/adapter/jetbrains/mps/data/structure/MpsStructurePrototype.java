package org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsStructurePrototype extends AbstractMpsNode {
    
    public ArrayList<org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.field.MpsStructureField> fields = new ArrayList<>();
    
    public org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.field.MpsStructureField recordIdField;
    
    public ArrayList<org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.field.MpsStructureField> getFields() {
        return this.fields;
    }
    
    public org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.field.MpsStructureField getRecordIdField() {
        return this.recordIdField;
    }
    
    public void setRecordIdField(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.field.MpsStructureField value) {
        this.recordIdField = value;
    }
    
    public String getLanguageId() {
        return "6d27b471-0525-4d4c-bf85-b4bd447d6edd";
    }
    
    public String getLanguageName() {
        return "org.clawiz.etl.common.language";
    }
    
    public String getLanguageConceptId() {
        return "2943750519754464343";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.etl.common.language.structure.Structure";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("6d27b471-0525-4d4c-bf85-b4bd447d6edd", "org.clawiz.etl.common.language", "2943750519754464343", "org.clawiz.etl.common.language.structure.Structure", ConceptPropertyType.CHILD, "2943750519754464349", "fields"));
        result.add(new ConceptProperty("6d27b471-0525-4d4c-bf85-b4bd447d6edd", "org.clawiz.etl.common.language", "2943750519754464343", "org.clawiz.etl.common.language.structure.Structure", ConceptPropertyType.REFERENCE, "285384861138638585", "recordIdField"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        for (AbstractMpsNode value : getFields() ) {
            addConceptNodeChild("2943750519754464343", "fields", value);
        }
        addConceptNodeRef("2943750519754464343", "recordIdField", getRecordIdField());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.etl.common.metadata.data.structure.Structure.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.etl.common.metadata.data.structure.Structure structure = (org.clawiz.etl.common.metadata.data.structure.Structure) node;
        
        structure.getFields().clear();
        for (org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.field.MpsStructureField mpsNode : getFields() ) {
            if ( mpsNode != null ) {
                structure.getFields().add((org.clawiz.etl.common.metadata.data.structure.field.StructureField) mpsNode.toMetadataNode(structure, "fields"));
            } else {
                structure.getFields().add(null);
            }
        }
        
        if ( getRecordIdField() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "recordIdField", false, getRecordIdField());
        } else {
            structure.setRecordIdField(null);
        }
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.etl.common.metadata.data.structure.Structure structure = (org.clawiz.etl.common.metadata.data.structure.Structure) node;
        
        getFields().clear();
        for (org.clawiz.etl.common.metadata.data.structure.field.StructureField metadataNode : structure.getFields() ) {
            if ( metadataNode != null ) {
                getFields().add(loadChildMetadataNode(metadataNode));
            } else {
                getFields().add(null);
            }
        }
        
        if ( structure.getRecordIdField() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "recordIdField", false, structure.getRecordIdField());
        } else {
            setRecordIdField(null);
        }
        
    }
}
