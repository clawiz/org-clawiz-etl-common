package org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.connection;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsJDBCConnectionPrototype extends org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.connection.MpsAbstractConnection {
    
    public String url;
    
    public String username;
    
    public String password;
    
    public String getUrl() {
        return this.url;
    }
    
    public void setUrl(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.url = null;
        }
        this.url = value;
    }
    
    public String getUsername() {
        return this.username;
    }
    
    public void setUsername(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.username = null;
        }
        this.username = value;
    }
    
    public String getPassword() {
        return this.password;
    }
    
    public void setPassword(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.password = null;
        }
        this.password = value;
    }
    
    public String getLanguageId() {
        return "6d27b471-0525-4d4c-bf85-b4bd447d6edd";
    }
    
    public String getLanguageName() {
        return "org.clawiz.etl.common.language";
    }
    
    public String getLanguageConceptId() {
        return "2943750519754464358";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.etl.common.language.structure.JDBCConnection";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("6d27b471-0525-4d4c-bf85-b4bd447d6edd", "org.clawiz.etl.common.language", "2943750519754464358", "org.clawiz.etl.common.language.structure.JDBCConnection", ConceptPropertyType.PROPERTY, "2943750519754464359", "url"));
        result.add(new ConceptProperty("6d27b471-0525-4d4c-bf85-b4bd447d6edd", "org.clawiz.etl.common.language", "2943750519754464358", "org.clawiz.etl.common.language.structure.JDBCConnection", ConceptPropertyType.PROPERTY, "2943750519754464361", "username"));
        result.add(new ConceptProperty("6d27b471-0525-4d4c-bf85-b4bd447d6edd", "org.clawiz.etl.common.language", "2943750519754464358", "org.clawiz.etl.common.language.structure.JDBCConnection", ConceptPropertyType.PROPERTY, "2943750519754464364", "password"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeProperty("2943750519754464358", "url", getUrl());
        addConceptNodeProperty("2943750519754464358", "username", getUsername());
        addConceptNodeProperty("2943750519754464358", "password", getPassword());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.etl.common.metadata.data.connection.JDBCConnection.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.etl.common.metadata.data.connection.JDBCConnection structure = (org.clawiz.etl.common.metadata.data.connection.JDBCConnection) node;
        
        structure.setUrl(getUrl());
        
        structure.setUsername(getUsername());
        
        structure.setPassword(getPassword());
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.etl.common.metadata.data.connection.JDBCConnection structure = (org.clawiz.etl.common.metadata.data.connection.JDBCConnection) node;
        
        setUrl(structure.getUrl());
        
        setUsername(structure.getUsername());
        
        setPassword(structure.getPassword());
        
    }
}
