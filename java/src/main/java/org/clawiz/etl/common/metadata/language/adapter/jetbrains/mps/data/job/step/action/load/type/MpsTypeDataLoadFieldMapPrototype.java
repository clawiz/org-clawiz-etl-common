package org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step.action.load.type;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsTypeDataLoadFieldMapPrototype extends AbstractMpsNode {
    
    public org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.field.MpsStructureField sourceField;
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.MpsTypeField targetField;
    
    public org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.field.MpsStructureField getSourceField() {
        return this.sourceField;
    }
    
    public void setSourceField(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.field.MpsStructureField value) {
        this.sourceField = value;
    }
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.MpsTypeField getTargetField() {
        return this.targetField;
    }
    
    public void setTargetField(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.MpsTypeField value) {
        this.targetField = value;
    }
    
    public String getLanguageId() {
        return "6d27b471-0525-4d4c-bf85-b4bd447d6edd";
    }
    
    public String getLanguageName() {
        return "org.clawiz.etl.common.language";
    }
    
    public String getLanguageConceptId() {
        return "285384861138755888";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.etl.common.language.structure.TypeDataLoadFieldMap";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("6d27b471-0525-4d4c-bf85-b4bd447d6edd", "org.clawiz.etl.common.language", "285384861138755888", "org.clawiz.etl.common.language.structure.TypeDataLoadFieldMap", ConceptPropertyType.REFERENCE, "285384861138755909", "sourceField"));
        result.add(new ConceptProperty("6d27b471-0525-4d4c-bf85-b4bd447d6edd", "org.clawiz.etl.common.language", "285384861138755888", "org.clawiz.etl.common.language.structure.TypeDataLoadFieldMap", ConceptPropertyType.REFERENCE, "285384861138755911", "targetField"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeRef("285384861138755888", "sourceField", getSourceField());
        addConceptNodeRef("285384861138755888", "targetField", getTargetField());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.etl.common.metadata.data.job.step.action.load.type.TypeDataLoadFieldMap.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.etl.common.metadata.data.job.step.action.load.type.TypeDataLoadFieldMap structure = (org.clawiz.etl.common.metadata.data.job.step.action.load.type.TypeDataLoadFieldMap) node;
        
        if ( getSourceField() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "sourceField", false, getSourceField());
        } else {
            structure.setSourceField(null);
        }
        
        if ( getTargetField() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "targetField", false, getTargetField());
        } else {
            structure.setTargetField(null);
        }
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.etl.common.metadata.data.job.step.action.load.type.TypeDataLoadFieldMap structure = (org.clawiz.etl.common.metadata.data.job.step.action.load.type.TypeDataLoadFieldMap) node;
        
        if ( structure.getSourceField() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "sourceField", false, structure.getSourceField());
        } else {
            setSourceField(null);
        }
        
        if ( structure.getTargetField() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "targetField", false, structure.getTargetField());
        } else {
            setTargetField(null);
        }
        
    }
}
