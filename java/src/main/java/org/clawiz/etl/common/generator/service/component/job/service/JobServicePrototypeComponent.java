/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.etl.common.generator.service.component.job.service;

import org.clawiz.core.common.system.generator.java.component.element.JavaMemberElement;
import org.clawiz.core.common.system.generator.java.component.element.JavaMethodElement;
import org.clawiz.etl.common.metadata.data.job.Job;
import org.clawiz.etl.common.generator.service.component.job.AbstractJobGeneratorJobJavaClassComponent;
import org.clawiz.etl.common.generator.service.component.job.service.element.*;
import org.clawiz.etl.common.manager.instance.step.AbstractJobStepInstance;
import org.clawiz.etl.common.manager.service.AbstractJobService;
import org.clawiz.etl.common.storage.job.JobService;

import java.math.BigDecimal;

public class JobServicePrototypeComponent extends AbstractJobGeneratorJobJavaClassComponent {

    protected void addImports() {

        addImport(Job.class);
        addImport(JobService.class);
        addImport(BigDecimal.class);
        addImport(AbstractJobStepInstance.class);

    }

    private void addVariables() {

        addVariable("Job", "_job")
                .withAccessLevel(JavaMemberElement.AccessLevel.PRIVATE);

    }


    protected void addCommonGetters() {
        JavaMethodElement method = addMethod("Job", "getJob");
        method.addText("return _job;");
    }

    @Override
    public void process() {
        super.process();

        setName(getGenerator().getComponentByClass(JobServiceComponent.class).getName() + "Prototype");

        setExtends(AbstractJobService.class.getName() + "<" + getInstanceClassName() + ">" );

        addImports();

        addVariables();

        addElement(JobServicePrototypeInitMethodElement.class);

        addCommonGetters();

        addElement(JobServicePrototypeCreateInstanceMethodElement.class);

    }

}
