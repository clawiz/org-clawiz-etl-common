package org.clawiz.etl.common.metadata.data.structure.valuetype;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class StructureValueTypeDateTimePrototype extends org.clawiz.etl.common.metadata.data.structure.valuetype.AbstractStructureValueType {
    
    public StructureValueTypeDateTime withName(String value) {
        setName(value);
        return (StructureValueTypeDateTime) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
