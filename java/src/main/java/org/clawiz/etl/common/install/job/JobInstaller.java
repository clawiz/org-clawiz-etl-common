/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.etl.common.install.job;

import org.clawiz.core.common.metadata.install.AbstractMetadataNodeInstaller;
import org.clawiz.etl.common.metadata.data.job.Job;
import org.clawiz.etl.common.metadata.data.job.step.JobStep;
import org.clawiz.etl.common.generator.service.JobServiceGenerator;
import org.clawiz.etl.common.storage.job.JobObject;
import org.clawiz.etl.common.storage.job.JobService;
import org.clawiz.etl.common.storage.jobstep.JobStepObject;
import org.clawiz.etl.common.storage.jobstep.JobStepService;

import java.math.BigDecimal;
import java.util.HashMap;

public class JobInstaller extends AbstractMetadataNodeInstaller {

    JobService     jobService;
    JobStepService jobStepService;

    protected void saveJob(Job job) {

        JobObject jobObject = jobService.load(jobService.packageNameToId(job.getPackageName(), job.getName(), true));

        HashMap<BigDecimal, BigDecimal> stepsCache = new HashMap<>();
        for( JobStep jobStep : job.getSteps() ) {
            JobStepObject stepObject = jobStepService.load(jobStepService.jobNameToId(jobObject.getId(), jobStep.getName(), true));
            stepObject.setActive(true);
            stepObject.save();
            stepsCache.put(stepObject.getId(), stepObject.getId());
        }

        for( JobStepObject stepObject : jobStepService.loadList("job_id = ?", jobObject.getId())) {
            if ( ! stepsCache.containsKey(stepObject.getId()) ) {
                stepObject.setActive(false);
                stepObject.save();
            }
        }


        jobObject.save();


    }

    @Override
    public void process() {

        Job job = (Job) getNode();

        saveJob(job);

        JobServiceGenerator generator = getService(JobServiceGenerator.class, true);
        generator.setRootDestinationPath(getDestinationPath());
        generator.setJob(job);

        generator.run();

    }
}
