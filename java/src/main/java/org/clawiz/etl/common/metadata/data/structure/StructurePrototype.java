package org.clawiz.etl.common.metadata.data.structure;

import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class StructurePrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    @ExchangeElement
    private org.clawiz.etl.common.metadata.data.structure.field.StructureFieldList fields = new org.clawiz.etl.common.metadata.data.structure.field.StructureFieldList();
    
    @ExchangeReference
    private org.clawiz.etl.common.metadata.data.structure.field.StructureField recordIdField;
    
    public Structure withName(String value) {
        setName(value);
        return (Structure) this;
    }
    
    public org.clawiz.etl.common.metadata.data.structure.field.StructureFieldList getFields() {
        return this.fields;
    }
    
    public Structure withField(org.clawiz.etl.common.metadata.data.structure.field.StructureField value) {
        getFields().add(value);
        return (Structure) this;
    }
    
    public <T extends org.clawiz.etl.common.metadata.data.structure.field.StructureField> T createField(Class<T> nodeClass) {
        org.clawiz.etl.common.metadata.data.structure.field.StructureField value = createChildNode(nodeClass, "fields");
        getFields().add(value);
        return (T) value;
    }
    
    public org.clawiz.etl.common.metadata.data.structure.field.StructureField createField() {
        return createField(org.clawiz.etl.common.metadata.data.structure.field.StructureField.class);
    }
    
    public org.clawiz.etl.common.metadata.data.structure.field.StructureField getRecordIdField() {
        return this.recordIdField;
    }
    
    public void setRecordIdField(org.clawiz.etl.common.metadata.data.structure.field.StructureField value) {
        this.recordIdField = value;
    }
    
    public Structure withRecordIdField(org.clawiz.etl.common.metadata.data.structure.field.StructureField value) {
        setRecordIdField(value);
        return (Structure) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        for (MetadataNode node : getFields()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        if ( getRecordIdField() != null ) { 
            getRecordIdField().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        for (MetadataNode node : getFields()) {
            references.add(node);
        }
        
        references.add(getRecordIdField());
        
    }
}
