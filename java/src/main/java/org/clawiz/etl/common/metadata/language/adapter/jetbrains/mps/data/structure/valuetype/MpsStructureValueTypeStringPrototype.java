package org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.valuetype;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsStructureValueTypeStringPrototype extends org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.valuetype.MpsAbstractStructureValueType {
    
    public BigDecimal length;
    
    public BigDecimal getLength() {
        return this.length;
    }
    
    public void setLength(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.length = null;
        }
        this.length = StringUtils.toBigDecimal(value);
    }
    
    public String getLanguageId() {
        return "6d27b471-0525-4d4c-bf85-b4bd447d6edd";
    }
    
    public String getLanguageName() {
        return "org.clawiz.etl.common.language";
    }
    
    public String getLanguageConceptId() {
        return "4155258003355141100";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.etl.common.language.structure.StructureValueTypeString";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("6d27b471-0525-4d4c-bf85-b4bd447d6edd", "org.clawiz.etl.common.language", "4155258003355141100", "org.clawiz.etl.common.language.structure.StructureValueTypeString", ConceptPropertyType.PROPERTY, "7374764979001196473", "length"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeProperty("4155258003355141100", "length", getLength() != null ? getLength().toString() : null);
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.etl.common.metadata.data.structure.valuetype.StructureValueTypeString.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.etl.common.metadata.data.structure.valuetype.StructureValueTypeString structure = (org.clawiz.etl.common.metadata.data.structure.valuetype.StructureValueTypeString) node;
        
        structure.setLength(getLength());
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.etl.common.metadata.data.structure.valuetype.StructureValueTypeString structure = (org.clawiz.etl.common.metadata.data.structure.valuetype.StructureValueTypeString) node;
        
        setLength(structure.getLength() != null ? structure.getLength().toString() : null);
        
    }
}
