/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.etl.common.generator.connection.jdbc.component;

import org.clawiz.core.common.system.generator.java.component.element.JavaMethodElement;
import org.clawiz.etl.common.manager.connection.AbstractJDBCConnection;

public class JDBCConnectionPrototypeComponent extends AbstractJDBCConnectionComponent {


    protected void addGetters() {

        JavaMethodElement method = null;
        if ( getConnection().getUrl() != null ) {
            method = addMethod("String", "getUrl");
            method.addText("return \"" + getConnection().getUrl() + "\";");
        }

        if ( getConnection().getUsername() != null ) {
            method = addMethod("String", "getUsername");
            method.addText("return \"" + getConnection().getUsername() + "\";");
        }

        if ( getConnection().getPassword() != null ) {
            method = addMethod("String", "getPassword");
            method.addText("return \"" + getConnection().getPassword() + "\";");
        }

    }

    @Override
    public void process() {
        super.process();

        setName(getGenerator().getComponentByClass(JDBCConnectionComponent.class).getName() + "Prototype");

        setExtends(AbstractJDBCConnection.class.getName());

        addGetters();
    }

}
