/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.etl.common.generator.service.component.job.service.element;

public class JobServicePrototypeInitMethodElement extends AbstractJobServicePrototypeJavaMethodElement {


    @Override
    public void process() {
        super.process();

        setName("init");


        addText("super.init();");

        addText("");
        addText("if ( _job == null ) {");
        addText("    ");
        addText("    JobService jobService = getService(JobService.class);");
        addText("    BigDecimal jobId      = jobService.fullNameToId(\"" + getJob().getFullName() + "\");");
        addText("    if ( jobId == null ) {");
        addText("        throwException(\"Job '?' not registered in the database\", \"" + getJob().getFullName() + "\");");
        addText("    }");
        addText("    _job = jobService.getJob(jobId);");
        addText("    ");
        addText("}");


    }
}
