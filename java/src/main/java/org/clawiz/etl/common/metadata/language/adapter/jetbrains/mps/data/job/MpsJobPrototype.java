package org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsJobPrototype extends AbstractMpsNode {
    
    public ArrayList<org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step.MpsJobStep> steps = new ArrayList<>();
    
    public ArrayList<org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step.MpsJobStep> getSteps() {
        return this.steps;
    }
    
    public String getLanguageId() {
        return "6d27b471-0525-4d4c-bf85-b4bd447d6edd";
    }
    
    public String getLanguageName() {
        return "org.clawiz.etl.common.language";
    }
    
    public String getLanguageConceptId() {
        return "2943750519754464330";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.etl.common.language.structure.Job";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("6d27b471-0525-4d4c-bf85-b4bd447d6edd", "org.clawiz.etl.common.language", "2943750519754464330", "org.clawiz.etl.common.language.structure.Job", ConceptPropertyType.CHILD, "2943750519754464421", "steps"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        for (AbstractMpsNode value : getSteps() ) {
            addConceptNodeChild("2943750519754464330", "steps", value);
        }
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.etl.common.metadata.data.job.Job.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.etl.common.metadata.data.job.Job structure = (org.clawiz.etl.common.metadata.data.job.Job) node;
        
        structure.getSteps().clear();
        for (org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step.MpsJobStep mpsNode : getSteps() ) {
            if ( mpsNode != null ) {
                structure.getSteps().add((org.clawiz.etl.common.metadata.data.job.step.JobStep) mpsNode.toMetadataNode(structure, "steps"));
            } else {
                structure.getSteps().add(null);
            }
        }
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.etl.common.metadata.data.job.Job structure = (org.clawiz.etl.common.metadata.data.job.Job) node;
        
        getSteps().clear();
        for (org.clawiz.etl.common.metadata.data.job.step.JobStep metadataNode : structure.getSteps() ) {
            if ( metadataNode != null ) {
                getSteps().add(loadChildMetadataNode(metadataNode));
            } else {
                getSteps().add(null);
            }
        }
        
    }
}
