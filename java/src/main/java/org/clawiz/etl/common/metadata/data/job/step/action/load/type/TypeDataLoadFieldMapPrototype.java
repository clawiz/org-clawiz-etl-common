package org.clawiz.etl.common.metadata.data.job.step.action.load.type;

import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class TypeDataLoadFieldMapPrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    @ExchangeReference
    private org.clawiz.etl.common.metadata.data.structure.field.StructureField sourceField;
    
    @ExchangeReference
    private org.clawiz.core.common.metadata.data.type.field.TypeField targetField;
    
    public TypeDataLoadFieldMap withName(String value) {
        setName(value);
        return (TypeDataLoadFieldMap) this;
    }
    
    public org.clawiz.etl.common.metadata.data.structure.field.StructureField getSourceField() {
        return this.sourceField;
    }
    
    public void setSourceField(org.clawiz.etl.common.metadata.data.structure.field.StructureField value) {
        this.sourceField = value;
    }
    
    public TypeDataLoadFieldMap withSourceField(org.clawiz.etl.common.metadata.data.structure.field.StructureField value) {
        setSourceField(value);
        return (TypeDataLoadFieldMap) this;
    }
    
    public org.clawiz.core.common.metadata.data.type.field.TypeField getTargetField() {
        return this.targetField;
    }
    
    public void setTargetField(org.clawiz.core.common.metadata.data.type.field.TypeField value) {
        this.targetField = value;
    }
    
    public TypeDataLoadFieldMap withTargetField(org.clawiz.core.common.metadata.data.type.field.TypeField value) {
        setTargetField(value);
        return (TypeDataLoadFieldMap) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getSourceField() != null ) { 
            getSourceField().prepare(session);
        }
        if ( getTargetField() != null ) { 
            getTargetField().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getSourceField());
        
        references.add(getTargetField());
        
    }
}
