package org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.valuetype;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsStructureValueTypeScaledNumberPrototype extends org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.valuetype.MpsAbstractStructureValueType {
    
    public BigDecimal precision;
    
    public BigDecimal scale;
    
    public BigDecimal getPrecision() {
        return this.precision;
    }
    
    public void setPrecision(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.precision = null;
        }
        this.precision = StringUtils.toBigDecimal(value);
    }
    
    public BigDecimal getScale() {
        return this.scale;
    }
    
    public void setScale(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.scale = null;
        }
        this.scale = StringUtils.toBigDecimal(value);
    }
    
    public String getLanguageId() {
        return "6d27b471-0525-4d4c-bf85-b4bd447d6edd";
    }
    
    public String getLanguageName() {
        return "org.clawiz.etl.common.language";
    }
    
    public String getLanguageConceptId() {
        return "4155258003355141113";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.etl.common.language.structure.StructureValueTypeScaledNumber";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("6d27b471-0525-4d4c-bf85-b4bd447d6edd", "org.clawiz.etl.common.language", "4155258003355141113", "org.clawiz.etl.common.language.structure.StructureValueTypeScaledNumber", ConceptPropertyType.PROPERTY, "4155258003355141115", "precision"));
        result.add(new ConceptProperty("6d27b471-0525-4d4c-bf85-b4bd447d6edd", "org.clawiz.etl.common.language", "4155258003355141113", "org.clawiz.etl.common.language.structure.StructureValueTypeScaledNumber", ConceptPropertyType.PROPERTY, "4155258003355141114", "scale"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeProperty("4155258003355141113", "precision", getPrecision() != null ? getPrecision().toString() : null);
        addConceptNodeProperty("4155258003355141113", "scale", getScale() != null ? getScale().toString() : null);
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.etl.common.metadata.data.structure.valuetype.StructureValueTypeScaledNumber.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.etl.common.metadata.data.structure.valuetype.StructureValueTypeScaledNumber structure = (org.clawiz.etl.common.metadata.data.structure.valuetype.StructureValueTypeScaledNumber) node;
        
        structure.setPrecision(getPrecision());
        
        structure.setScale(getScale());
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.etl.common.metadata.data.structure.valuetype.StructureValueTypeScaledNumber structure = (org.clawiz.etl.common.metadata.data.structure.valuetype.StructureValueTypeScaledNumber) node;
        
        setPrecision(structure.getPrecision() != null ? structure.getPrecision().toString() : null);
        
        setScale(structure.getScale() != null ? structure.getScale().toString() : null);
        
    }
}
