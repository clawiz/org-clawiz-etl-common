package org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.valuetype;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsStructureValueTypeStructureReferencePrototype extends org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.valuetype.MpsAbstractStructureValueType {
    
    public org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.MpsStructure referencedStructure;
    
    public org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.MpsStructure getReferencedStructure() {
        return this.referencedStructure;
    }
    
    public void setReferencedStructure(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.structure.MpsStructure value) {
        this.referencedStructure = value;
    }
    
    public String getLanguageId() {
        return "6d27b471-0525-4d4c-bf85-b4bd447d6edd";
    }
    
    public String getLanguageName() {
        return "org.clawiz.etl.common.language";
    }
    
    public String getLanguageConceptId() {
        return "4155258003355155902";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.etl.common.language.structure.StructureValueTypeStructureReference";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("6d27b471-0525-4d4c-bf85-b4bd447d6edd", "org.clawiz.etl.common.language", "4155258003355155902", "org.clawiz.etl.common.language.structure.StructureValueTypeStructureReference", ConceptPropertyType.REFERENCE, "4155258003355155904", "referencedStructure"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeRef("4155258003355155902", "referencedStructure", getReferencedStructure());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.etl.common.metadata.data.structure.valuetype.StructureValueTypeStructureReference.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.etl.common.metadata.data.structure.valuetype.StructureValueTypeStructureReference structure = (org.clawiz.etl.common.metadata.data.structure.valuetype.StructureValueTypeStructureReference) node;
        
        if ( getReferencedStructure() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "referencedStructure", false, getReferencedStructure());
        } else {
            structure.setReferencedStructure(null);
        }
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
        addForeignKey(getReferencedStructure());
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.etl.common.metadata.data.structure.valuetype.StructureValueTypeStructureReference structure = (org.clawiz.etl.common.metadata.data.structure.valuetype.StructureValueTypeStructureReference) node;
        
        if ( structure.getReferencedStructure() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "referencedStructure", false, structure.getReferencedStructure());
        } else {
            setReferencedStructure(null);
        }
        
    }
}
