/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.etl.common.generator.service.component.job;

import org.clawiz.core.common.system.generator.GeneratorUtils;
import org.clawiz.core.common.system.generator.abstractgenerator.AbstractGenerator;
import org.clawiz.core.common.system.generator.java.component.AbstractJavaClassComponent;
import org.clawiz.core.common.system.service.NotInitializeService;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.etl.common.metadata.data.job.Job;
import org.clawiz.etl.common.generator.helper.JobGeneratorHelper;
import org.clawiz.etl.common.generator.service.JobServiceGenerator;

public class AbstractJobGeneratorJobJavaClassComponent extends AbstractJavaClassComponent {

    private JobServiceGenerator    _jobServiceGenerator;
    private Job                    job;

    @NotInitializeService
    protected JobGeneratorHelper   jobGeneratorHelper;

    @Override
    public JobServiceGenerator getGenerator() {
        return _jobServiceGenerator;
    }

    @Override
    public void setGenerator(AbstractGenerator generator) {
        super.setGenerator(generator);
        _jobServiceGenerator = (JobServiceGenerator) generator;
        job                  = _jobServiceGenerator.getJob();
        jobGeneratorHelper   = _jobServiceGenerator.getJobGeneratorHelper();
    }


    public String getServiceClassName() {
        return getJob().getJavaClassName() + "Service";
    }

    public String getInstanceClassName() {
        return getJob().getJavaClassName() + "Instance";
    }

    public Job getJob() {
         return job;
    }

    public JobGeneratorHelper getJobGeneratorHelper() {
        return jobGeneratorHelper;
    }
}
