/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.etl.common.manager.service.step.action;

import org.clawiz.etl.common.manager.instance.step.action.JobStepActionInstance;
import org.clawiz.etl.common.manager.instance.step.action.JobStepActionState;
import org.clawiz.etl.common.manager.service.step.AbstractJobStepService;

public class JobStepActionThread extends Thread {

    JobStepActionInstance actionInstance;

    public JobStepActionInstance getActionInstance() {
        return actionInstance;
    }

    public void setActionInstance(JobStepActionInstance actionInstance) {
        this.actionInstance = actionInstance;
    }

    private boolean protectedMode;

    public boolean isProtectedMode() {
        return protectedMode;
    }

    @Override
    public void run() {

        AbstractJobStepService service = actionInstance.getStepInstance().getService();

        actionInstance.setState(JobStepActionState.RUN);
        try {

//            service.logDebug("Start action thread for " + actionInstance.getAction().getName());

            service.process(actionInstance);

//            service.logDebug("Done  action thread for " + actionInstance.getAction().getName());

            protectedMode = true;
            service.threadDone(this);

        } catch (Exception e) {
            e.printStackTrace();
            protectedMode = true;
            service.threadException(this, e);
        } finally {
            protectedMode = false;
        }

    }

}


