/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.etl.common.manager.dataset;

import org.clawiz.etl.common.metadata.data.job.step.action.AbstractJobStepAction;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

public class AbstractDataSet<T extends AbstractDataSetRow> implements Iterable<T> {

    ArrayList<T> rows               = new ArrayList<>();
    long         totalRowsCount     = 0;
    long         extractedRowsCount = 0;

    ConcurrentHashMap<AbstractJobStepAction, AbstractJobStepAction> processedActionsCache = new ConcurrentHashMap<>();

    public long getTotalRowsCount() {
        return totalRowsCount;
    }

    public void setTotalRowsCount(long totalRowsCount) {
        this.totalRowsCount = totalRowsCount;
    }

    public long getExtractedRowsCount() {
        return extractedRowsCount;
    }

    public void setExtractedRowsCount(long extractedRowsCount) {
        this.extractedRowsCount = extractedRowsCount;
    }

    public void add(T row) {
        rows.add(row);
    }

    public int size() {
        return rows.size();
    }

    @Override
    public Iterator<T> iterator() {
        return rows.iterator();
    }

    public boolean isActionProcessed(AbstractJobStepAction action) {
        return processedActionsCache.containsKey(action);
    }

    public void setActionProcessed(AbstractJobStepAction action) {
        processedActionsCache.put(action, action);
    }

}
