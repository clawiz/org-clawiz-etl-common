/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.etl.common.manager.instance.step;

import org.clawiz.core.common.CoreException;
import org.clawiz.etl.common.metadata.data.job.step.JobStep;
import org.clawiz.etl.common.manager.instance.AbstractJobInstance;
import org.clawiz.etl.common.manager.service.step.AbstractJobStepService;

public class AbstractJobStepInstance<T extends AbstractJobStepService> {

    JobStep              step;
    T                    service;

    JobStepInstanceState state = JobStepInstanceState.READY;
    Exception            cause;

    AbstractJobInstance  jobInstance;

    public JobStep getStep() {
        return step;
    }

    public void setStep(JobStep step) {
        this.step = step;
    }

    public JobStepInstanceState getState() {
        return state;
    }

    public void setState(JobStepInstanceState state) {
        if ( this.state == JobStepInstanceState.ERROR && state != JobStepInstanceState.ERROR ) {
            throw new CoreException("Cannot rerun error step");
        }
        this.state = state;
    }

    public Exception getCause() {
        return cause;
    }

    public void setCause(Exception cause) {
        this.cause = cause;
    }

    public T getService() {
        return service;
    }

    public void setService(T service) {
        this.service = service;
    }

    public AbstractJobInstance getJobInstance() {
        return jobInstance;
    }

    public void setJobInstance(AbstractJobInstance jobInstance) {
        this.jobInstance = jobInstance;
    }

}
