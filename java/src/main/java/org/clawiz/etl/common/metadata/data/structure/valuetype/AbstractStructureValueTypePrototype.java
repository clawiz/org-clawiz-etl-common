package org.clawiz.etl.common.metadata.data.structure.valuetype;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class AbstractStructureValueTypePrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    public AbstractStructureValueType withName(String value) {
        setName(value);
        return (AbstractStructureValueType) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
