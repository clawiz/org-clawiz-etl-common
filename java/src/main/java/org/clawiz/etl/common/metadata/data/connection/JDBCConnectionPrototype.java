package org.clawiz.etl.common.metadata.data.connection;

import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class JDBCConnectionPrototype extends org.clawiz.etl.common.metadata.data.connection.AbstractConnection {
    
    @ExchangeAttribute
    private String url;
    
    @ExchangeAttribute
    private String username;
    
    @ExchangeAttribute
    private String password;
    
    public JDBCConnection withName(String value) {
        setName(value);
        return (JDBCConnection) this;
    }
    
    public String getUrl() {
        return this.url;
    }
    
    public void setUrl(String value) {
        this.url = value;
    }
    
    public JDBCConnection withUrl(String value) {
        setUrl(value);
        return (JDBCConnection) this;
    }
    
    public String getUsername() {
        return this.username;
    }
    
    public void setUsername(String value) {
        this.username = value;
    }
    
    public JDBCConnection withUsername(String value) {
        setUsername(value);
        return (JDBCConnection) this;
    }
    
    public String getPassword() {
        return this.password;
    }
    
    public void setPassword(String value) {
        this.password = value;
    }
    
    public JDBCConnection withPassword(String value) {
        setPassword(value);
        return (JDBCConnection) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
