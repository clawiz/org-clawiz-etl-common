package org.clawiz.etl.common.storage.job;


import org.clawiz.core.common.metadata.MetadataBase;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.etl.common.metadata.data.job.Job;
import org.clawiz.etl.common.metadata.data.job.step.JobStep;
import org.clawiz.etl.common.storage.jobstep.JobStepService;

import java.math.BigDecimal;
import java.util.concurrent.ConcurrentHashMap;

public class JobService extends JobServicePrototype {

    private static ConcurrentHashMap<String, BigDecimal> _fullNamesCache = new ConcurrentHashMap<>();

    @SuppressWarnings("Duplicates")
    public BigDecimal fullNameToId(String name) {
        if ( name == null ) {
            return null;
        }
        String[] tokens = StringUtils.splitByLastTokenOccurrence(name, "\\.");
        if ( tokens.length < 2 ) {
            return null;
        }
        BigDecimal id = packageNameToId(tokens[0], tokens[1]);
        if ( id != null ) {
            _fullNamesCache.put(name, id);
        }
        return id;
    }

    private static ConcurrentHashMap<BigDecimal, Job> _jobsCache = new ConcurrentHashMap<>();

    public Job getJob(BigDecimal jobId) {
        if ( jobId == null ) {
            return null;
        }
        Job job = _jobsCache.get(jobId);
        if ( job != null ){
            return job;
        }
        JobObject jobObject = load(jobId);
        if ( jobObject == null ) {
            throwException("Job with id ? not found in database", jobId);
        }

        MetadataBase   metadataBase = getService(MetadataBase.class);
        JobStepService stepService  = getService(JobStepService.class);

        job = metadataBase.getNode(Job.class, jobObject.getPackageName(), jobObject.getName(), true);
        job.setId(jobId);
        for (JobStep step : job.getSteps() ) {
            BigDecimal id = stepService.jobNameToId(jobId, step.getName());
            if ( id == null ) {
                throwException("Job ? step '?' not registered in database. Attempt to reinstall module", job.getFullName(), step.getName());
            }
            step.setId(id);
        }

        _jobsCache.put(jobId, job);
        return job;
    }

    public static void clearStaticCaches() {
        _fullNamesCache.clear();
        _jobsCache.clear();
    }

    @Override
    public void save(JobObject processObject) {
        super.save(processObject);
        clearStaticCaches();
    }

    @Override
    public void delete(BigDecimal id) {
        super.delete(id);
        clearStaticCaches();
    }

}
