package org.clawiz.etl.common.storage.jobstep;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.object.AbstractObject;
import org.clawiz.core.common.system.session.Session;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Clob;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.date.DateUtils;
import java.lang.SuppressWarnings;

public class JobStepObjectPrototype extends AbstractObject {
    
    /**
    * Job
    */
    private java.math.BigDecimal jobId;
    
    /**
    * Name
    */
    private java.lang.String name;
    
    /**
    * Description
    */
    private java.lang.String description;
    
    /**
    * active
    */
    private java.lang.Boolean active;
    
    public JobStepService service;
    
    /**
    * 
    * @return     Job
    */
    public java.math.BigDecimal getJobId() {
        return this.jobId;
    }
    
    /**
    * Set 'Job' value
    * 
    * @param      jobId Job
    */
    public void setJobId(java.math.BigDecimal jobId) {
         this.jobId = jobId;
    }
    
    /**
    * Set 'Job' value and return this object
    * 
    * @param      jobId Job
    * @return     JobStep object
    */
    public JobStepObjectPrototype withJobId(java.math.BigDecimal jobId) {
        setJobId(jobId);
        return this;
    }
    
    /**
    * 
    * @return     Name
    */
    public java.lang.String getName() {
        return this.name;
    }
    
    /**
    * Set 'Name' value
    * 
    * @param      name Name
    */
    public void setName(java.lang.String name) {
        
        if ( name != null && name.length() > 128) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "JobStep.Name", "128");
        }
        
         this.name = name;
    }
    
    /**
    * Set 'Name' value and return this object
    * 
    * @param      name Name
    * @return     JobStep object
    */
    public JobStepObjectPrototype withName(java.lang.String name) {
        setName(name);
        return this;
    }
    
    /**
    * 
    * @return     Description
    */
    public java.lang.String getDescription() {
        return this.description;
    }
    
    /**
    * Set 'Description' value
    * 
    * @param      description Description
    */
    public void setDescription(java.lang.String description) {
        
        if ( description != null && description.length() > 512) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "JobStep.Description", "512");
        }
        
         this.description = description;
    }
    
    /**
    * Set 'Description' value and return this object
    * 
    * @param      description Description
    * @return     JobStep object
    */
    public JobStepObjectPrototype withDescription(java.lang.String description) {
        setDescription(description);
        return this;
    }
    
    /**
    * 
    * @return     active
    */
    public java.lang.Boolean isActive() {
        return this.active != null ? this.active : false;
    }
    
    /**
    * Set 'active' value
    * 
    * @param      active active
    */
    public void setActive(java.lang.Boolean active) {
         this.active = active;
    }
    
    /**
    * Set 'active' value
    * 
    * @param      active active
    */
    public void setActive(boolean active) {
        this.active= new Boolean(active);
    }
    
    /**
    * Set 'active' value and return this object
    * 
    * @param      active active
    * @return     JobStep object
    */
    public JobStepObjectPrototype withActive(java.lang.Boolean active) {
        setActive(active);
        return this;
    }
    
    /**
    * Set 'active' value
    * 
    * @param      active active
    * @return     
    */
    public JobStepObjectPrototype withActive(boolean active) {
        setActive(active);
        return this;
    }
    
    public JobStepService getService() {
        return this.service;
    }
    
    public void setService(JobStepService service) {
        this.service = service;
    }
    
    /**
    * Copy values of all fields to 'target'
    * 
    * @param       target Target object to copy data
    */
    @SuppressWarnings("Duplicates")
    public void copy(JobStepObjectPrototype  target) {
        target.setJobId(getJobId());
        target.setName(getName());
        target.setDescription(getDescription());
        target.setActive(isActive());
    }
    
    /**
    * Fill object field default values
    */
    public void fillDefaults() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before fill object default values");
        }
    }
    
    /**
    * Check object data consistency
    */
    public void check() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before check object data");
        }
        service.check((JobStepObject) this);
    }
    
    /**
    * Save object to database
    */
    @SuppressWarnings("Duplicates")
    public void save() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before save object");
        }
        service.save((JobStepObject) this);
    }
    
    /**
    * Return true if all field values equals to 'object' field values or both value of field is null
    * 
    * @param      object Equal check object
    * @return     field-by-field equals check
    */
    public boolean equals(JobStepObjectPrototype object) {
        return 
               isObjectsEquals(this.getJobId(), object.getJobId() ) 
            && isObjectsEquals(this.getName(), object.getName() ) 
            && isObjectsEquals(this.getDescription(), object.getDescription() ) 
            && isObjectsEquals(this.isActive(), object.isActive() ) 
               ;
    }
    
    /**
    * Calculate hash value of object data
    * 
    * @return     int hash code of object
    */
    public int hashCode() {
        int result = 1;
        
        result = result * 31 + (getJobId() != null ? getJobId().hashCode() : 0);
        result = result * 31 + (getName() != null ? getName().hashCode() : 0);
        result = result * 31 + (getDescription() != null ? getDescription().hashCode() : 0);
        result = result * 31 + (isActive() != null ? isActive().hashCode() : 0);
        
        return result;
    }
    
    /**
    * Prepare concatenated string values of key 'JobName' fields
    * 
    * @return     Concatenated string values of key 'toJobName' fields
    */
    public String toJobName() {
        return service.getObjectService().idToString(getJobId()) + "," + getName();
    }
}
