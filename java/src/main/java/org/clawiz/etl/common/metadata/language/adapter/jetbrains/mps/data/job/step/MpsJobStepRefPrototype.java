package org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsJobStepRefPrototype extends AbstractMpsNode {
    
    public org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step.MpsJobStep jobStep;
    
    public org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step.MpsJobStep getJobStep() {
        return this.jobStep;
    }
    
    public void setJobStep(org.clawiz.etl.common.metadata.language.adapter.jetbrains.mps.data.job.step.MpsJobStep value) {
        this.jobStep = value;
    }
    
    public String getLanguageId() {
        return "6d27b471-0525-4d4c-bf85-b4bd447d6edd";
    }
    
    public String getLanguageName() {
        return "org.clawiz.etl.common.language";
    }
    
    public String getLanguageConceptId() {
        return "2943750519754464336";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.etl.common.language.structure.JobStepRef";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("6d27b471-0525-4d4c-bf85-b4bd447d6edd", "org.clawiz.etl.common.language", "2943750519754464336", "org.clawiz.etl.common.language.structure.JobStepRef", ConceptPropertyType.REFERENCE, "2943750519754464337", "jobStep"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeRef("2943750519754464336", "jobStep", getJobStep());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return null;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
    }
}
