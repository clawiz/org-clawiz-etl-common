/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.etl.common.helper;

import org.clawiz.core.common.system.service.Service;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.etl.common.manager.instance.AbstractJobInstance;
import org.clawiz.etl.common.manager.instance.step.AbstractJobStepInstance;
import org.clawiz.etl.common.manager.instance.step.JobStepInstanceState;

public class JobHelper extends Service {

    public void dumpInstance(AbstractJobInstance instance ) {

        logDebug("<job instance " + instance.getJob().getFullName() + " : " + instance);
        logDebug("State : " + instance.getState());
        int maxLength = 0;
        for (AbstractJobStepInstance stepInstance : instance.getSteps() ) {
            if ( stepInstance.getStep().getName().length() > maxLength ) {
                maxLength = stepInstance.getStep().getName().length();
            }
        }

        for (AbstractJobStepInstance stepInstance : instance.getSteps() ) {
            String errorStr = "";
            if ( stepInstance.getCause() != null ) {
                errorStr = stepInstance.getCause().getMessage();
            }
            String name = stepInstance.getStep().getName();
            logDebug("  " + name + StringUtils.space(maxLength - name.length()) + " : " + stepInstance.getState() + " " + errorStr );
        }

        logDebug("</job instance " + instance.getJob().getFullName() + " : " + instance);


    }

}
