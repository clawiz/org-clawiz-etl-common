/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.etl.common.generator.service.component.step.service.element;

import org.clawiz.core.common.system.generator.abstractgenerator.component.AbstractComponent;
import org.clawiz.core.common.system.generator.java.component.element.AbstractJavaClassElement;
import org.clawiz.core.common.system.generator.java.component.element.JavaMethodElement;
import org.clawiz.core.common.system.service.NotInitializeService;
import org.clawiz.etl.common.metadata.data.connection.AbstractConnection;
import org.clawiz.etl.common.metadata.data.job.step.JobStep;
import org.clawiz.etl.common.generator.helper.JobGeneratorHelper;
import org.clawiz.etl.common.generator.service.component.job.service.JobServicePrototypeComponent;
import org.clawiz.etl.common.generator.service.component.step.service.JobStepServicePrototypeComponent;

public class AbstractJobStepServicePrototypeMethodElement extends JavaMethodElement {


    JobStepServicePrototypeComponent prototypeComponent;

    @NotInitializeService
    protected JobGeneratorHelper  jobGeneratorHelper;

    @Override
    public void setComponent(AbstractComponent component) {
        super.setComponent(component);
        prototypeComponent = (JobStepServicePrototypeComponent) component;
        jobGeneratorHelper = prototypeComponent.getJobGeneratorHelper();
    }

    @Override
    public JobStepServicePrototypeComponent getComponent() {
        return prototypeComponent;
    }

    public String getConnectionVariableName(AbstractConnection connection) {
        return prototypeComponent.getConnectionVariableName(connection);
    }

    public String getServiceClassName() {
        return getComponent().getServiceClassName();
    }

    public String getInstanceClassName() {
        return getComponent().getInstanceClassName();
    }

    public JobStep getStep() {
        return getComponent().getStep();
    }

    @Override
    public void process() {
        super.process();
        addAnnotation(SuppressWarnings.class).withValue(null, "Duplicates");
    }
}
